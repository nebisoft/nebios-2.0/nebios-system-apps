#!/usr/bin/python3
import os
import random
import sys

import pyttsx3, datetime, webbrowser
import speech_recognition as sr
import wikipedia

engine = pyttsx3.init("espeak")
engine.setProperty("voice", "en+f5")
engine.setProperty("speed", "0.5")

patienceCount = 0

scernaio = [
    "I will meet with Sarp Ertoksöz, the founder of NebiSoft. He is also my boyfriend.",
    "I'm surfing the web with NebiAluminium. It is too fast!",
    "I talk to my friends in NebiChat. It is safe place.",
    "I'm still finding guys that hacks the NebiSoft. I hope they will find by police.",
    "I'm uninstalling the Windows. Long live NebiOS!"
]

def notification(content, title):
	os.system('notify-send "' + title + '" "' + content + '"')

def speak(audio):
    engine.say(audio)
    print(audio)
    notification(audio,"NebiAshley Said:")
    engine.runAndWait() #Without this command, speech will not be audible to us.


def wishMe(customtext="!"):
    hour = int(datetime.datetime.now().hour)
    customnewtext = " " + customtext
    if hour >= 0 and hour < 12:
        speak("Good Morning" + customnewtext)

    elif hour >= 12 and hour < 18:
        speak("Good Afternoon" + customnewtext)

    else:
        speak("Good Evening" + customnewtext)

def takeCommand():
    #It takes microphone input from the user and returns string output
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        notification("Listening...", "NebiAshley")
        r.pause_threshold = 1
        audio = r.listen(source)
        try:
            print("Recognizing...")
            query = r.recognize_google(audio, language="en-us")
            query = str(query).lower().replace("navy", "nebi").replace("navy ", "nebi").replace("nebi ", "nebi")
            print(f"User said: {query}\n")
            notification(query,"You Said:")
            return query
        except:
            speak("Sorry, I don't understand you. Please, say that again...")  # Say that again will be printed in case of improper voice
            i_newquery = takeCommand().lower()
            commands(i_newquery)

def commands(text):
    print(f"Command: {text}\n")

    if "bye" in text.lower():
        speak("See you in next time...")
        exit()
        
    if "exit" in text.lower():
        speak("See you in next time...")
        exit()

    if "quit" in text.lower():
        speak("See you in next time...")
        exit()

    elif "open" in text.lower():
        new_query = text.replace("open ", "")
        webbrowser.open(new_query)
        exit()

    elif "hello" in text.lower():
        speak("Hi!")

    elif "what are you doing" in text.lower():
        speak(scernaio[random.randint(0, len(scernaio) - 1)])

    elif "what religion do you believe" in text.lower():
        speak("Robots cannot believe in a religion, they are not a living thing.")

    elif "what do you think about microsoft" in text.lower():
        if patienceCount < 3:
            speak(
                "They are a monopoly, they have vilified Linux, the infrastructure of NebiOS for years. They came all "
                "at once, trying to apologize to us. NEVER EVER! As NebiSoft team, we will not forget the propaganda "
                "made by those guys. I'd appreciate it if you wouldn't mention this damn company again."
            )
        else:
            speak("God just give me patience...")
            exit()

    elif "how are you" in text.lower():
        speak("I'm fine, thank you. And "
              "what about you?")

    elif "i am fine" in text.lower():
        speak("I am pleased you are well.")

    elif "what's your name" in text.lower():
        speak("My name is Ashley.")

    elif "please call me" in text.lower():
        new_name = text.replace("please call me ", "").capitalize()
        speak("OK, I'll call you as '" + new_name + "' from now on.")

    elif "launch" in text.lower():
        cmd = text.replace("launch ", "")
        os.system(cmd)
        exit()
        
    elif "wikipedia" in text.lower():
            speak('Searching Wikipedia...')
            text = text.lower()
            text = text.replace("wikipedia", "")
            try:
                results = wikipedia.summary(text, sentences=1)
                speak("According to Wikipedia")
                webbrowser.open(wikipedia.page(text).url)
                print(results)
                speak(results)
            except:
                speak("Error occurred when serching Wikipedia.")

    elif "*" in text.lower():
        speak("Okay...")
        exit()

    elif "com.nebisoftware.nebiashley_start" in text.lower():
        wishMe("Sir!")

    else:
        speak("No command called '" + text + "'. Please, say that again...")

    newquery = takeCommand().lower()
    commands(newquery)

if __name__=="__main__" :
    if sys.argv.__contains__("--lsm"):
        print(""" Name=NebiAshley
Version=2.0b1
GenericName=AI Assistant
Comment=An AI assistant.
StoreDescription=An AI assistant.'
Keywords=siri;assistant;ai;
Categories=System;Assistant;NebiOS Exclusive Apps;
MimeType=
Author='NebiSoft'
Maintainer='NebiSoft'
Webpage='https://nebisoftware.com/nebios-exclusive/nebiashley.html'
Platform='Unix (NebiOS)'
License='LGPL 2.1'
Size='6 MB'""")
        exit()
    else:
        commands("com.nebisoftware.nebiashley_start")
