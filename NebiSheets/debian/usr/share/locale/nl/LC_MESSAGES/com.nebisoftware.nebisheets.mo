��    ;      �  O   �           	       	        $     )  7   =     u     {     �  -   �     �     �      �     �  !     )   :  #   d     �  '   �  *   �  #   �          "  	   3     =  	   K     U     a     u  $   z  &   �  $   �  ,   �          1  &   K  $   r     �     �  (   �  !   �  !   	  #   $	  '   H	     p	     x	  ?   �	     �	     �	     �	     �	     �	  	   
     
     
     3
     ;
     A
  g  G
     �  
   �     �     �     �  A   �     =     C     a  ,   p     �  
   �  .   �      �  "     )   $  *   N     y  &   �  )   �  &   �            
   0     ;     O     ^     o     �  *   �  ,   �  -   �  5        R     l  /   �  (   �     �     �  &   �  -   %  *   S  /   ~  ,   �     �  	   �  6   �  	   (     2     @     F  %   Y          �     �  
   �     �     �                       1         (       )      ,            '           "              %         /      $   !                                                    2      3   ;   
   	   .   7   4   6      #      -      *      &   9      +   5                 :                      8   0    Add numbers Bold CSV files Cell Choose a saved file Click to insert numbers or functions to a selected cell Color Create an empty sheet Divide numbers Elevate a number to the power of a second one Fill Font Gives the arc cosine of a number Gives the arc sine of a number Gives the arc tangent of a number Gives the cosine of a number (in radians) Gives the mean of a list of numbers Gives the modulo of numbers Gives the sine of an angle (in radians) Gives the tangent of a number (in radians) Insert functions to a selected cell Italic Multiply numbers New Sheet Not saved yet Open File Open a file Open another window Redo Remove fill color of a selected cell Remove stroke color of a selected cell Removes the decimal part of a number Reset font color of a selected cell to black Return the biggest value Return the smallest value Rounds a number to the nearest integer Save this file with a different name Save your work Search functions Set colors to letters in a selected cell Set fill color of a selected cell Set font color of a selected cell Set stroke color of a selected cell Set the border width of a selected cell Sheet 1 Spreadsheet Start something new, or continue what you have been working on. Strikethrough Stroke Style Subtract numbers The square root of a number Underline Undo Untitled Spreadsheet %i _Cancel _Open _Save Project-Id-Version: com.github.elework.spreadsheet
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-09-19 13:20+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
Last-Translator: Heimen Stoffels <vistausss@outlook.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl
 Getallen toevoegen Vetgedrukt CSV-bestanden Cel Kies een opgeslagen bestand Klik om getallen of functies in te voegen in de geselecteerde cel Kleur Voeg een blanco rekenblad toe Getallen delen Verhoog het getal van het ene met het andere Opvullen Lettertype Toon de curvecosinus van een getal (in straal) Toon de curvesinus van een getal Toon de curvetangent van een getal Toon de cosinus van een getal (in straal) Toon het totaal van een lijst met getallen Toon de modulaire berekening Toon de sinus van een hoek (in straal) Toon de tangent van een getal (in straal) Functies invoegen in geselecteerde cel Cursief Getallen vermenigvuldigen Nieuw blad Nog niet opgeslagen Bestand openen Open een bestand Nieuw venster openen Opnieuw uitvoeren Opvulkleur van de geselecteerde cel wissen Penseelkleur van de geselecteerde cel wissen Verwijder het decimale gedeelte van een getal Standaard letterkleur herstellen in geselecteerde cel Bereken de hoogste waarde Bereken de laagste waarde Rond een getal af naar het hoogste gehele getal Sla dit bestand op onder een andere naam Sla je werk op Functies zoeken Letters inkleuren in geselecteerde cel Opvulkleur van de geselecteerde cel instellen Letterkleur instellen in geselecteerde cel Penseelkleur van de geselecteerde cel instellen Randdikte van de geselecteerde cel instellen Rekenblad 1 Rekenblad Maak een nieuw werkblad of ga door met je vorige werk. Doorhalen Penseelstreek Stijl Getallen aftrekken Toon de vierkantswortel van een getal Onderstrepen Ongedaan maken Naamloos rekenblad %i Ann_uleren _Openen Op_slaan 