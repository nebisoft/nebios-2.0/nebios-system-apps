��    /      �  C                	   %     /     4  7   H     �     �     �  -   �     �     �  #   �       *   #  #   N     r  	   �     �  	   �     �     �  $   �  &   �  $     ,   '     T     m  &   �  $   �     �     �  (   �  !     !   >  #   `  '   �     �     �  ?   �                #     (     @     H     N  2  T     �	     �	     �	  $   �	  Q   �	     &
     .
     H
  (   [
     �
     �
  '   �
     �
  *   �
  5   
     @     V     g     ~     �  	   �  ?   �  ;   �  &   (  G   O     �     �  ,   �  -         .     H  :   a  ?   �  :   �  ;     ;   S  	   �     �  ?   �     �     �               *     3     ;                       
           #      (               "   	   +       )                               !            *   .   ,                    -                       $         '         %          /                      &               Add numbers CSV files Cell Choose a saved file Click to insert numbers or functions to a selected cell Color Create an empty sheet Divide numbers Elevate a number to the power of a second one Fill Font Gives the mean of a list of numbers Gives the modulo of numbers Gives the tangent of a number (in radians) Insert functions to a selected cell Multiply numbers New Sheet Not saved yet Open File Open a file Redo Remove fill color of a selected cell Remove stroke color of a selected cell Removes the decimal part of a number Reset font color of a selected cell to black Return the biggest value Return the smallest value Rounds a number to the nearest integer Save this file with a different name Save your work Search functions Set colors to letters in a selected cell Set fill color of a selected cell Set font color of a selected cell Set stroke color of a selected cell Set the border width of a selected cell Sheet 1 Spreadsheet Start something new, or continue what you have been working on. Stroke The square root of a number Undo Untitled Spreadsheet %i _Cancel _Open _Save Project-Id-Version: com.github.elework.spreadsheet
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-22 16:00+0200
Last-Translator: NathanBnm
Language-Team: none
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajoute des nombres Fichiers CSV Cellule Sélectionner un fichier enregistré Cliquez pour insérer des nombres ou des fonctions dans la cellule sélectionnée Couleur Créer une feuille vierge Divise des nombres Met un nombre à la puissance d'un autre Remplissage Police Donne la moyenne d'une liste de nombres Donne le modulo de deux nombres Donne la tangente d'un nombre (en radians) Insérer des fonctions dans la cellule sélectionnée Multiplie des nombres Nouvelle feuille Pas encore enregistré Ouvrir un fichier Ouvrir un fichier Rétablir Supprime la couleur de remplissage de la cellule sélectionnée Supprimer la couleur de border de la cellule sélectionnée Retire la partie décimale d'un nombre Réinitialise la couleur de police de la cellule sélectionnée en noir Renvoie la plus grande valeur Renvoie la plus petite valeur Arrondi un nombre à l'entier le plus proche Enregistrer ce fichier avec un nom différent Enregistrez votre travail Rechercher des fonctions Définir la couleur de police de la cellule sélectionnée Définir la couleur de remplissage de la cellule sélectionnée Définit la couleur de police de la cellule sélectionnée Définit la couleur de bordure de la cellule sélectionnée Définir la largeur de bordure de la cellule sélectionnée Feuille 1 Spreadsheet Commencez quelque chose de nouveau, ou continuez votre travail. Bordure La racine carrée d'un nombre Annuler Classeur sans titre %i _Annuler _Ouvrir _Enregistrer 