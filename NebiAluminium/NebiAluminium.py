#!/usr/bin/python3
import asyncio
import os
from functools import cached_property
import sys
from PyQt5.QtCore import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineCore import *
from sys import platform
from bs4 import BeautifulSoup
import requests

def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

class RequestManager(QWebEngineUrlRequestInterceptor):
    """
    This subclass works as a gate. As far as I understand the functionality, the WebView instances are sending their requests to this class.
    He then decides either that a request is valid and doesn't contain any ads (look at interceptRequest) or a request is an ad and he blocks it.'
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_up()

    def set_up(self):
        self.setup()

    def setup(self):
        print("RequestManager.setup()")
        files = os.listdir("engine/adblock/")
        wincount = 0
        for file in files:
            thefile = open("engine/adblock/" + file,encoding="utf8")
            while 1:
                lines = thefile.readlines(1000000)
                res = ""
                if not lines:
                    break
                for line in lines:
                    res = res + line
                    app.processEvents()
                    if wincount == 0:
                        wincount = 1
                        window = main()
                try:
                        self.blist = self.blist + res.split("\n")
                except:
                        self.blist = res.split("\n")
                for i in self.blist:
                        if i.startswith("!") or i.startswith("##") or i.startswith("["):
                            self.blist.remove(i)
                del self.blist[len(self.blist) - 1]

                print(str(file) + " " + str(len(self.blist)))

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')
        window.initArgs()

    def interceptRequest(self, info):
        requrl = info.requestUrl().toString()
        if str2bool(str(self.settingsprofile.value("adblock_enabled", True))) == True:
            try:
                for host in self.blist:
                    if "*" in host:
                        host = host.replace("*", "")
                    if host in requrl:
                        info.block(True)
                        print(f"blocking {requrl}")
                    else:
                        pass
            except:
                pass


class SettingsSearch(QWidget):
    def __init__(self):
        super(SettingsSearch, self).__init__()

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')

        self.sebox = QGroupBox("Search engine:")
        self.senamelist = ["DuckDuckGo", "Bing"]
        self.selist = ["https://duckduckgo.com/?q=", "https://www.bing.com/search?q="]
        self.secombo = QComboBox()
        self.secombo.addItems(self.senamelist)
        self.secombo.setCurrentText(self.senamelist[self.selist.index(
            self.settingsprofile.value("search_engine", "https://duckduckgo.com/?q="))])
        self.secombo.currentTextChanged.connect(self.on_cbox_text_changed)
        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.secombo)
        self.sebox.setLayout(self.vbox)

        self.nogooglebox = QGroupBox("Why Google is not here?")
        self.label = QLabel("The reason Google isn't on the list is because it doesn't respect your privacy. At "
                            "NebiSoft, we try to protect your privacy.")
        self.label.setWordWrap(True)
        self.ybox = QVBoxLayout()
        self.ybox.addWidget(self.label)
        self.nogooglebox.setLayout(self.ybox)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.sebox)
        mainLayout.addWidget(self.nogooglebox)
        self.setLayout(mainLayout)

    def on_cbox_text_changed(self, val):
        self.settingsprofile.setValue("search_engine", self.selist[self.secombo.currentIndex()])
        self.window().search_engine = self.settingsprofile.value("search_engine", "https://duckduckgo.com/?q=")


class SettingsLookAndFeel(QWidget):
    def __init__(self):
        super(SettingsLookAndFeel, self).__init__()

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')

        self.iconbox = QGroupBox("Icon theme:")
        self.iconlist = os.listdir("ui/icons/")
        self.iconcombo = QComboBox()
        self.iconcombo.addItems(self.iconlist)
        self.iconcombo.setCurrentText(QIcon.themeName())
        self.vbox = QVBoxLayout()
        self.vbox.addWidget(self.iconcombo)
        self.iconbox.setLayout(self.vbox)
        self.iconcombo.currentTextChanged.connect(self.on_icon_change)

        strings = os.listdir("ui/stylesheet/")

        self.csbox = QGroupBox("Theme:")
        self.cscombo = QComboBox()
        self.cscombo.addItems(strings)
        self.cscombo.setCurrentText(self.settingsprofile.value("qss_theme_name", "Default"))
        self.vboxp = QVBoxLayout()
        self.vboxp.addWidget(self.cscombo)
        self.csbox.setLayout(self.vboxp)
        self.cscombo.currentTextChanged.connect(self.on_cbox_text_changed)

        mainLayout = QVBoxLayout()
        if platform == "linux" or platform == "linux2":
            print("Disabling icon selection")
        else:
            mainLayout.addWidget(self.iconbox)
        mainLayout.addWidget(self.csbox)
        self.setLayout(mainLayout)

    def on_cbox_text_changed(self, val):
        newval = str(val)
        self.settingsprofile.setValue("qss_theme_name", newval)
        try:
            self.window().loadTheme()
        except:
            print("Error when setting theme.")

    def on_icon_change(self, val):
        newval = str(val)
        self.settingsprofile.setValue("icon_theme", newval)
        try:
            QIcon.setThemeName(self.settingsprofile.value("icon_theme", "Default"))
        except:
            print("Error when setting icon theme.")


class SettingsPrivacy(QWidget):
    def clearCache(self):
        cview = QWebEngineView()
        cview.page().profile().clearHttpCache()
        cview.page().profile().cookieStore().deleteAllCookies()

    def adBlockSetting(self):
        self.settingsprofile.setValue("adblock_enabled", self.adblockenable.isChecked())

    def downloadSetting(self):
        self.settingsprofile.setValue("download_enabled", self.dlfile.isChecked())

    def fsSetting(self):
        self.settingsprofile.setValue("full_screen_enabled", self.entfs.isChecked())

    def permSetting(self):
        self.settingsprofile.setValue("permissions_enabled", self.sndnot.isChecked())

    def __init__(self, parent):
        super().__init__(parent)

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')

        self.parenti = parent

        cookbox = QGroupBox("Cookies and Local Website Data")
        cleancookbtn = QPushButton("Clean Cookies and Website Data", self)
        cleancookbtn.clicked.connect(self.clearCache)
        vbox = QVBoxLayout()
        vbox.addWidget(cleancookbtn)
        cookbox.setLayout(vbox)

        miscbox = QGroupBox("Misc (Requires refresh)")
        self.adblockenable = QCheckBox("Enable Adblock")
        adblocknotify = QLabel("Adblock Filters can be managed from your file manager opening"
                               " the NebiAluminium Root Directory > engine > adblock."
                               " Please restart the NebiAluminium after modifications.")
        adblocknotify.setWordWrap(True)
        self.adblockenable.clicked.connect(self.adBlockSetting)
        self.adblockenable.setChecked(str2bool(str(self.settingsprofile.value("adblock_enabled", True))))
        vboxp = QVBoxLayout()
        vboxp.addWidget(self.adblockenable)
        vboxp.addWidget(adblocknotify)
        miscbox.setLayout(vboxp)

        featbox = QGroupBox("Features and Permissions")
        websitescan = QLabel("Websites can")
        self.dlfile = QCheckBox("Download files")
        self.dlfile.clicked.connect(self.downloadSetting)
        self.dlfile.setChecked(bool(self.settingsprofile.value("download_enabled", True)))
        self.entfs = QCheckBox("Enter full screen")
        self.entfs.clicked.connect(self.fsSetting)
        self.entfs.setChecked(bool(self.settingsprofile.value("full_screen_enabled", True)))
        self.sndnot = QCheckBox("Use permissions (asks when requested)")
        self.sndnot.clicked.connect(self.permSetting)
        self.sndnot.setChecked(bool(self.settingsprofile.value("permissions_enabled", True)))
        vboxy = QVBoxLayout()
        vboxy.addWidget(websitescan)
        vboxy.addWidget(self.dlfile)
        vboxy.addWidget(self.entfs)
        vboxy.addWidget(self.sndnot)
        featbox.setLayout(vboxy)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(cookbox)
        mainLayout.addWidget(featbox)
        mainLayout.addWidget(miscbox)
        self.setLayout(mainLayout)


class SettingsGeneral(QWidget):
    def __init__(self):
        super(SettingsGeneral, self).__init__()

        defaultbox = QGroupBox("Default Browser")
        isNotDefault = QLabel(
            "NebiAluminium is not your default web browser right now :( But you can change with clicking button below.")
        isNotDefault.setWordWrap(True)
        isDefault = QLabel("NebiAluminium is your default web browser. Yay! UwU")
        isDefault.setWordWrap(True)
        mdbutton = QPushButton("Make NebiAluminium Default Browser", self)
        vboxp = QVBoxLayout()
        vboxp.addWidget(isNotDefault)
        vboxp.addWidget(isDefault)
        vboxp.addWidget(mdbutton)
        defaultbox.setLayout(vboxp)

        homeoptbox = QGroupBox("Homepage...")
        blankpage = QRadioButton("Blank Page - about:blank")
        defaultpage = QRadioButton("Default Homepage")
        custompage = QRadioButton("Custom page")
        urlbox = QLineEdit()
        urlbox.setPlaceholderText("Custom Homepage - Example: www.yoursite.com")
        vboxy = QVBoxLayout()
        vboxy.addWidget(blankpage)
        vboxy.addWidget(defaultpage)
        vboxy.addWidget(custompage)
        vboxy.addWidget(urlbox)
        homeoptbox.setLayout(vboxy)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(defaultbox)
        mainLayout.addWidget(homeoptbox)
        self.setLayout(mainLayout)


class AboutDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(AboutDialog, self).__init__(*args, **kwargs)

        layout = QVBoxLayout()

        title = QLabel("NebiAluminium Browser")
        font = title.font()
        font.setBold(True)
        title.setFont(font)

        logo = QLabel()
        logo.setPixmap(
            QPixmap("icon.ico"))
        layout.addWidget(logo)

        layout.addWidget(title)

        desc = QLabel(
            "A simple but powerful, optimised and open-source web browser for your daily use.")
        desc.setAlignment(Qt.AlignCenter)
        desc.setWordWrap(True)
        layout.addWidget(QLabel("Version: 2.0"))
        layout.addWidget(desc)
        layout.addWidget(QLabel("Licensed under GNU LGPL V3."))
        layout.addWidget(QLabel("Copyright (C) 2022 NebiSoft."))

        for i in range(0, layout.count()):
            layout.itemAt(i).setAlignment(Qt.AlignHCenter)

        self.setLayout(layout)


class SettingsDialog(QWidget):

    def __init__(self, parent):
        super().__init__(parent)

        self.parenti = parent

        self.setStyleSheet("""
    QTabWidget::tab-bar {
            alignment: center;
        }
        QTabBar::scroller { /* the width of the scroll buttons */
    width: 64px;
}
        """)

        dlgLayout = QVBoxLayout()

        # Create a form layout and add widgets

        formLayout = QHBoxLayout()
        space1 = QWidget()
        formLayout.addWidget(space1, 14)

        tabs = QTabWidget()
        tabs.setTabPosition(QTabWidget.South)
        # tabs.addTab(SettingsGeneral(), "General")
        tabs.addTab(SettingsPrivacy(parent), "Privacy and Safety")
        tabs.addTab(SettingsLookAndFeel(), "Look and Feel")
        tabs.addTab(SettingsSearch(), "Searching")
        # tabs.addTab(QLabel("xyz"), "Customize Homepage")
        # tabs.addTab(QLabel("xyz"), "NebiID")
        tabs.addTab(AboutDialog(), "About")
        # tabs.addTab(QLabel("xyz"), "Expermental")
        formLayout.addWidget(tabs, 75)
        space2 = QWidget()
        formLayout.addWidget(space2, 14)
        # Set the layout on the dialog

        dlgLayout.addLayout(formLayout)

        self.setLayout(dlgLayout)


class ArchiveHistory(QWidget):
    def load(self, i):
        self.parent.tab_widget.create_tab()
        self.parent.tab_widget.currentWidget().load(QUrl(i))

    def remove(self, i):
        print(self.listview.selectedItems()[0].text())
        self.history.remove(self.listview.selectedItems()[0].text())
        self.settingsprofile.setValue("history", self.history)
        self.history = self.settingsprofile.value("history", [""])
        self.listview.clear()
        for i in self.history:
            self.listview.addItem(i)

    def rightMenuShow(self, i):
        rightMenu = QMenu(self.listview)
        removeAction = QAction("Delete")
        removeAction.triggered.connect(lambda _: self.remove(i))

        rightMenu.addAction(removeAction)
        rightMenu.exec_(QCursor.pos())

    def __init__(self, parent):
        super().__init__()

        self.parent = parent

        self.listview = QListWidget()
        self.listview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listview.customContextMenuRequested[QPoint].connect(self.rightMenuShow)

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')

        self.history = self.settingsprofile.value("history", [""])

        for i in self.history:
            self.listview.addItem(i)

        self.listview.itemDoubleClicked.connect(lambda _: self.load(self.listview.currentItem().text()))
        vbox = QVBoxLayout()
        vbox.addWidget(self.listview)
        self.setLayout(vbox)


class ArchiveBookmarks(QWidget):
    def load(self, i):
        self.parent.tab_widget.create_tab()
        self.parent.tab_widget.currentWidget().load(QUrl(i))

    def remove(self, i):
        print(self.listview.selectedItems()[0].text())
        self.history.remove(self.listview.selectedItems()[0].text())
        self.settingsprofile.setValue("bookmarks", self.history)
        self.history = self.settingsprofile.value("bookmarks", ["https://nebisoftware.com",
                                                                  "https://social.nebisoftware.com",
                                                                  "https://cloud.nebisoftware.com",
                                                                  "https://duckduckgo.com",
                                                                  "https://nebisoftware.com/privacy.html"])


        self.listview.clear()
        for i in self.history:
            self.listview.addItem(i)

    def rightMenuShow(self, i):
        rightMenu = QMenu(self.listview)
        removeAction = QAction("Delete")
        removeAction.triggered.connect(lambda _: self.remove(i))

        rightMenu.addAction(removeAction)
        rightMenu.exec_(QCursor.pos())

    def __init__(self, parent):
        super().__init__()

        self.parent = parent

        self.listview = QListWidget()
        self.listview.setContextMenuPolicy(Qt.CustomContextMenu)
        self.listview.customContextMenuRequested[QPoint].connect(self.rightMenuShow)

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')

        self.history = self.settingsprofile.value("bookmarks", ["https://nebisoftware.com",
                                                                  "https://social.nebisoftware.com",
                                                                  "https://cloud.nebisoftware.com",
                                                                  "https://duckduckgo.com",
                                                                  "https://nebisoftware.com/privacy.html"])

        for i in self.history:
            self.listview.addItem(i)

        self.listview.itemDoubleClicked.connect(lambda _: self.load(self.listview.currentItem().text()))
        vbox = QVBoxLayout()
        vbox.addWidget(self.listview)
        self.setLayout(vbox)


class ArchiveDialog(QWidget):

    def __init__(self, parent):
        super().__init__()

        self.setWindowTitle("Archive")

        dlgLayout = QVBoxLayout()

        # Create a form layout and add widgets

        formLayout = QHBoxLayout()
        space1 = QWidget()
        formLayout.addWidget(space1, 14)

        tabs = QTabWidget()
        tabs.setTabPosition(QTabWidget.South)
        tabs.addTab(ArchiveHistory(parent), "History")
        tabs.addTab(ArchiveBookmarks(parent), "Bookmarks")
        formLayout.addWidget(tabs, 75)
        space2 = QWidget()
        formLayout.addWidget(space2, 14)
        # Set the layout on the dialog

        dlgLayout.addLayout(formLayout)

        self.setLayout(dlgLayout)


class MyWebEnginePage(QWebEnginePage):
    def acceptNavigationRequest(self, url, _type, isMainFrame):
        return QWebEnginePage.acceptNavigationRequest(self, url, _type, isMainFrame)


class WebView(QWebEngineView):
    def createWindow(self, type_):
        if not isinstance(self.window(), Browser):
            return

        if type_ == QWebEnginePage.WebBrowserTab:
            return self.window().tab_widget.create_tab()

    def onFullScreen(self, request):
        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')
        print(bool(self.settingsprofile.value("full_screen_enabled", True)))
        if bool(self.settingsprofile.value("full_screen_enabled", True)) == True:
            isFullscreen = request.toggleOn()
            request.accept()
            if (isFullscreen == True):
                self.window().tab_widget.tabBar().hide()
                self.window().showFullScreen()

            if (isFullscreen == False):
                self.window().tab_widget.tabBar().show()
                self.window().showNormal()
        else:
            request.reject()


class TabBar(QTabBar):
    def tabSizeHint(self, index):
        size = QTabBar.tabSizeHint(self, index)
        try:
            if self.count() < 6:
                if not self.window().isMobile:
                    w = int(((self.window().width() - self.window().trWidget.width() * 2) / self.count()) / 1.5)
                else:
                    w = int(self.window().width() / self.count())
                return QSize(w, size.height())
            else:
                return size
        except Exception as e:
            print("Error: " + str(e))
            return size

    def __init__(self, parent):
        super().__init__(parent)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)

        self.Parent = parent

        self.edit_parent = QWidget(self)
        self.edit_parent.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        self.lay = QHBoxLayout(self)
        self.lay.setContentsMargins(0, 0, 0, 0)
        self.edit_parent.setLayout(self.lay)
        self._editor = QLineEdit(self)
        self._editor.setTextMargins(24, 0, 48, 0)
        self._editor.setPlaceholderText("Search something...")
        self._editor.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.edit_parent.setWindowFlags(Qt.Popup | Qt.NoDropShadowWindowHint)
        self.edit_parent.setFocusProxy(self)
        self._editor.editingFinished.connect(self.handleEditingFinished)
        self._editor.installEventFilter(self)
        self.lay.addWidget(self._editor, 100)

        self.trWidget = QWidget()
        topRight = QHBoxLayout()
        topRight.setContentsMargins(11, 11, 11, 11)
        self.btnArc = QPushButton(QIcon.fromTheme("bookmark-new-list"), "")
        self.btnArc.clicked.connect(lambda: self.window().archive())
        self.btnSet = QPushButton(QIcon.fromTheme("settings"), "")
        self.btnSet.clicked.connect(lambda: self.window().settings())
        self.btnArc.setFlat(True)
        self.btnSet.setFlat(True)
        topRight.addWidget(self.btnArc)
        topRight.addWidget(self.btnSet)
        self.trWidget.setLayout(topRight)
        self.lay.addWidget(self.trWidget, 0)

    def handleEditingFinished(self):
        index = self.currentIndex()
        if index >= 0:
            self.edit_parent.hide()
            if self._editor.text() != "":
                self.window().loadnewurl(self._editor.text())

    def eventFilter(self, widget, event):
        if ((event.type() == QEvent.MouseButtonPress and
             not self._editor.geometry().contains(event.globalPos())) or
                (event.type() == QEvent.KeyPress and
                 event.key() == Qt.Key_Escape)):
            self.edit_parent.hide()
            return False
        return super().eventFilter(widget, event)

    def mouseDoubleClickEvent(self, event):
        index = self.tabAt(event.pos())
        if index >= 0:
            self.editTab(index)

    def editTab(self, index):
        rect = self.tabRect(index)
        startPoint = self.parent().mapToGlobal(QPoint(int(self.window().width() / 2) - int(rect.width() / 2) - 4, 0))
        startSize = QSize(rect.width(), rect.height())
        finalPoint = self.parent().mapToGlobal(QPoint(0, 0))
        finalSize = QSize(self.window().width(), rect.height())
        self._editor.setBaseSize(startSize)
        self.edit_parent.move(startPoint)
        if not self.edit_parent.isVisible():
            self.edit_parent.show()
            self.anim1 = QPropertyAnimation(self.edit_parent, b"pos")
            self.anim1.setEasingCurve(QEasingCurve.InOutCubic)
            self.anim1.setEndValue(finalPoint)
            self.anim1.setDuration(500)
            self.anim2 = QPropertyAnimation(self.edit_parent, b"size")
            self.anim2.setEasingCurve(QEasingCurve.InOutCubic)
            self.anim2.setStartValue(startSize)
            self.anim2.setEndValue(finalSize)
            self.anim2.setDuration(500)
            self.anim3 = QPropertyAnimation(self.trWidget, b"size")
            self.anim3.setEasingCurve(QEasingCurve.InOutCubic)
            self.anim3.setStartValue(QSize(0, rect.height()))
            self.anim3.setEndValue(QSize(75, rect.height()))
            self.anim3.setDuration(500)
            self.anim_group = QParallelAnimationGroup()
            self.anim_group.addAnimation(self.anim1)
            self.anim_group.addAnimation(self.anim2)
            self.anim_group.addAnimation(self.anim3)
            self.anim_group.start()
        self._editor.setFocus()
        self._editor.selectAll()

    @property
    def editor(self):
        return self._editor


class TabWidget(QTabWidget):

    def create_tab(self):
        view = WebView()
        page = MyWebEnginePage(profile, view)
        view.setPage(page)
        view.settings().setAttribute(QWebEngineSettings.FullScreenSupportEnabled, True)
        view.page().fullScreenRequested.connect(lambda request: view.onFullScreen(request))
        view.page().profile().setHttpUserAgent(
            "Mozilla/5.0 (X11; NebiOS; Linux x86_64; rv:91.0) Gecko/20100101 AluminiumEngine/2.0 NebiAluminium/2.0 Firefox/98")
        view.page().setBackgroundColor(Qt.transparent)
        view.page().featurePermissionRequested.connect(self.window().onFeaturePermissionRequested)
        settings = view.settings().globalSettings()
        view.loadStarted.connect(lambda _="": self.window().mw.setStyleSheet("/* ACCENT_MODE */ #mw { background-color: rgba(255,255,255, 0.93); }"))
        view.urlChanged.connect(lambda qurl, browser=view:
                                self.window().update_urlbar(qurl, view))
        view.loadFinished.connect(self.window().onLoadFinished)

        index = self.addTab(view, "...")
        self.setTabIcon(index, view.icon())
        view.titleChanged.connect(
            lambda title, view=view: self.update_title(view, title)
        )
        view.iconChanged.connect(lambda icon, view=view: self.update_icon(view, icon))
        self.setCurrentWidget(view)
        return view

    def update_title(self, view, title):
        index = self.indexOf(view)
        self.setTabText(index, title)

    def update_icon(self, view, icon):
        index = self.indexOf(view)
        self.setTabIcon(index, icon)

    def __init__(self):
        super().__init__()
        self.settingsprof = QSettings('NebiSoft', 'NebiAluminium')
        self.setTabBar(self.tab_bar)

    @cached_property
    def tab_bar(self):
        return TabBar(self.window())


class Browser(QMainWindow):
    def initWidgets(self):
        self.tlWidget = QWidget()
        self.topLeft = QHBoxLayout()
        self.topLeft.setContentsMargins(16, 11, 16, 11)
        self.back_btn = QPushButton(QIcon.fromTheme("go-previous"), "")
        self.back_btn.clicked.connect(lambda: self.tab_widget.currentWidget().back())
        self.next_btn = QPushButton(QIcon.fromTheme("go-next"), "")
        self.next_btn.clicked.connect(lambda: self.tab_widget.currentWidget().forward())
        self.back_btn.setFlat(True)
        self.next_btn.setFlat(True)
        self.topLeft.addWidget(self.back_btn)
        self.topLeft.addWidget(self.next_btn)
        self.tlWidget.setLayout(self.topLeft)
        self.trWidget = QWidget()
        self.topRight = QHBoxLayout()
        self.topRight.setContentsMargins(16, 11, 16, 11)
        self.btnReload = QPushButton(QIcon.fromTheme("view-refresh"), "")
        self.btnReload.clicked.connect(lambda: self.tab_widget.currentWidget().reload())
        self.btnNewTab = QPushButton(QIcon.fromTheme("list-add"), "")
        self.btnNewTab.clicked.connect(lambda: self.tab_widget.create_tab())
        self.btnNewTab.clicked.connect(lambda: self.tab_widget.currentWidget().load(
            QUrl("file:" + os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                        "engine/html", "newtab.html").replace("\\", "/"))))
        self.btnReload.setFlat(True)
        self.btnNewTab.setFlat(True)
        self.topRight.addWidget(self.btnReload)
        self.topRight.addWidget(self.btnNewTab)
        self.trWidget.setLayout(self.topRight)

    def loadMobileLayout(self):
        self.isMobile = True
        self.mobileToolbar = QToolBar("Mobile Toolbar")
        self.mobileToolbar.setMovable(False)
        self.mobileToolbar.setContextMenuPolicy(Qt.PreventContextMenu)
        self.spaceItem = QWidget()
        self.spaceItem.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        self.mobileToolbar.addWidget(self.tlWidget)
        self.mobileToolbar.addWidget(self.spaceItem)
        self.mobileToolbar.addWidget(self.trWidget)
        self.addToolBar(Qt.BottomToolBarArea, self.mobileToolbar)

    def loadDesktopLayout(self):
        self.isMobile = False
        try:
            self.removeToolBar(self.mobileToolbar)
        except Exception as e:
            print("Error: " + str(e))

        self.tab_widget.setCornerWidget(self.tlWidget, Qt.TopLeftCorner)
        self.tab_widget.setCornerWidget(self.trWidget, Qt.TopRightCorner)

    def removeMenu(self, bool=True):
        self.menuBar().setVisible(bool)

    def closeEvent(self, e):
        # Write window size and position to config file
        self.settingsprofile.setValue("size", self.size())
        self.settingsprofile.setValue("pos", self.pos())
        session = []
        for i in range(0, self.tab_widget.count()):
            self.tab_widget.setCurrentIndex(i)
            session.append(self.tab_widget.currentWidget().url().toString())

        self.settingsprofile.setValue("session", session)
        e.accept()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.isMobile = False
        self.mw = QWidget()
        self.mw.setStyleSheet("/* ACCENT_MODE */ #mw { background-color: rgba(255,255,255, 0.93); }")
        self.mw.setObjectName("mw")
        self.mw.setLayout(QVBoxLayout())
        self.mw.layout().setContentsMargins(0,0,0,0)
        self.mw.layout().addWidget(self.tab_widget)
        self.setCentralWidget(self.mw)
        self.tab_widget.setElideMode(Qt.ElideRight)
        self.tab_widget.setDocumentMode(False)
        self.tab_widget.setMovable(True)
        self.tab_widget.tabCloseRequested.connect(self.close_current_tab)

        self.settingsprofile = QSettings('NebiSoft', 'NebiAluminium')
        self.search_engine = self.settingsprofile.value("search_engine", "https://duckduckgo.com/?q=")
        self.isDynamic = bool(self.settingsprofile.value("is_dynamic_toolbar_layout", True))
        self.menubtn = QMenu()
        self.homep = "file:" + os.path.join(
            str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""), "engine/html",
            "newtab.html").replace("\\", "/")

        # Initial window size/pos last saved. Use default values for first time
        self.resize(self.settingsprofile.value("size", QSize(800, 600)))
        self.move(self.settingsprofile.value("pos", QPoint(50, 50)))
        self.restoreState(self.settingsprofile.value("state", self.saveState()))

        self.tab_widget.setTabsClosable(True)
        self.tab_widget.currentChanged.connect(self.current_tab_changed)

        self.initWidgets()
        if (self.size().width() <= 768):
            self.loadMobileLayout()
        if (self.size().width() >= 769):
            self.loadDesktopLayout()

        self.history = self.settingsprofile.value("history", [""])
        self.bookmarks = self.settingsprofile.value("bookmarks", ["https://nebisoftware.com",
                                                                  "https://social.nebisoftware.com",
                                                                  "https://cloud.nebisoftware.com",
                                                                  "https://duckduckgo.com",
                                                                  "https://nebisoftware.com/privacy.html"])

        self.shortcutNewTab = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_T), self)
        self.shortcutNewTab.activated.connect(self.btnNewTab.click)
        self.shortcutNewTab.setEnabled(True)

        self.shortcutClsTab = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_W), self)
        self.shortcutClsTab.activated.connect(lambda i="": self.close_current_tab(self.tab_widget.currentIndex()))
        self.shortcutClsTab.setEnabled(True)

        self.shortcutFls = QShortcut(QKeySequence(Qt.Key_F11), self)
        self.shortcutFls.activated.connect(lambda _="": self.setFullScreen(not self.isFullScreen()))
        self.shortcutFls.setEnabled(True)

        self.shortcutNewWin = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_N), self)
        self.shortcutNewWin.activated.connect(lambda _="": Browser().show())
        self.shortcutNewWin.setEnabled(True)

        self.shortcutLink = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_L), self)
        self.shortcutLink.activated.connect(
            lambda _="": self.tab_widget.tab_bar.editTab(self.tab_widget.currentIndex()))
        self.shortcutLink.setEnabled(True)

        QIcon.setThemeName(self.settingsprofile.value("icon_theme", "Default"))
        self.loadTheme()

    def initArgs(self):
        args = sys.argv[1:]
        if args.__contains__("--reset-settings"):
            self.settingsprofile.clear()
            exit()

        tempargs = []
        for i in args:
            if i.startswith("--"):
                print(i)
            else:
                tempargs.insert(len(tempargs) + 1, i)
        print(tempargs)
        args = tempargs

        if (len(args) == 0):
            session = self.settingsprofile.value("session", ["file:" + os.path.join(
                str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""), "engine/html",
                "welcome.html").replace("\\", "/")])
            print("curr session: " + str(session))
            for i in session:
                view = self.tab_widget.create_tab()
                view.load(QUrl(i))

        for i in args:
            contains = i.upper().__contains__("HTTPS:") or i.upper().__contains__("HTTP:") or i.upper().__contains__(
                "FILE:")
            isexists = os.path.exists(i)
            new_i = i
            if (isexists == True):
                new_i = "file:" + new_i
            if (contains == False and isexists == False):
                new_i = "https://" + new_i
            view = self.tab_widget.create_tab()
            view.load(QUrl(new_i))

    def setFullScreen(self, i):
        if i == True:
            self.showFullScreen()
        else:
            self.showNormal()

    def loadnewurl(self, link):
        print(link)
        q = QUrl(link)
        engine_location = str(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip", ""),
                                           "engine")).replace("\\", "/")

        if (q.toString() == "aluminium://"):
            q = QUrl(str(q.toString().replace("aluminium://", "file://" + engine_location + "/html/")) + "index.html")
        elif (q.toString().startswith("aluminium:/")):
            q = QUrl(str(q.toString().replace("aluminium://", "file://" + engine_location + "/html/")) + ".html")

        if (q.scheme() == ""):
            if (q.toString().__contains__(".")):
                if (q.toString().__contains__(" ")):
                    q = QUrl(self.search_engine + link)
                else:
                    q = QUrl("http://" + link)
            else:
                q = QUrl(self.search_engine + link)
        if (link != ""):
            if self.tab_widget.tabBar()._editor.text() != "":
                self.tab_widget.currentWidget().setUrl(q)

    def close_current_tab(self, i):
        self.tab_widget.removeTab(i)
        if int(self.tab_widget.count()) < 1:
            self.tab_widget.create_tab()
            self.tab_widget.currentWidget().load(QUrl(
                "file:" + os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                       "engine/html", "newtab.html").replace("\\", "/")))

    def current_tab_changed(self):
        self.window().mw.setStyleSheet("/* ACCENT_MODE */ #mw { background-color: rgba(255,255,255, 0.93); }")
        self.loadTheme()
        if not self.contains("google.com") or self.contains("youtube.com") or self.contains(
                "facebook.com") or self.contains("instagram.com"):
            self.tab_widget.setStyleSheet(self.tab_widget.styleSheet() + """QTabBar::tab:selected{
                        background: #ff5f5f;
                    }""")

        if (self.tab_widget.tabText(self.tab_widget.currentIndex()) == "Settings"):
            self.tab_widget.tabBar().editor.setEnabled(False)
        elif (self.tab_widget.tabText(self.tab_widget.currentIndex()) == "Archive"):
            self.tab_widget.tabBar().editor.setEnabled(False)
        else:
            self.tab_widget.tabBar().editor.setEnabled(True)

        try:
            q = self.tab_widget.currentWidget().url()
        except:
            q = QUrl("")
        link_string = str(q.toString())
        engine_location = str(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip", ""),
                                           "engine")).replace("\\", "/")

        self.this_is_engine_page = False
        if (link_string.__contains__(engine_location)):
            self.this_is_engine_page = True
            link_string = link_string.replace(engine_location, "aluminium://").replace(".html", "").replace("/html/",
                                                                                                            "")[7:]
            if link_string.startswith("/aluminium"):
                link_string = link_string[1:]
        else:
            self.this_is_engine_page = False
            link_string = q.toString()

        self.loadTheme()

        print(QUrl(link_string).scheme())

        if q.scheme() == 'https':
            # Secure padlock icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/object-locked.svg);}")
        elif q.scheme() == 'http':
            # Insecure padlock icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/object-unlocked.svg); }")
        elif q.scheme() == 'file' and not QUrl(link_string).scheme() == "aluminium":
            # Local icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/text-rtf.svg);}")
        elif QUrl(link_string).scheme() == 'aluminium':
            # AluminiumEngine Page icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/network-connect.svg);}")
        else:
            # Unknown icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/paint-unknown.svg);}")

        if (link_string.startswith("aluminium:") == True):
            files = ["newtab", "privatebrowsing", "welcome"]

            if any(x in q.toString() for x in files):
                link_string = ""

        try:
            self.back_btn.setEnabled(self.tab_widget.currentWidget().history().canGoBack())
            self.next_btn.setEnabled(self.tab_widget.currentWidget().history().canGoForward())
        except Exception as e:
            print("Error: " + str(e))

        self.tab_widget.tab_bar._editor.setText(link_string)
        try:
            if self.mw.styleSheet().__contains__("/* ACCENT_MODE */"):
                self.get_accent()
            else:
                print("Current theme overrides the window color.")
        except Exception as e:
            pass

    @cached_property
    def tab_widget(self):
        return TabWidget()

    def update_urlbar(self, q, browser=None):
        link_string = str(q.toString())

        engine_location = str(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip", ""),
                                           "engine")).replace("\\", "/")

        if link_string.startswith("aluminium:"):
            self.tab_widget.currentWidget().setUrl(QUrl(link_string.replace("aluminium://", engine_location)))

        self.this_is_engine_page = False
        if (link_string.__contains__(engine_location)):
            self.this_is_engine_page = True
            link_string = str(q.toString())[7:]
            link_string = link_string.replace(engine_location, "aluminium://").replace(".html", "").replace("/html/",
                                                                                                            "")
            if link_string.startswith("/aluminium"):
                link_string = link_string[1:]
        else:
            self.this_is_engine_page = False
            link_string = q.toString()

        try:
            if link_string.startswith(engine_location) == False:
                if self.history[0] != link_string:
                    self.history.insert(0, link_string)
                    self.settingsprofile.setValue("history", self.history)
        except Exception as e:
            print("Error: " + str(e))

        try:
            self.back_btn.setEnabled(self.tab_widget.currentWidget().history().canGoBack())
            self.next_btn.setEnabled(self.tab_widget.currentWidget().history().canGoForward())
        except:
            print("-")

        link_string_full = link_string

        if (link_string.startswith("aluminium://") == True):
            files = ["newtab", "privatebrowsing", "welcome"]

            if any(x in link_string for x in files):
                link_string = ""

        if browser != self.tab_widget.currentWidget():
            # If this signal is not from the current tab, ignore
            return

        self.loadTheme()

        if q.scheme() == 'https':
            # Secure padlock icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/object-locked.svg);}")
        elif q.scheme() == 'http':
            # Insecure padlock icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/object-unlocked.svg); }")
        elif q.scheme() == 'file' and not QUrl(link_string_full).scheme() == "aluminium":
            # Local icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/text-rtf.svg);}")
        elif QUrl(link_string_full).scheme() == 'aluminium':
            # AluminiumEngine Page icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/network-connect.svg);}")
        else:
            # Unknown icon
            self.tab_widget.tabBar()._editor.setStyleSheet(
                self.tab_widget.tabBar()._editor.styleSheet() + "QLineEdit {background-image: url(ui/icons/Default/16/actions/paint-unknown.svg);}")

        self.tab_widget.tabBar()._editor.setText(link_string)
        self.link = link_string_full
        self.historyIsAdded = False

    def contains(self, text):
        return self.tab_widget.tabBar()._editor.text().__contains__(text)

    def get_accent(self):
        wb = self.tab_widget.currentWidget().page()
        def get_accent_color(src):
            try:
                rev1 = str(src).split('name="theme-color"')[1]
                rev1 = rev1.split(">")[0]
                rev1 = rev1.replace('content="', "").replace('"', "")
            except:
                rev1 = "rgba(255,255,255, 0.93)"
            finalcolor = rev1
            print(finalcolor)
            self.mw.setStyleSheet("/* ACCENT_MODE */ #mw { background-color: " + str(finalcolor) + "; }")
            self.setStyleSheet("")
            print(self.mw.styleSheet())
        wb.toHtml(get_accent_color)

    def onLoadFinished(self):
        self.loadTheme()
        print("Accent color mode = " + str(self.mw.styleSheet().__contains__("/* ACCENT_MODE */")))
        if self.mw.styleSheet().__contains__("/* ACCENT_MODE */"):
            self.get_accent()
        else:
            print("Current theme overrides the window color.")

        if self.contains("google.com") or self.contains("youtube.com") or self.contains(
                "facebook.com") or self.contains("instagram.com"):
            self.tab_widget.setStyleSheet(self.tab_widget.styleSheet() + """QTabBar::tab:selected{
                background: #ff5f5f;
            }""")
            self.tab_widget.setTabText(self.tab_widget.currentIndex(),
                                       "[PRIVACY ALERT] " + self.tab_widget.tabText(self.tab_widget.currentIndex()))
        if self.link == "aluminium://newtab" and self.historyIsAdded == False:
            items1 = self.history[:5]
            for item in items1:
                try:
                    reqs = requests.get(item)
                    soup = BeautifulSoup(reqs.text, 'html.parser')
                    for title in soup.find_all('title'):
                        theTitle = title.get_text()
                    self.tab_widget.currentWidget().page().runJavaScript(
                        "addHistory('" + item + "', '" + theTitle + "');")
                except:
                    if item.startswith("aluminium://"):
                        item = item.replace("aluminium://", "") + ".html"
                        titles = "(AluminiumEngine page) " + item
                    self.tab_widget.currentWidget().page().runJavaScript(
                        "addHistory('" + item + "', '" + titles + "');")
            items2 = self.bookmarks[:5]
            for item in items2:
                try:
                    reqs = requests.get(item)
                    soup = BeautifulSoup(reqs.text, 'html.parser')
                    for title in soup.find_all('title'):
                        theTitle = title.get_text()
                        print(theTitle)
                        if theTitle.__contains__("	NebiCloud"):
                            self.tab_widget.currentWidget().page().runJavaScript(
                                "addBookmark('" + item + "', '" + "NebiCloud Web" + "');")
                        else:
                            self.tab_widget.currentWidget().page().runJavaScript(
                                "addBookmark('" + item + "', '" + theTitle + "');")
                except:
                    if item.startswith("aluminium://"):
                        item = item.replace("aluminium://", "") + ".html"
                        titles = "(AluminiumEngine page) " + item
                    self.tab_widget.currentWidget().page().runJavaScript(
                        "addBookmark('" + item + "', '" + titles + "');")
            self.historyIsAdded = True

    def get_download_path(self):
        """Returns the default downloads path for linux or windows"""
        if os.name == 'nt':
            import winreg
            sub_key = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders'
            downloads_guid = '{374DE290-123F-4565-9164-39C4925E467B}'
            with winreg.OpenKey(winreg.HKEY_CURRENT_USER, sub_key) as key:
                location = winreg.QueryValueEx(key, downloads_guid)[0]
            return location
        else:
            return os.path.join(os.path.expanduser('~'), 'Downloads')

    @pyqtSlot("QWebEngineDownloadItem*")
    def on_downloadRequested(self, download):
        print(bool(self.settingsprofile.value("download_enabled", True)))
        if bool(self.settingsprofile.value("download_enabled", True)) == True:
            old_path = download.url().path()  # download.path()
            suffix = QFileInfo(old_path).suffix()
            path, _ = QFileDialog.getSaveFileName(
                self, "Download file", old_path, "*." + suffix
            )
            if path:
                print("Download to: " + path)
                download.setPath(path)
                download.accept()
                message = "        " + "Downloading file: " + path
                self.downloadToolbar = QToolBar("Download Toolbar")
                self.downloadToolbar.setContextMenuPolicy(Qt.PreventContextMenu)
                self.downloadToolbar.setStyleSheet("background-color: #ff5f5f;")
                self.titlelabel = QLabel(message)
                font = self.titlelabel.font()
                font.setBold(True)
                self.titlelabel.setFont(font)
                spaceItem = QWidget()
                spaceItem.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
                self.downloadToolbar.addWidget(self.titlelabel)
                self.downloadToolbar.addWidget(spaceItem)
                self.downloadToolbar.setMovable(False)
                self.downloadPauseRsmBtn = QPushButton("Pause")
                self.downloadPauseRsmBtn.setFlat(True)
                self.downloadPauseRsmBtn.clicked.connect(lambda _="": self.downloadPauseRsm(download))
                self.downloadToolbar.addWidget(self.downloadPauseRsmBtn)
                self.downloadDenyBtn = QPushButton("Cancel")
                self.downloadDenyBtn.clicked.connect(lambda _="": download.cancel())
                self.downloadToolbar.addWidget(self.downloadDenyBtn)
                self.addToolBar(Qt.TopToolBarArea, self.downloadToolbar)
                download.downloadProgress.connect(self.updateDownload)
                download.finished.connect(self.foo)

    def downloadPauseRsm(self, download):
        if download.isPaused() == False:
            download.pause()
            self.downloadPauseRsmBtn.setText("Resume")
        else:
            download.resume()
            self.downloadPauseRsmBtn.setText("Pause")

    def updateDownload(self, bytesReceived, bytesTotal):
        bytesReceived = bytesReceived / 1024 / 1024
        bytesTotal = bytesTotal / 1024 / 1024
        strBytesReceived = str(bytesReceived).split(".")[0]
        strBytesTotal = str(bytesTotal).split(".")[0]
        if bytesReceived > 999:
            bytesReceived = bytesReceived / 1024
            print(bytesTotal)
            strBytesReceived = str(bytesReceived)
        if bytesTotal > 999:
            bytesTotal = bytesTotal / 1024
            print(bytesTotal)
            strBytesTotal = str(bytesTotal)
        newStrRec = strBytesReceived[:3]
        if newStrRec.__contains__("."):
            newStrRec = newStrRec + " GB"
        else:
            newStrRec = newStrRec + " MB"
        newStrTot = strBytesTotal[:3]
        if newStrTot.__contains__("."):
            newStrTot = newStrTot + " GB"
        else:
            newStrTot = newStrTot + " MB"
        msg = self.titlelabel.text().split(" - ")[0] + " - " + newStrRec + "/" + newStrTot
        self.titlelabel.setText(msg)

    def foo(self):
        self.titlelabel.setText(self.titlelabel.text().split(" - ")[0] + " - Finished!")
        timer = QTimer()
        timer.timeout.connect(self.rmDwnTb)
        timer.timeout.connect(timer.stop)
        timer.setInterval(5000)
        timer.start()
        self.removeToolBar(self.downloadToolbar)
        print("Download finished sucessfully!")

    def rmDwnTb(self):
        self.removeToolBar(self.downloadToolbar)

    def settings(self):
        self.tab_widget.tabBar().edit_parent.hide()
        self.settingsDg = SettingsDialog(self.window())
        self.i = self.tab_widget.addTab(self.settingsDg, "Settings")
        self.tab_widget.setCurrentIndex(self.i)

    def archive(self):
        self.tab_widget.tabBar().edit_parent.hide()
        self.arcDg = ArchiveDialog(self.window())
        self.i = self.tab_widget.addTab(self.arcDg, "Archive")
        self.tab_widget.setCurrentIndex(self.i)

    def loadTheme(self):
        try:
            with open(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                   "ui/stylesheet/", self.settingsprofile.value("qss_theme_name", "Default"),
                                   "TabBar_Layout.qss")) as f:
                self.tab_widget.tabBar().edit_parent.setStyleSheet(f.read())
            with open(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                   "ui/stylesheet/", self.settingsprofile.value("qss_theme_name", "Default"),
                                   "Link_Bar.qss")) as f:
                self.tab_widget.tabBar()._editor.setStyleSheet(f.read())
            with open(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                   "ui/stylesheet/", self.settingsprofile.value("qss_theme_name", "Default"),
                                   "Browser_Tabs.qss")) as f:
                self.tab_widget.setStyleSheet(f.read())
            with open(os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                   "ui/stylesheet/", self.settingsprofile.value("qss_theme_name", "Default"),
                                   "Main_Window.qss")) as f:
                self.mw.setStyleSheet(f.read())
                if self.mw.styleSheet().__contains__("/* ACCENT_MODE */"):
                    self.mw.setStyleSheet("/* ACCENT_MODE */ #mw { background-color: rgba(255,255,255, 0.93); }")
        except Exception as e:
            print(e)

    def onFeaturePermissionRequested(self, url, feature):
        if bool(self.settingsprofile.value("permissions_enabled", True)) == True:
            denybtn = QAction("Deny", self)
            acceptbtn = QPushButton("Accept", self)
            tbdiag = QToolBar("warn_dialog")
            denybtn.triggered.connect(
                lambda _: self.tab_widget.currentWidget().page().setFeaturePermission(url, feature,
                                                                                      QWebEnginePage.PermissionDeniedByUser))
            acceptbtn.clicked.connect(
                lambda _: self.tab_widget.currentWidget().page().setFeaturePermission(url, feature,
                                                                                      QWebEnginePage.PermissionGrantedByUser))
            denybtn.triggered.connect(lambda _: tbdiag.deleteLater())
            acceptbtn.clicked.connect(lambda _: tbdiag.deleteLater())

            print(feature)
            if feature == self.tab_widget.currentWidget().page().Notifications:
                self.warnMessageBanner(str(url.toString()) + " wants send notifications.", acceptbtn, denybtn, tbdiag)
            elif feature == self.tab_widget.currentWidget().page().Geolocation:
                self.warnMessageBanner(str(url.toString()) + " wants know your location.", acceptbtn, denybtn, tbdiag)
            elif feature == 3:
                self.warnMessageBanner(str(url.toString()) + " wants use the webcam.", acceptbtn, denybtn, tbdiag)
            elif feature == 2:
                self.warnMessageBanner(str(url.toString()) + " wants use the microphone.", acceptbtn, denybtn, tbdiag)
            elif feature == 4:
                self.warnMessageBanner(str(url.toString()) + " wants use the webcam and microphone.", acceptbtn, denybtn, tbdiag)
            elif feature == 5:
                self.warnMessageBanner(str(url.toString()) + " wants lock your cursor.", acceptbtn, denybtn, tbdiag)
            else:
                self.warnMessageBanner(str(url.toString()) + " wants use a feature of the browser.", acceptbtn, denybtn, tbdiag)

    def warnMessageBanner(self, message, acceptbtn, denybtn, tbdiag):
        message = "        " + message
        tbdiag.setContextMenuPolicy(Qt.PreventContextMenu)
        tbdiag.setStyleSheet("background-color: #ff5f5f;")
        titlelabel = QLabel(message)
        font = titlelabel.font()
        font.setBold(True)
        titlelabel.setFont(font)
        spaceItem = QWidget()
        spaceItem.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        tbdiag.addWidget(titlelabel)
        tbdiag.addWidget(spaceItem)
        tbdiag.addAction(denybtn)
        tbdiag.addWidget(acceptbtn)
        tbdiag.setMovable(False)
        self.addToolBar(Qt.TopToolBarArea, tbdiag)


margs = sys.argv
margs.insert(len(margs) + 1,"--enable-smooth-scrolling")
app = QApplication(margs)


def main():
    font = QApplication.font("QMenu")
    font.setFamily("Karla")
    app.setFont(font)
    app.setApplicationName("NebiAluminium")
    app.setApplicationDisplayName("NebiAluminium")
    app.setOrganizationName("NebiSoft")
    app.setWindowIcon(QIcon("icon.ico"))
    w = Browser()
    w.show()
    profile.downloadRequested.connect(lambda download: w.on_downloadRequested(download))

    if platform == "linux" and os.path.exists("/System"):
        print("NebiOS platform.")
    else:
        exit("YOUR OS IS NOT NEBIOS!")

    return w

if __name__ == "__main__":
    icon = QIcon.fromTheme('back')
    print("Icon Theme: " + str(icon.themeName()))
    profile = QWebEngineProfile.defaultProfile()
    interceptor = RequestManager()
    profile.setUrlRequestInterceptor(interceptor)

    QIcon.setThemeSearchPaths([os.path.join(str(sys.path[0]).replace("lib/library.zip", "").replace("base_library.zip/", ""),
                                            "ui/icons")])
    print("Output of QtGui.QIcon.themeSearchPaths(): " + str(QIcon.themeSearchPaths()))

    sys.exit(app.exec_())

while True:
    app.processEvents()
