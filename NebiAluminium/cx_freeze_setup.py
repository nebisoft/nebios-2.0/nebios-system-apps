from cx_Freeze import setup, Executable
buildOptions = dict(packages = [], excludes = [])

import sys
base = 'Win32GUI' if sys.platform =='win32' else None

executables = [
    Executable('NebiAluminium.py',
    base=base,
    icon = "icon.ico" )
              ]

buildOptions = dict(include_files = ['engine/', 'ui/'])

setup(
    name='NebiAluminium',
    version = '2.0',
    description = 'NebiAluminium',
    options = dict(build_exe = buildOptions),
    executables = executables
    )
