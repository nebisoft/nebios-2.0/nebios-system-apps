#!/usr/bin/python3
import os
import psutil
import gi
gi.require_version("Gtk", "3.0")
gi.require_version('GtkSource', '4')
from gi.repository import GtkSource, Gtk, GObject
from gi.overrides import Gtk, GObject

class MyApp(object):

    def __init__(self):

        self.builder = Gtk.Builder()
        self.glade_file = "ui/app.glade"
        GObject.type_register(GtkSource.View)
        self.builder.add_from_file(self.glade_file)

        self.loading = self.builder.get_object("Loading...")
        self.loading.connect("destroy", exit)
        self.loading.show_all()

        self.mbname = os.popen("cat /sys/devices/virtual/dmi/id/board_name").read()[:-1]
        self.mbvendor = os.popen("cat /sys/devices/virtual/dmi/id/board_vendor").read()[:-1]
        self.mbmodel = self.mbvendor + " " + self.mbname
        self.nebios_full = os.popen("echo $(lsb_release -ds | sed 's+ .\.. + \"+g')\\\"").read()[:-1]
        self.nebios_ver = os.popen("lsb_release -rs").read()[:-1]
        self.kernel = os.popen("uname -r").read()[:-1]
        self.cpu = os.popen("cat /proc/cpuinfo | grep 'model name' | uniq").read()[:-1].replace("model name	: ", "")
        self.ram = str(int(os.popen("grep MemTotal /proc/meminfo | awk '{print $2}'").read()[:-1]) / 1024 / 1024)[:4]
        self.gpu = os.popen("lspci -v | grep 'VGA compatible controller'").read()[:-1].split("VGA compatible controller: ")[1].split(" (")[0]
        if self.gpu.lower().startswith("nvidia"):
            self.gpu = "NVIDIA " + self.gpu.split("[")[1].replace("]", "").split(" Re")[0]
        self.bootdisk = "sd" + str(os.popen("lsblk | grep boot").read()[:-1].split("sd")[1])[0]
        self.bootdiskmodel = os.popen("udisksctl status | grep " + self.bootdisk).read()[:-1].split("  ")[0]
        self.systemsize = 13
        self.appsize = str(self.getFolderSize("/Applications") / 1024 / 1024 / 1024)[:5]
        self.allsize = str(psutil.disk_usage('/').used / 1024 / 1024 / 1024).split(".")[0]
        self.percentsize = psutil.disk_usage('/').percent

        print("Kernel Version: " + str(self.kernel))
        print("CPU: " + str(self.cpu))
        print("RAM: " + str(self.ram) + " GB")
        print("GPU: " + str(self.gpu))
        print("Boot Disk: " + str(self.bootdisk))
        print("Boot Disk Model: " + str(self.bootdiskmodel))
        print("System Size: " + str(self.systemsize) + " GB")
        print("Apps Size: " + str(self.appsize) + " GB")
        print("All HDD Size: " + str(self.allsize) + " GB")
        print("All HDD Size In Percent: " + str(self.percentsize) + "%")

        self.loading.hide()
        self.window = self.builder.get_object("MainWin")
        self.window.connect("destroy", exit)

        self.builder.get_object("MotherboardModel").set_text(self.mbmodel)
        self.builder.get_object("OsVer").set_text(self.nebios_ver)
        self.builder.get_object("KernelVer").set_text(self.kernel)
        self.builder.get_object("FullDistName").set_text(self.nebios_full)
        self.builder.get_object("CpuId").set_text(self.cpu)
        self.builder.get_object("RamId").set_text(str(self.ram) + " GB")
        self.builder.get_object("GpuId").set_text(self.gpu)
        self.builder.get_object("HddModel").set_text(self.bootdiskmodel)
        self.builder.get_object("ContentSize").set_text("System: " + str(self.systemsize) + " GB - Apps: " + str(self.appsize) + " GB - All: " + str(self.allsize) + " GB")
        self.builder.get_object("HddUsage").set_value(self.percentsize)
        self.builder.get_object("GpuId2").set_text(self.gpu)

        self.builder.get_object("GpuDrivers").connect("clicked", self.DriversDialog)
        if self.gpu.lower().__contains__("ati") or self.gpu.lower().__contains__("radeon"):
        	self.builder.get_object("GpuDrivers").set_label("You don't need to install drivers")

        self.window.show_all()

    def DriversDialog(self, btn):
        if self.gpu.lower().__contains__("amd"):
            os.system("/usr/bin/software-properties-gtk --open-tab=4 &")
        else:
            os.system("/usr/bin/software-properties-gtk --open-tab=4 &")

    def getFolderSize(self, folder):
        Folderpath = folder
        size = 0

        # get size
        for path, dirs, files in os.walk(Folderpath):
            for f in files:
                fp = os.path.join(path, f)
                try:
                    size += os.path.getsize(fp)
                except:
                    pass
        return size

    def goBack(self, btn):
        pages = self.stack.get_children()
        cur_page = self.stack.get_visible_child()
        i = pages.index(cur_page)
        if i == 0: return
        self.stack.set_visible_child(pages[i - 1])

    def goNext(self, btn):
        pages = self.stack.get_children()
        cur_page = self.stack.get_visible_child()
        i = pages.index(cur_page)
        if i == len(pages) - 1: self.endWelcome()
        self.stack.set_visible_child(pages[i + 1])

    def endWelcome(self):
        os.system(self.restartWMcmd)
        exit(0)

if __name__ == '__main__':
    try:
        gui = MyApp()
        Gtk.main()
    except KeyboardInterrupt:
        pass
