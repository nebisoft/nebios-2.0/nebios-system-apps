IS_SOFTWARE_RENDER="False"

set -e

if [[ "$2" == "--software-render" ]]
then
  IS_SOFTWARE_RENDER="True"
fi

echo "Is software render? $IS_SOFTWARE_RENDER"

pkexec waydroid init -s $1