#!/usr/bin/env python3
import os
import subprocess
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Vte, GLib, GdkPixbuf

class FirstSetup:
    def __init__(self):
        self.VARIANT = "VANILLA"
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/main.glade")

        self.Window:Gtk.Window = self.builder.get_object("FirstSetup_Win")
        self.Window.connect("destroy", lambda _: Gtk.main_quit())
        self.Stack:Gtk.Stack = self.builder.get_object("FirstSetup_Stack")
        self.StackBackBtn:Gtk.Button = self.builder.get_object("FirstSetup_Back")
        self.ExtraSoftRenderCheck:Gtk.CheckButton = self.builder.get_object("FirstSetup_ExtraSoftRenderCheck")
        self.VteContainer:Gtk.Box = self.builder.get_object("FirstSetup_VteContainer")

        StartButton:Gtk.Button = self.builder.get_object("FirstSetup_StartButton")
        VanillaInstallButton:Gtk.Button = self.builder.get_object("FirstSetup_VanillaInstall")
        GappsInstallButton:Gtk.Button = self.builder.get_object("FirstSetup_GappsInstall")
        ExtraNextButton:Gtk.Button = self.builder.get_object("FirstSetup_ExtraNextButton")
        self.InstallNextButton:Gtk.Button = self.builder.get_object("FirstSetup_InstallNextButton")
        RegisterGenerateButton:Gtk.Button = self.builder.get_object("FirstSetup_RegisterGenerateButton")
        RegisterNextButton:Gtk.Button = self.builder.get_object("FirstSetup_RegisterNextButton")
        self.InstallTitleLabel:Gtk.Label = self.builder.get_object("FirstSetup_InstallTitleLabel")
        self.GoogleCodeEntry:Gtk.Entry = self.builder.get_object("FirstSetup_GoogleCodeEntry")

        self.StackBackBtn.connect("clicked", lambda _: self.StackBack())
        StartButton.connect("clicked", lambda _: self.Stack.set_visible_child_name("1"))
        VanillaInstallButton.connect("clicked", lambda _: self.SetVariant("VANILLA"))
        GappsInstallButton.connect("clicked", lambda _: self.SetVariant("GAPPS"))
        ExtraNextButton.connect("clicked", lambda _: self.StartInstall())
        RegisterGenerateButton.connect("clicked", lambda _: self.RegisterDevice())
        self.InstallNextButton.connect("clicked", lambda _: self.Stack.set_visible_child_name("4"))
        RegisterNextButton.connect("clicked", lambda _: self.FinishWizard())

        self.Window.show()

    def FinishWizard(self):
        self.Window.hide()


    def RegisterDevice(self):
        self.Stack.set_visible_child_name("4")
        try:
            completed_process = subprocess.run(['bash', "./scripts/code.sh"], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                               text=True, check=True)

            # Get the standard output and standard error (if any) from the completed process
            script_output = completed_process.stdout
            script_error = completed_process.stderr

            # Print the script's output and error (if any)
            self.GoogleCodeEntry.set_text(script_output.split("\nCODE=")[1][:-1])

        except subprocess.CalledProcessError as e:
            # If the script exits with a non-zero status code, you can handle it here
            self.GoogleCodeEntry.set_text("Error " + e.returncode)

    def StartInstall(self):
        # Create a Vte.Terminal widget
        terminal = Vte.Terminal()

        # Create a list of SpawnFlags
        spawn_flags = [GLib.SpawnFlags.DEFAULT]

        # Replace "/bin/bash" with the actual command you want to run
        command = ["/usr/bin/env", "bash", os.environ["PWD"] + "/scripts/init.sh", self.VARIANT]

        if self.ExtraSoftRenderCheck.get_active() == True:
            command = ["/usr/bin/env", "bash", os.environ["PWD"] + "/scripts/init.sh", self.VARIANT, "--software-render"]

        print("xdroid: command is", command)

        # Spawn the terminal
        terminal.spawn_sync(
            Vte.PtyFlags.DEFAULT,
            None,
            command,
            None,
            GLib.SpawnFlags.DEFAULT,
        )

        terminal.connect("child-exited", self.on_vte_exited)

        # Pack the terminal widget into the VteContainer
        self.VteContainer.pack_start(terminal, True, True, 0)
        self.VteContainer.show_all()

        # After adding the terminal, you can make InstallNextButton sensitive
        self.InstallNextButton.set_sensitive(False)
        self.StackBackBtn.set_visible(False)
        self.Stack.set_visible_child_name("3")

    def on_vte_exited(self, terminal, status):
        if status != 0:
            self.InstallTitleLabel.set_label("Error: " + str(status))
        else:
            self.InstallTitleLabel.set_label("Install completed!")
            self.InstallNextButton.set_sensitive(True)

    def StackBack(self):
        stack = self.Stack
        index = int(stack.get_visible_child_name())
        if index > 0:
            stack.set_visible_child_name(str(index - 1))

    def SetVariant(self, name):
        print("setting self.VARIANT (", self.VARIANT, ") to ", name)
        self.VARIANT = name
        self.Stack.set_visible_child_name("2")

class TrayIcon:
    def __init__(self):
        self.window = Gtk.Window()
        self.window.set_default_size(200, 100)
        self.window.connect("delete-event", Gtk.main_quit)

        self.statusicon = Gtk.StatusIcon()
        self.statusicon.set_from_file("icon.png")  # Replace with your icon file

        self.menu = Gtk.Menu()

        # Launch Xdroid Android Emulator
        self.launch_item = Gtk.MenuItem("Launch Xdroid")
        self.launch_item.connect("activate", self.launch_xdroid)
        self.menu.append(self.launch_item)

        # Switch to Xdroid Session (disabled)
        self.switch_item = Gtk.MenuItem("Switch to Xdroid Session")
        self.switch_item.set_sensitive(False)
        self.menu.append(self.switch_item)

        # Kill Xdroid Session
        kill_item = Gtk.MenuItem("Kill Xdroid Session")
        kill_item.connect("activate", self.kill_xdroid_session)
        self.menu.append(kill_item)

        # Separator
        separator = Gtk.SeparatorMenuItem()
        self.menu.append(separator)

        # Quit
        quit_item = Gtk.MenuItem("Quit")
        quit_item.connect("activate", self.quit)
        self.menu.append(quit_item)

        self.statusicon.set_tooltip_text("Xdroid Tray Icon")
        self.statusicon.connect('activate', self.on_left_click)
        self.statusicon.connect('popup-menu', self.on_right_click)

        self.menu.show_all()  # Ensure menu items are visible
        self.statusicon.set_visible(True)

    def launch_xdroid(self, widget):
        os.system("xdroid-session &")
        self.launch_item.set_sensitive(False)
        self.switch_item.set_sensitive(True)

    def switch_xdroid_session(self, widget):
        os.system("pkexec chvt 8")

    def kill_xdroid_session(self, widget):
        os.system("xdroid-exit")
        self.launch_item.set_sensitive(True)
        self.switch_item.set_sensitive(False)

    def quit(self, widget):
        Gtk.main_quit()

    def on_left_click(self, icon):
        # Handle left-click event here
        # For example, toggle the Switch to Xdroid Session item's sensitivity
        switch_item = self.menu.get_children()[1]  # Get the Switch to Xdroid Session item
        switch_item.set_sensitive(True)

    def on_right_click(self, icon, button, time):
        menu = self.menu
        if menu:
            menu.popup(None, None, None, None, button, time)

class Xdroid:
    def __init__(self):
        FirstSetup()
        if not self.is_waydroid_initialized():
            FirstSetup()
        else:
            TrayIcon()
        
        Gtk.main()

    def is_waydroid_initialized(self):
        try:
            result = subprocess.check_output(["waydroid", "session"], stderr=subprocess.STDOUT, text=True)
            return "ERROR: WayDroid is not initialized, run \"waydroid init\"" not in result
        except subprocess.CalledProcessError as e:
            return False

if __name__ == "__main__":
    Xdroid()
