#!/usr/bin/env python3
import os
import subprocess, gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class ShConf(Gtk.Window):
    def __init__(self):
        super().__init__()
        builder = Gtk.Builder()
        builder.add_from_file('./ui/main.glade')
        self.pos = "none"

        self.config_dict = self.load_config()

        self.client:Gtk.Window = builder.get_object("main_dlg")

        self.panel_pos = ["lft_top",
                          "lft_mid",
                          "lft_btm",

                          "btm_lft",
                          "btm_mid",
                          "btm_rig",

                          "rig_top",
                          "rig_mid",
                          "rig_btm"]

        for i in self.panel_pos:
            btn:Gtk.RadioButton = builder.get_object(i)
            btn.set_name(i)
            if self.config_dict["panelpos"] == i:
                btn.set_active(True)

        for i in self.panel_pos:
            btn:Gtk.RadioButton = builder.get_object(i)
            btn.connect("clicked", self.change_panel_pos)


        self.tb_pos =    ["top",
                          "left",
                          "bottom",
                          "right"]

        for i in self.tb_pos:
            btn = builder.get_object("titlebar_" + i)
            btn.set_name(i)
            if self.config_dict["titlebarpos"] == i:
                self.tb_pos_curr = i
                btn.set_active(True)

        for i in self.tb_pos:
            btn = builder.get_object("titlebar_" + i)
            btn.connect("clicked", self.change_tb_pos)

        self.panel_size = builder.get_object("panel_size")
        self.panel_size.set_value(float(self.config_dict["panelsize"]))
        self.panel_size.connect("value-changed", self.change_panel_size)

        self.panel_dont_hide = builder.get_object("panel_dont_hide")
        self.panel_hide_classic = builder.get_object("panel_hide_classic")
        self.panel_hide_smart = builder.get_object("panel_hide_smart")

        if self.config_dict["panelhidepol"] == "nil":
            self.panel_dont_hide.set_active(True)
        elif self.config_dict["panelhidepol"] == "classic":
            self.panel_hide_classic.set_active(True)
        elif self.config_dict["panelhidepol"] == "smart":
            self.panel_hide_smart.set_active(True)

        self.panel_dont_hide.connect("clicked", lambda _: self.change_panel_hide_policy("nil"))
        self.panel_hide_classic.connect("clicked", lambda _: self.change_panel_hide_policy("classic"))
        self.panel_hide_smart.connect("clicked", lambda _: self.change_panel_hide_policy("smart"))

        self.titlebar_icon_theme:Gtk.Entry = builder.get_object("titlebar_theme")
        self.titlebar_buttons:Gtk.Entry = builder.get_object("titlebar_wids")
        self.titlebar_icon_size:Gtk.SpinButton = builder.get_object("titlebar_size")

        self.titlebar_buttons.set_text(self.config_dict["titlebarbtns"])
        self.titlebar_icon_theme.set_text(self.config_dict["titlebaricon"])
        self.titlebar_icon_size.set_value(float(self.config_dict["titlebarsizepx"]))

        self.titlebar_apply:Gtk.Button = builder.get_object("titlebar_apply")
        self.titlebar_apply.connect("clicked", self.change_tb)

        self.titlebar_themes_loc:Gtk.Button = builder.get_object("open_titlebar_theme_location")
        self.titlebar_themes_loc.connect("clicked", lambda _: os.system("xdg-open file:///Users/$USER/.config/NebiSoft/NebiDE.shell.data/theme/icons/titlebar"))

        self.client.show_all()

    def load_config(self):
        # Dosya yolu
        config_file_path = "~/.config/NebiSoft/NebiDE.shell.conf"

        # Tilde (~) işaretini kullanıcı dizinine dönüştürün
        config_file_path = os.path.expanduser(config_file_path)

        # Boş bir sözlük oluşturun
        config_dict = {}

        # Dosyayı açın ve satır satır okuyun
        with open(config_file_path, 'r') as file:
            for line in file:
                # Satırı boşluk karakterlerinden temizleyin ve '=' işaretine göre bölin
                parts = line.strip().split('=')
                if len(parts) == 2:
                    key = parts[0].strip().strip('"')  # Çift tırnakları çıkarın
                    value = parts[1].strip().strip('"')  # Çift tırnakları çıkarın

                    # Değer bir sayı ise int'e dönüştürün
                    if value.isdigit():
                        value = int(value)

                    # Sözlüğe ekleyin
                    config_dict[key] = value

        # Oluşturulan sözlüğü gösterin
        return config_dict

    def change_panel_size(self, obj):
        objf = str(int(obj.get_value()))
        subprocess.Popen(["bash", "-c", "awesome-client \"require('beautiful').action_bar_icon_size = " +
                        objf +
                        "; awesome.emit_signal('bottom_panel::update_sizes', '" + self.pos + "')\""])
        subprocess.Popen(["sed", "-i", "s/panelsize=.*/panelsize=" +
                          objf +
                          "/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])

    def change_panel_hide_policy(self, pol):
        print(pol)
        subprocess.Popen(["bash", "-c", "awesome-client \"awesome.emit_signal('bottom_panel::change_hide_policy', '" + pol + "')\""])
        subprocess.Popen(["sed", "-i", "s/panelhidepol=.*/panelhidepol=\"" +
                          pol +
                          "\"/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])

    def change_panel_pos(self, btn):
        self.pos = btn.get_name()
        subprocess.run(["bash", "-c",
                        "awesome-client \"awesome.emit_signal('bottom_panel::update_sizes', '" + self.pos + "')\""])
        subprocess.Popen(["sed", "-i", "s/panelpos=.*/panelpos=\"" +
                          self.pos +
                          "\"/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])

    def change_tb_pos(self, btn):
        self.tb_pos_curr = str(btn.get_name()).replace("titlebar_", "")

    def change_tb(self, btn):
        # Update position
        subprocess.Popen(["sed", "-i", "s/titlebarpos=.*/titlebarpos=\"" +
                          self.tb_pos_curr +
                          "\"/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])
        subprocess.Popen(["sed", "-i", "s/titlebarbtns=.*/titlebarbtns=\"" +
                          self.titlebar_buttons.get_text() +
                          "\"/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])
        subprocess.Popen(["sed", "-i", "s/titlebaricon=.*/titlebaricon=\"" +
                          self.titlebar_icon_theme.get_text() +
                          "\"/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])
        subprocess.Popen(["sed", "-i", "s/titlebarsizepx=.*/titlebarsizepx=\"" +
                          str(int(self.titlebar_icon_size.get_value())) +
                          "\"/", os.environ["HOME"] + "/.config/NebiSoft/NebiDE.shell.conf"])
        subprocess.Popen(["nebide-shellconf", "--skip-startup-anim"])

if __name__ == '__main__':
    ShConf()
    Gtk.main()
