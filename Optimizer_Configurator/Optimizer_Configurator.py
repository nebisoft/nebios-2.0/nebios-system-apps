#!/bin/python3
import os
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class NewConfigDialog(Gtk.Dialog):
    def __init__(self, parent):
        Gtk.Dialog.__init__(self, "Create New Configuration", transient_for=parent)

        self.set_default_size(200, 150)

        box = self.get_content_area()

        label = Gtk.Label(label="Enter a name for the new configuration:")
        self.entry = Gtk.Entry()
        self.entry.set_placeholder_text("Configuration Name")
        box.add(label)
        box.add(self.entry)

        self.add_button("Cancel", Gtk.ResponseType.CANCEL)
        self.add_button("Create", Gtk.ResponseType.OK)

        self.show_all()

class EditConf(Gtk.Box):
    def __init__(self, file):
        super().__init__()
        self.FILE = file
        self.set_border_width(20)

        # Create a grid to organize widgets
        grid = Gtk.Grid()
        grid.set_column_spacing(10)
        grid.set_row_spacing(10)
        self.add(grid)

        # Exec_Pattern Entry
        exec_pattern_label = Gtk.Label(label="Regex pattern for catching the executable of game/app:")
        exec_pattern_help = Gtk.Label(
            label="Use regular expressions to match the name of the game/application executable. "
                  "For example, if you want to match executables ending with '.exe', use '.exe$'. "
                  "If you want to match executables starting with 'game_', use '^game_'. "
                  "Other optimization settings will be applied when the executable name matches this pattern.")
        self.exec_pattern_entry = Gtk.Entry()
        self.exec_pattern_entry.set_vexpand(False)
        self.exec_pattern_entry.set_valign(Gtk.Align.START)
        exec_pattern_label.set_halign(Gtk.Align.START)
        exec_pattern_help.set_line_wrap(True)
        grid.attach(exec_pattern_label, 0, 0, 1, 1)
        grid.attach(exec_pattern_help, 0, 1, 3, 1)
        grid.attach(self.exec_pattern_entry, 1, 0, 3, 1)

        # Only_On_Fullscreen Checkbox
        self.only_fullscreen_checkbox = Gtk.CheckButton(label=" Only optimize the PC performance when game/app is fullscreen")
        grid.attach(self.only_fullscreen_checkbox, 0, 2, 3, 1)

        # Kill_Compositor Checkbox
        self.kill_compositor_checkbox = Gtk.CheckButton(label=" Kill compositor (disables blur & transparency)")
        grid.attach(self.kill_compositor_checkbox, 0, 3, 3, 1)

        # Kill_Wallpaper_Engine Checkbox
        self.kill_wallpaper_engine_checkbox = Gtk.CheckButton(label=" Kill Wallpaper Engine (can improve performance)")
        grid.attach(self.kill_wallpaper_engine_checkbox, 0, 4, 3, 1)

        # NebiDE_CPU_Limit Entry (percent)
        nebi_de_cpu_limit_label = Gtk.Label(label="Limit NebiDE's CPU Usage to value (between 1 - 100):")
        self.nebi_de_cpu_limit_entry = Gtk.Entry()
        nebi_de_cpu_limit_label.set_halign(Gtk.Align.START)
        grid.attach(nebi_de_cpu_limit_label, 0, 5, 1, 1)
        grid.attach(self.nebi_de_cpu_limit_entry, 1, 5, 2, 1)

        # Game_Nice_Level Entry (integer between 0 and 19)
        game_nice_level_label = Gtk.Label(label="Game priority (between 0 - 19):")
        game_nice_level_label.set_halign(Gtk.Align.START)
        self.game_nice_level_entry = Gtk.Entry()
        grid.attach(game_nice_level_label, 0, 6, 1, 1)
        grid.attach(self.game_nice_level_entry, 1, 6, 2, 1)

        # NebiDE_Nice_Level Entry (integer between 0 and 19)
        nebi_de_nice_level_label = Gtk.Label(label="NebiDE priority (between 0 - 19):")
        nebi_de_nice_level_label.set_halign(Gtk.Align.START)
        self.nebi_de_nice_level_entry = Gtk.Entry()
        grid.attach(nebi_de_nice_level_label, 0, 7, 1, 1)
        grid.attach(self.nebi_de_nice_level_entry, 1, 7, 2, 1)

        # Custom_Sh_Script Entry
        custom_sh_script_label = Gtk.Label(label="Custom shell config (optional):")
        custom_sh_script_label.set_halign(Gtk.Align.START)
        self.custom_sh_script_entry = Gtk.Entry()
        grid.attach(custom_sh_script_label, 0, 8, 1, 1)
        grid.attach(self.custom_sh_script_entry, 1, 8, 1, 1)

        # Custom_Sh_Script FileChooserButton
        custom_sh_script_button = Gtk.FileChooserButton(title="Select Custom Shell Script",
                                                        action=Gtk.FileChooserAction.OPEN)
        custom_sh_script_filter = Gtk.FileFilter()
        custom_sh_script_filter.set_name("Shell Script Files")
        custom_sh_script_filter.add_pattern("*.sh")
        custom_sh_script_button.add_filter(custom_sh_script_filter)

        # Dosya seçildiğinde çalışacak işlevi bağlayın
        custom_sh_script_button.connect("file-set", self.on_custom_sh_script_file_selected)

        # Dosya ismini göstermeyi devre dışı bırakın
        custom_sh_script_button.set_use_preview_label(False)

        # Grid'e dosya seçiciyi ekleyin
        grid.attach(custom_sh_script_button, 2, 8, 1, 1)

        self.load_config_from_file(file)

        # Save Button
        save_button = Gtk.Button(label="Save")
        save_button.connect("clicked", self.save_config)
        grid.attach(save_button, 0, 9, 3, 1)

    def show_error_dialog(self, message):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
                                   Gtk.ButtonsType.OK, "Error")
        dialog.format_secondary_text(message)
        dialog.run()
        dialog.destroy()

    def on_custom_sh_script_file_selected(self, widget):
        file_path = widget.get_filename()
        if file_path:
            self.custom_sh_script_entry.set_text(file_path)

    def save_config(self, button):
        exec_pattern = self.exec_pattern_entry.get_text()
        only_fullscreen = self.only_fullscreen_checkbox.get_active()
        kill_compositor = self.kill_compositor_checkbox.get_active()
        kill_wallpaper_engine = self.kill_wallpaper_engine_checkbox.get_active()

        try:
            nebi_de_cpu_limit = int(self.nebi_de_cpu_limit_entry.get_text())
            game_nice_level = int(self.game_nice_level_entry.get_text())
            nebi_de_nice_level = int(self.nebi_de_nice_level_entry.get_text())
        except ValueError:
            self.show_error_dialog("Invalid input for CPU limit or Nice level. Please enter valid integers.")
            return

        custom_sh_script = self.custom_sh_script_entry.get_text()

        # Düzenlenmiş verileri bir dosyaya kaydet
        with open(self.FILE, "w") as config_file:
            config_file.write(f"Exec_Pattern=\"{exec_pattern}\"\n")
            config_file.write(f"Only_On_Fullscreen={only_fullscreen}\n")
            config_file.write(f"Kill_Compositor={kill_compositor}\n")
            config_file.write(f"Kill_Wallpaper_Engine={kill_wallpaper_engine}\n")
            config_file.write(f"NebiDE_CPU_Limit={nebi_de_cpu_limit}\n")
            config_file.write(f"Game_Nice_Level={game_nice_level}\n")
            config_file.write(f"NebiDE_Nice_Level={nebi_de_nice_level}\n")
            config_file.write(f"Custom_Sh_Script={custom_sh_script}\n")

        print("Configuration saved to ", self.FILE)

    def load_config_from_file(self, file_path):
        config_data = {}
        try:
            with open(file_path, 'r') as file:
                for line in file:
                    # Her satırı ayırarak anahtar ve değeri elde edin
                    parts = line.strip().split('=')
                    if len(parts) == 2:
                        key = parts[0].strip()
                        value = parts[1].strip().strip('"')  # Çift tırnakları kaldırın
                        # Boolean değerleri ayırt etmek için kontrol edin
                        if value.lower() == 'true':
                            config_data[key] = True
                        elif value.lower() == 'false':
                            config_data[key] = False
                        else:
                            config_data[key] = value

        except FileNotFoundError:
            print(f"File not found: {file_path}")
        except Exception as e:
            print(f"An error occurred while reading the config file: {e}")

        # Exec_Pattern Entry
        exec_pattern = config_data.get("Exec_Pattern", "")  # Varsa değeri al, yoksa boş bir string kullan
        self.exec_pattern_entry.set_text(exec_pattern)

        # Only_On_Fullscreen Checkbox
        only_fullscreen = config_data.get("Only_On_Fullscreen", False)  # Varsa değeri al, yoksa False kullan
        self.only_fullscreen_checkbox.set_active(only_fullscreen)

        # Kill_Compositor Checkbox
        kill_compositor = config_data.get("Kill_Compositor", False)
        self.kill_compositor_checkbox.set_active(kill_compositor)

        # Kill_Wallpaper_Engine Checkbox
        kill_wallpaper_engine = config_data.get("Kill_Wallpaper_Engine", False)
        self.kill_wallpaper_engine_checkbox.set_active(kill_wallpaper_engine)

        # NebiDE_CPU_Limit Entry (percent)
        nebi_de_cpu_limit = config_data.get("NebiDE_CPU_Limit", "")
        self.nebi_de_cpu_limit_entry.set_text(str(nebi_de_cpu_limit))

        # Game_Nice_Level Entry (integer between 0 and 19)
        game_nice_level = config_data.get("Game_Nice_Level", "")
        self.game_nice_level_entry.set_text(str(game_nice_level))

        # NebiDE_Nice_Level Entry (integer between 0 and 19)
        nebi_de_nice_level = config_data.get("NebiDE_Nice_Level", "")
        self.nebi_de_nice_level_entry.set_text(str(nebi_de_nice_level))

        # Custom_Sh_Script Entry
        custom_sh_script = config_data.get("Custom_Sh_Script", "")
        self.custom_sh_script_entry.set_text(custom_sh_script)

class OC:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/main.glade")

        window = self.builder.get_object("main-window")
        self.window = window
        self.back_btn:Gtk.Button = self.builder.get_object("back-btn")
        self.back_btn.connect("clicked", lambda _: self.stack.remove(self.stack.get_child_by_name("edit")))
        self.stack_container:Gtk.Box = self.builder.get_object("stack")
        self.stack = Gtk.Stack()
        self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.stack.set_transition_duration(500)

        self.section_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.section_box.set_border_width(20)

        self.stack.add_named(self.section_box, "confs")

        self.add_sections()

        self.stack_container.add(self.stack)

        window.connect("destroy", Gtk.main_quit)
        window.show_all()

    def add_sections(self):
        for child in self.section_box.get_children():
            self.section_box.remove(child)

        for i in os.listdir(os.path.expanduser("~/.config/NebiSoft/NebiDE.optimizer/")):
            file = os.path.expanduser("~/.config/NebiSoft/NebiDE.optimizer/") + i

            builder = Gtk.Builder()
            builder.add_from_file("./ui/main.glade")

            template = Gtk.Button()
            template_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            config_name = Gtk.Label(file)

            template.add(template_box)
            template_box.pack_start(config_name, True, False, 0)
            template_box.pack_start(Gtk.Label(">"), False, False, 1)
            template.set_name(file)
            template.connect("clicked", self.click_template)

            self.section_box.add(template)

        # Add a new button below the configuration buttons
        new_button = Gtk.Button(label="New Configuration")
        new_button.connect("clicked", self.new_configuration)
        self.section_box.add(new_button)
        self.section_box.show_all()

    def new_configuration(self, button):
        # Show the name dialog for the new configuration
        dialog = NewConfigDialog(self.window)
        response = dialog.run()

        if response == Gtk.ResponseType.OK:
            # Get the entered configuration name
            config_name = dialog.entry.get_text()

            # Create the file path for the new configuration
            config_file_path = os.path.expanduser(f"~/.config/NebiSoft/NebiDE.optimizer/{config_name}.filter.ninf")

            # Define the content for the new configuration file
            config_content = """Exec_Pattern="gamename\.bin$"
Only_On_Fullscreen=False
Kill_Compositor=True
Kill_Wallpaper_Engine=True
NebiDE_CPU_Limit=1
Game_Nice_Level=0
NebiDE_Nice_Level=19
Custom_Sh_Script=
"""

            # Write the content to the new configuration file
            try:
                with open(config_file_path, "w") as config_file:
                    config_file.write(config_content)
                print(f"Configuration '{config_name}' created at '{config_file_path}'")
                self.add_sections()
            except Exception as e:
                print(f"Error creating the configuration file: {e}")

        dialog.destroy()

    def click_template(self, button):
        file = button.get_name()
        print(file)
        edit_conf_widget = EditConf(file)
        edit_conf_widget.show_all()
        self.stack.add_named(edit_conf_widget, "edit")
        self.stack.set_visible_child(edit_conf_widget)  # Set the visible child directly
        self.back_btn.set_visible(True)

win = OC()
Gtk.main()
