import os
import sys

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GdkPixbuf, Pango
import subprocess
import threading

SCRIPT_PATH = ""
README = ""
SCRIPT_INFO = {}
ARGS = []

class ScriptSequence(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)

        self.progress_label = Gtk.Label()
        self.progress_label.set_justify(Gtk.Justification.CENTER)
        self.cmd_label = Gtk.Label()
        self.cmd_label.set_justify(Gtk.Justification.CENTER)
        self.progress_bar = Gtk.ProgressBar()
        self.status = ""

        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.vbox.set_homogeneous(False)
        self.vbox.set_border_width(10)
        self.vbox.pack_start(self.progress_label, False, False, 0)
        self.vbox.pack_start(self.cmd_label, False, False, 0)
        self.vbox.pack_start(self.progress_bar, False, False, 0)


        OutputBox = Gtk.ScrolledWindow()
        self.text_view = Gtk.TextView()
        self.text_view.set_editable(False)
        OutputBox.add(self.text_view)

        self.pack_start(OutputBox, True, True, 0)
        self.add(self.vbox)

        self.update_thread = None

        self.connect("destroy", Gtk.main_quit)

    def start(self):
        os.chdir(SCRIPT_PATH.rsplit("/", 1)[0])
        self.update_command = sys.argv[1] + " " + ARGS
        self.progress_label.set_text("Starting...")
        self.update_thread = threading.Thread(target=self.run_update_thread)
        self.update_thread.daemon = True
        self.update_thread.start()

    def run_update_thread(self):
        try:
            process = subprocess.Popen(self.update_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            while True:
                output = process.stdout.readline()
                if not output:
                    break
                GLib.idle_add(self.update_progress, output.strip(), output.replace("Status=", "", 1).split(" ", 1)[0])
            process.wait()
            GLib.idle_add(self.update_completed)
        except Exception as e:
            GLib.idle_add(self.update_failed, str(e))

    def update_progress(self, output, progress):
        if output.__contains__("Status="):
            self.status = output.replace("Status=", "", 1).replace("/", " of ", 1)
            self.progress_label.set_markup("<b>" + self.status + "</b>")
            self.progress_bar.set_fraction(
                float(1 / int(progress.split("/")[1])) * int(progress.split("/")[0]))
        if self.status != output.replace("Status=", "", 1).replace("/", " of ", 1):
            self.progress_label.set_markup("<b>" + self.status + "</b>")
            self.cmd_label.set_label(output[0:92] + " ...")

        buffer: Gtk.TextBuffer = self.text_view.get_buffer()
        buffer.set_text(buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), True) + "\n" + output)

    def update_completed(self):
        self.progress_label.set_markup("<b>Script finished.</b>")

    def update_failed(self, error_message):
        self.progress_label.set_text("Script failed: " + error_message)

class ArgsSequence(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.set_border_width(10)
        scrolled_window = Gtk.ScrolledWindow()
        self.text_view = Gtk.TextView()
        scrolled_window.add(self.text_view)
        self.load_readme_from_file("./assets/README")
        ok_btn = Gtk.Button("OK")
        ok_btn.set_halign(Gtk.Align.END)
        ok_btn.connect("clicked", lambda _: (self.save_args(), app.stack.set_visible_child_name("script"), app.stack.get_child_by_name("script").start()))
        self.pack_start(scrolled_window, True, True, 0)
        self.pack_start(ok_btn, False, False, 1)

    def save_args(self):
        global ARGS
        buffer:Gtk.TextBuffer = self.text_view.get_buffer()
        bufftxt = buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), True)
        ARGS = bufftxt.replace("\n", " ")

    def load_readme_from_file(self, file_path):
        buffer:Gtk.TextBuffer = self.text_view.get_buffer()
        global SCRIPT_INFO
        if SCRIPT_INFO["Args"] != "":
            for i in SCRIPT_INFO["Args"][1:-1].split(", "):
                buffer.set_text(buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter(), True) + i[1:-1] + "\n")
        else:
            buffer.set_text("# No Args")

class ReadmeSequence(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.set_border_width(10)
        scrolled_window = Gtk.ScrolledWindow()
        self.text_view = Gtk.TextView()
        self.text_view.set_editable(False)
        scrolled_window.add(self.text_view)
        self.load_readme_from_file("./assets/README")
        ok_btn = Gtk.Button("OK")
        ok_btn.set_halign(Gtk.Align.END)
        ok_btn.connect("clicked", lambda _: app.stack.set_visible_child_name("args"))
        self.pack_start(scrolled_window, True, True, 0)
        self.pack_start(ok_btn, False, False, 1)

    def load_readme_from_file(self, file_path):
        buffer:Gtk.TextBuffer = self.text_view.get_buffer()
        if README != "":
            buffer.set_text(README)
        else:
            buffer.set_text("No Readme")
class WelcomeSequence(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=24)
        self.set_border_width(10)
        logo_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale("./assets/logo.png", -1, 256, True)
        logo = Gtk.Image.new_from_pixbuf(logo_pixbuf)
        global SCRIPT_NAME, SCRIPT_AUTHOR
        welcome_label = Gtk.Label()
        welcome_label.set_text("Welcome to " + str(SCRIPT_INFO["Name"]) + "\nby " + str(SCRIPT_INFO["Author"]))


        # Set the font size using Pango
        welcome_label.override_font(
            Pango.FontDescription("ExtraBold 20"))  # You can adjust the size (e.g., "Sans 20" for font size 20)

        go_btn = Gtk.Button("Next >")
        go_btn.set_halign(Gtk.Align.CENTER)
        go_btn.connect("clicked", lambda _: app.stack.set_visible_child_name("readme"))

        self.add(logo)
        self.add(welcome_label)
        self.add(go_btn)
        self.set_valign(Gtk.Align.CENTER)
        self.set_hexpand(True)

class NSCR_Interpreter(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_size_request(800, 600)

        info = subprocess.check_output([sys.argv[1]], env={
            "NSCR_STATUS": "info"
        }).decode().strip().split("\n")

        global README
        README = subprocess.check_output([sys.argv[1]], env={
            "NSCR_STATUS": "readme"
        }).decode().strip()

        for i in info:
            if i != "":
                v = i.split("=", 1)
                SCRIPT_INFO[v[0]] = v[1]

        self.set_title(SCRIPT_PATH + " - NSCR Intepreter")

        self.stack = Gtk.Stack()
        self.add(self.stack)
        self.stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        self.stack.set_transition_duration(500)
        self.stack.add_named(WelcomeSequence(), "welcome")
        self.stack.add_named(ReadmeSequence(), "readme")
        self.stack.add_named(ArgsSequence(), "args")
        self.stack.add_named(ScriptSequence(), "script")
        self.show_all()

if __name__ == "__main__":
    if sys.argv == [sys.argv[0]]:
        print("ERROR: No script specified.")
        exit(1)
    SCRIPT_PATH = os.path.join(sys.argv[1])
    app = NSCR_Interpreter()
    Gtk.main()

