#!/bin/bash
echo "Stat=1/5 Preparing to install"
sudo mkdir -p /tmp/NebiOS_Update/
sudo add-apt-repository -y "deb [trusted=yes] https://legacypkg.nebisoftware.com/nebios/ ./"
echo "Stat=2/5 Downloading Packages: NebiDE"
sudo apt install --yes  --download-only nebide
echo "Stat=3/5 Downloading Packages: NebiOS App Runtime"
sudo apt install --yes --download-only napp-runtime nebios-windows-support
echo "Stat=4/5 Removing Repo"
sudo add-apt-repository -y "deb [trusted=yes] https://legacypkg.nebisoftware.com/nebios/ ./"
echo "Stat=5/5 Installing Packages"
sudo apt install --yes nebide nebios-windows-support
