��    a      $  �   ,      8     9     ?     H     L     U     f     u     {     �     �     �     �     �     �     �     �     �  	   �     	     
	     	     	     	     4	     :	     H	     a	  (   r	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     
     '
     /
  
   4
  
   ?
  	   J
     T
     ]
     c
     v
     �
  
   �
     �
     �
     �
     �
     �
     �
  
   �
     �
                 
        '     7     @     L  	   Q  
   [     f  	   x     �     �  
   �     �     �     �     �     �     �       	             #  	   1  	   ;     E     Q     _  
   f     q     }     �     �     �     �     �  M  �                         &     =     X     _     f     }     �     �  
   �  	   �     �     �     �     �     �  
   �  	                   3     9     J     h  8   �     �     �     �     �  	             -     D     G     Y     e     q     w     �     �     �  	   �     �     �     �     �          &     :     M     h  	   �     �     �     �     �     �     �  
   �     �               ,     1     @     P     n     }     �     �     �     �     �     �     �  !     
   1     <     E     J     [     n     {     �     �     �     �     �     �     �     �          !     U   Y         %   \       E       ]   !   K   :   A   1                  C   9   ?   .   3           ;   P   `      ^   B   J   
      I       0                        M   R   4   &              *                            6   _              (       G      N   W   L          H   S   @   =       [       Z   $      '      5           #              X      a                V   	   +   D       <              F   O   Q                7   ,   T       "      -   )   2           /      >   8     None %d of %d ... 3px tile About NebiDecker Add color stop Align Apply Aspect Ratio:  Background color Background pattern Batthern Beige paper Black linen Bottom Bright squares Bring forward Button %d By: %s Cancel Center Circle Click to add text... Clone Color Palette Controller Configuration Controller found Could not rename: File already exists… Create a new deck Custom Color Dark geometric Decks Delete Diamond Upholstery Edit image with… End Export to PDF Fabric dark Fabric light Flowers Font Font Style Font color Font size Geometry Gplay Gradient Direction Gradient Editor Gradient Palette Hexellence Insert Image Insert Shape Insert Text Box Jump to slide: Justify Left Light wool Make a Simple Deck Middle My Deck Name:  NebiDecker Neutral squares New Deck Next Slide: Open Open File Open Image Open a saved deck Open file Open file location… Open: %s Pick Color Presenter Notes: Previous Slide: Redo Remove color stop Replace Image… Return to Welcome Screen Right Roundness Save Send backward Set Jump: Set up… Shape color Silver scales Square Start Deck Subtle grid Subtle squares Toggle Deck: Top Undo Untitled Deck XV Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-03-17 18:42-0600
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: Adolfo Jayme Barrientos <fitojb@ubuntu.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: ca
  Cap %d / %d ... Mosaic de 3 px Quant a l’NebiDecker Afegeix una etapa de color Alinea Aplica Relació d’aspecte:  Color del fons Patró de fons Rajoles Paper beix Lli negre A baix Quadrats brillants Porta cap endavant Botó %d Per: %s Cancel·la Al centre Cercle Feu clic per a afegir-hi text… Clona Paleta de colors Configuració del controlador S’ha trobat un controlador No s’ha pogut canviar el nom: el fitxer ja existeix… Creeu una presentació nova Color personalitzat Geometria fosca Presentacions Suprimeix Entapissat de diamants Edita la imatge amb… Fi Exporta com a PDF Teixit fosc Teixit clar Flors Tipus de lletra Estil del tipus de lletra Color del text Mida del text Geometria GPlay Direcció del degradat Editor de degradats Paleta de degradats Excel·lència hexagonal Insereix una imatge Insereix una forma Insereix un quadre de text Salta a la diapositiva: Justifica A l’esquerra Llana clara Creeu una presentació senzilla Al mig La meva presentació Nom:  NebiDecker Quadrats neutrals Presentació nova Diapositiva següent: Obre Obre un fitxer Obre una imatge Obriu una presentació desada Obre un fitxer Localitza el fitxer… Obre: %s Trieu un color Notes de l’orador: Diapositiva anterior: Refés Treu l’etapa de color Substitueix la imatge… Torna a la pantalla de benvinguda A la dreta Rodonesa Desa Envia cap enrere Configura el salt: Configura… Color de la forma Escates d’argent Quadrat Inicia la presentació Graella subtil Quadrats subtils Commuta la presentació: A dalt Desfés Presentació sense títol XV 