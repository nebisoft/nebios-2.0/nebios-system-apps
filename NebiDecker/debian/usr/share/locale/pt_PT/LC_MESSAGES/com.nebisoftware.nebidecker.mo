��    ^           �      �     �     �                    &     5     ;     A     P     a     t     }     �     �     �     �  	   �     �     �     �     �     �     �     �     	     !	  (   2	     [	     m	     z	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	  
   �	  	   

     
     
     0
     @
  
   Q
     \
     i
     v
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
  	      
   
       	   '     1     G  
   P     [     l     |     �     �     �     �  	   �     �     �  	   �  	   �     �             
              ,     ;     H     L     Q    _     t     |     �     �     �     �     �     �     �     �     �          "     .     :     @     Q  	   f     p     x     �     �     �     �     �     �     �  D   �     C     a     s     �     �     �     �     �     �     �     �     �                     -  	   >     H     b     w     �     �     �     �     �     �     �  	   �     �               4     ;     M  	   a     k     q     �  !   �     �     �  	   �     �     �  	                  -     B     \  	   d     n     v     �     �     �     �     �     �     �     �          #     (     1         2       L      $              0           -      O          '       *       &      \   U       <           6         7           "   W       F              9   ?      Q   I   ,   (   +           @          >       C         5           8             !   D   S   Y   ^   ;      	   X   M      N   ]       )            B   Z   :   H   .   %      #       K          =      3      1          [   R   4   
   /       G          J   T   E                     A   V      P         None %d of %d ... 3px tile About NebiDecker Add color stop Align Apply Aspect Ratio:  Background color Background pattern Batthern Beige paper Black linen Bottom Bright squares Bring forward Button %d By: %s Cancel Center Circle Click to add text... Clone Color Palette Controller Configuration Controller found Could not rename: File already exists… Create a new deck Custom Color Dark geometric Decks Delete Diamond Upholstery Edit image with… End Export to PDF Fabric dark Fabric light Flowers Font Font Style Font color Font size Geometry Gradient Direction Gradient Editor Gradient Palette Hexellence Insert Image Insert Shape Insert Text Box Jump to slide: Justify Left Light wool Make a Simple Deck Middle My Deck Name:  Neutral squares New Deck Next Slide: Open Open File Open Image Open a saved deck Open file Open file location… Open: %s Pick Color Presenter Notes: Previous Slide: Redo Remove color stop Replace Image… Return to Welcome Screen Right Roundness Save Send backward Set Jump: Set up… Shape color Silver scales Square Start Deck Subtle grid Subtle squares Toggle Deck: Top Undo Untitled Deck Project-Id-Version: 1.0.0
Report-Msgid-Bugs-To: Felipe
PO-Revision-Date: 2018-02-11 18:40+0000
Last-Translator: Emanuel Angelo <emanuel.angelo@gmail.com>
Language-Team: 
Language: pt_PT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
  Nenhum %d de %d … Azulejo de 3px Sobre o NebiDecker Adicionar ponto de cor Alinhamento Aplicar Relação largura/altura: Cor de fundo Padrão de fundo Azulejo de chuveiro Papel pardo Linho preto Fundo Quadrados claros Trazer para a frente Botão %d Por: %s Cancelar Centrado Círculo Clique para adicionar texto… Clonar Paleta de cores Configuração do controlador Controlador encontrado Não foi possível renomear: Já existe um ficheiro com esse nome… Criar uma nova apresentação Cor personalizada Geometria escura Apresentações Eliminar Estofamento em Diamante Editar imagem com… Fim Exportar para PDF Tecido escuro Tecido claro Flores Tipo de letra Estilo da letra Cor da letra Tamanho da letra Geometria Orientação do gradiente Editor de gradientes Paleta de gradientes Hexelência Inserir imagem Inserir forma Inserir caixa de texto Saltar para: Justificado Esquerda Lã clara Crie uma apresentação simples Meio A minha apresentação Nome:  Quadrados neutros Nova apresentação Seguinte: Abrir Abrir ficheiro Abrir imagem Abrir uma apresentação guardada Abrir ficheiro Abrir local do ficheiro… Abrir: %s Escolher cor Notas do apresentador Anterior: Refazer Remover ponto de cor Substituir imagem… Voltar à página inicial Direita Redondeza Guardar Enviar para o fundo Criar salto: Configurar… Cor da forma Escamas prateadas Quadrado Iniciar apresentação Grelha subtil Quadrados subtis Iniciar/Sair da apresentação: Topo Desfazer Apresentação sem título 