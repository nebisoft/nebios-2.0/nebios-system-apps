��    `        �         (     )     /     8     <     E     V     e     k     q     �     �     �     �     �     �     �  	   �     �     �     �     �     	     	     !	     /	     H	  (   Y	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
  
   
  
   &
  	   1
     ;
     D
     J
     ]
     m
  
   ~
     �
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
  
                  '     3  	   8  
   B     M  	   _     i       
   �     �     �     �     �     �     �     �  	   �          
  	     	   "     ,     8     F  
   M     X     d     s     �     �     �     �  8  �     �  	   �     �     �     �       	   !     +     4     H     Y     k     y     �     �     �  	   �     �  	   �  	   �     �      �     �               0  5   G     }     �     �     �  	   �     �     �                     .     ;  
   B     M     Y     f  	   u          �     �     �  
   �     �     �     �     �  	               !   )     K     Q     e  
   l     w     �     �     �     �     �      �     �     �               (     C     T     `     t  !   �     �     �  	   �     �     �     �     �               #     1     A     R     l     q     �     �     T   X         $   [       D       \       J   9   @   0                  B   8   >   -   2           :   O   _      ]   A   I   
      H       /                        L   Q   3   %              )   `                         5   ^              '       F      M   V   K          G   R   ?   <      Z       Y   #      &      4           "              W                       U   	   *   C       ;              E   N   P                6   +   S       !      ,   (   1           .      =   7     None %d of %d ... 3px tile About NebiDecker Add color stop Align Apply Aspect Ratio:  Background color Background pattern Beige paper Black linen Bottom Bright squares Bring forward Button %d By: %s Cancel Center Circle Click to add text... Clone Color Palette Controller Configuration Controller found Could not rename: File already exists… Create a new deck Custom Color Dark geometric Decks Delete Diamond Upholstery Edit image with… End Export to PDF Fabric dark Fabric light Flowers Font Font Style Font color Font size Geometry Gplay Gradient Direction Gradient Editor Gradient Palette Hexellence Insert Image Insert Shape Insert Text Box Jump to slide: Justify Left Light wool Make a Simple Deck Middle My Deck Name:  NebiDecker Neutral squares New Deck Next Slide: Open Open File Open Image Open a saved deck Open file Open file location… Open: %s Pick Color Presenter Notes: Previous Slide: Redo Remove color stop Replace Image… Return to Welcome Screen Right Roundness Save Send backward Set Jump: Set up… Shape color Silver scales Square Start Deck Subtle grid Subtle squares Toggle Deck: Top Undo Untitled Deck XV Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-18 14:09+0100
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 3.32.0
Last-Translator: Nico Sonack <nsonack@outlook.com>
Plural-Forms: nplurals=2; plural=(n != 1)
Language: de
  Nichts %d von %d ... 3px Kachelmuster Über NebiDecker Farbpunkt hinzufügen Anordnung Anwenden Seitenverhältnis:  Hintergrundfarbe Hintergrundmuster Beiges Papier Schwarzes Leinen Unten Helle Quadrate In den Vordergrund Button %d Von: %s Abbrechen Zentriert Kreis Klicken um Text hinzuzufügen... Duplizieren Farbpalette Fernbedienung einrichten Fernbedienung gefunden Umbenennen fehlgeschlagen: Datei existiert bereits… Neue Präsentation erstellen Benutzerdefinierte Farbe Dunkle Geometrie Präsentationen Entfernen Diamantpolsterung Bild bearbeiten mit… Ende Als PDF exportieren Dunkler Stoff Heller Stoff Blumen Schriftart Schriftstil Schriftfarbe Schriftgröße Geometrie Gplay Verlaufsrichtung Farbverlaufseditor Farbverlaufspalette Hexzellenz Bild einfügen Form einfügen Textelement hinzufügen Gehe zu Folie: Blocksatz Links Helle Wolle Erstelle einfache Präsentationen Mitte Meine Präsentation Name:  NebiDecker Neutrale Quadrate Neue Präsentation Nächste Folie: Öffnen Datei öffnen Bild öffnen Bestehende Präsentation öffnen Datei öffnen Pfad öffnen… Öffnen: %s Farbe wählen Notizen für Präsentator: Vorherige Folie: Wiederholen Farbpunkt entfernen Bild ersetzen… Zum Startbildschirm zurückkehren Rechts Abrundungsgrad Speichern In den Hintergrund Sprungmarke setzen: Einrichten… Farbe der Form Silberne Reihen Quadrat Präsentieren Leichtes Raster Leichte Quadrate Präsentation umschalten: Oben Rückgängig machen Unbenannte Präsentation XV 