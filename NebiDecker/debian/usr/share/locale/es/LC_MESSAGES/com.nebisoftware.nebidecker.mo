��    a      $  �   ,      8     9     ?     H     L     U     f     u     {     �     �     �     �     �     �     �     �     �  	   �     	     
	     	     	     	     4	     :	     H	     a	  (   r	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     
     '
     /
  
   4
  
   ?
  	   J
     T
     ]
     c
     v
     �
  
   �
     �
     �
     �
     �
     �
     �
  
   �
     �
                 
        '     7     @     L  	   Q  
   [     f  	   x     �     �  
   �     �     �     �     �     �     �       	             #  	   1  	   ;     E     Q     _  
   f     q     }     �     �     �     �     �  '  �     �     �     �     �               5     =     E     \     k     |  
   �  
   �     �     �     �  	   �     �     �  	   �     �     �     	          "     =  6   P     �     �     �     �     �     �     �               $  
   0     ;     B     P     `     o  
   �     �     �     �     �     �     �     �          ,  
   4     ?  
   N     Y     l     u     �  
   �     �     �     �     �     �     �      �          /  	   D     N     ]     o     �     �     �     �     �     �     �     �               '     9     K     R     l     �  "   �     �     �     �     �     U   Y         %   \       E       ]   !   K   :   A   1                  C   9   ?   .   3           ;   P   `      ^   B   J   
      I       0                        M   R   4   &              *                            6   _              (       G      N   W   L          H   S   @   =       [       Z   $      '      5           #              X      a                V   	   +   D       <              F   O   Q                7   ,   T       "      -   )   2           /      >   8     None %d of %d ... 3px tile About NebiDecker Add color stop Align Apply Aspect Ratio:  Background color Background pattern Batthern Beige paper Black linen Bottom Bright squares Bring forward Button %d By: %s Cancel Center Circle Click to add text... Clone Color Palette Controller Configuration Controller found Could not rename: File already exists… Create a new deck Custom Color Dark geometric Decks Delete Diamond Upholstery Edit image with… End Export to PDF Fabric dark Fabric light Flowers Font Font Style Font color Font size Geometry Gplay Gradient Direction Gradient Editor Gradient Palette Hexellence Insert Image Insert Shape Insert Text Box Jump to slide: Justify Left Light wool Make a Simple Deck Middle My Deck Name:  NebiDecker Neutral squares New Deck Next Slide: Open Open File Open Image Open a saved deck Open file Open file location… Open: %s Pick Color Presenter Notes: Previous Slide: Redo Remove color stop Replace Image… Return to Welcome Screen Right Roundness Save Send backward Set Jump: Set up… Shape color Silver scales Square Start Deck Subtle grid Subtle squares Toggle Deck: Top Undo Untitled Deck XV Project-Id-Version: 1.0.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-03-17 18:15-0600
Last-Translator: Adolfo Jayme Barrientos <fitojb@ubuntu.com>
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
  Ninguno %d de %d ... Mosaicos de 3 px Acerca de NebiDecker Añadir delimitador de color Alinear Aplicar Relación de aspecto:  Color de fondo Patrón de fondo Azulejos Papel beis Lino negro Abajo Cuadros claros Traer adelante Botón %d Por: %s Cancelar Al centro Círculo Pulse para añadir texto… Clonar Paleta de colores Configuración del control Control encontrado No se pudo cambiar el nombre: ese archivo ya existe… Crear una presentación nueva Color personalizado Geometría oscura Presentaciones Eliminar Tapizado de diamantes Editar la imagen con… Fin Exportar a PDF Tela oscura Tela clara Flores Tipo de letra Estilo de letra Color de texto Tamaño de texto Geometría GPlay Dirección del degradado Editor de degradados Paleta de degradados Excelencia hexagonal Insertar una imagen Insertar una forma Insertar un cuadro de texto Saltar: Justificar A la izquierda Lana clara Comparta sus ideas En medio Mi presentación Nombre:  NebiDecker Cuadros neutros Presentación nueva Diapositiva siguiente: Abrir Abrir un archivo Abrir una imagen Abrir una presentación guardada Abrir un archivo Ubicar el archivo… Abrir: %s Elija un color Notas del orador: Diapositiva anterior: Rehacer Quitar delimitador de color Reemplazar la imagen… Volver a la pantalla principal A la derecha Redondez Guardar Enviar atrás Crear salto: Configurar… Color de la forma Escamas plateadas Cuadro Comenzar la presentación Cuadrícula pequeña Cuadros pequeños Iniciar/salir de la presentación: Arriba Deshacer Presentación sin nombre XV 