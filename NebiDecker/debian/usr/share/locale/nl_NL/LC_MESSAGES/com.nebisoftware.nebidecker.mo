��    a      $  �   ,      8     9     ?     H     L     U     f     u     {     �     �     �     �     �     �     �     �     �  	   �     	     
	     	     	     	     4	     :	     H	     a	  (   r	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     
     '
     /
  
   4
  
   ?
  	   J
     T
     ]
     c
     v
     �
  
   �
     �
     �
     �
     �
     �
     �
  
   �
     �
                 
        '     7     @     L  	   Q  
   [     f  	   x     �     �  
   �     �     �     �     �     �     �       	             #  	   1  	   ;     E     Q     _  
   f     q     }     �     �     �     �     �  s  �     '  	   -     7  	   ;     E     U  	   i  	   s     }     �     �     �     �     �  	   �     �     �            	     	        )     0  
   O     Z     g       >   �     �     �                     +     @     [     a     u     �     �  
   �     �     �     �  	   �     �     �            
   ,     7     K     Y     k  	   ~     �  
   �     �     �     �     �  
   �     �     �                    +  !   =     _     n  
   �     �     �     �     �     �     �     �       	   %     /     7     I     [  	   h     r     �     �     �     �     �  	   �     �     �          U   Y         %   \       E       ]   !   K   :   A   1                  C   9   ?   .   3           ;   P   `      ^   B   J   
      I       0                        M   R   4   &              *                            6   _              (       G      N   W   L          H   S   @   =       [       Z   $      '      5           #              X      a                V   	   +   D       <              F   O   Q                7   ,   T       "      -   )   2           /      >   8     None %d of %d ... 3px tile About NebiDecker Add color stop Align Apply Aspect Ratio:  Background color Background pattern Batthern Beige paper Black linen Bottom Bright squares Bring forward Button %d By: %s Cancel Center Circle Click to add text... Clone Color Palette Controller Configuration Controller found Could not rename: File already exists… Create a new deck Custom Color Dark geometric Decks Delete Diamond Upholstery Edit image with… End Export to PDF Fabric dark Fabric light Flowers Font Font Style Font color Font size Geometry Gplay Gradient Direction Gradient Editor Gradient Palette Hexellence Insert Image Insert Shape Insert Text Box Jump to slide: Justify Left Light wool Make a Simple Deck Middle My Deck Name:  NebiDecker Neutral squares New Deck Next Slide: Open Open File Open Image Open a saved deck Open file Open file location… Open: %s Pick Color Presenter Notes: Previous Slide: Redo Remove color stop Replace Image… Return to Welcome Screen Right Roundness Save Send backward Set Jump: Set up… Shape color Silver scales Square Start Deck Subtle grid Subtle squares Toggle Deck: Top Undo Untitled Deck XV Project-Id-Version: NebiDecker
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-05-21 11:51+0200
Language-Team: Dutch (vistausss@outlook.com)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Last-Translator: Heimen Stoffels <vistausss@outlook.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: nl_NL
  Geen %d van %d ... 3px tegel Over NebiDecker Kleurstop toevoegen Uitlijnen Toepassen Beeldverhouding:  Achtergrondkleur Achtergrondpatroon Batthern Beige papier Zwarte linnen Onderkant Lichte vierkanten Naar voren halen Knop %d Door: %s Annuleren Centreren Cirkel Klik om tekst toe te voegen... Dupliceren Kleurenpalet Controller-configuratie Controller gevonden Het wijzigen van de naam is mislukt: het bestand bestaat al… Een nieuwe presentatie maken Aangepaste kleur Donkere geometrie Presentaties Verwijderen Diamanten stoffering Afbeelding bewerken met… Einde Exporteren naar PDF Donkere structuur Lichte structuur Bloemen Lettertype Lettertypestijl Lettertypekleur Lettertypegrootte Geometrie Gplay Kleurverloop-richting Kleurverloop-bewerker Kleurverloop-palet Hexellence Afbeelding invoegen Vorm invoegen Tekstvak invoegen Springen naar dia: Uitvullen Links Lichte wol Eenvoudige presentatie maken Midden Mijn presentatie Naam:  NebiDecker Neutrale vierkanten Nieuwe presentatie Volgende dia: Openen Bestand openen Afbeelding openen Een opgeslagen presentatie openen Bestand openen Bestandslocatie openen… Openen: %s Kleur kiezen Notities van de presentator: Vorige dia: Opnieuw Kleurstop verwijderen Afbeelding vervangen… Terugkeren naar welkomstscherm Rechts Rondingen Opslaan Naar achter duwen Sprong instellen: Instellen… Vormkleur Zilveren schalen Vierkant Presentatie starten Subtiel raster Subtiele vierkanten Presentatieschakeling: Bovenkant Ongedaan maken Naamloze presentatie XV 