��    a      $  �   ,      8     9     ?     H     L     U     f     u     {     �     �     �     �     �     �     �     �     �  	   �     	     
	     	     	     	     4	     :	     H	     a	  (   r	     �	     �	     �	     �	     �	     �	     �	     �	      
     
     
     '
     /
  
   4
  
   ?
  	   J
     T
     ]
     c
     v
     �
  
   �
     �
     �
     �
     �
     �
     �
  
   �
     �
                 
        '     7     @     L  	   Q  
   [     f  	   x     �     �  
   �     �     �     �     �     �     �       	             #  	   1  	   ;     E     Q     _  
   f     q     }     �     �     �     �     �    �     �     �     �     �     �     �       	             (     8     H     Q  
   ^     i     m       	   �     �     �     �     �      �  	   �     �     �       4     !   T     v     �     �  	   �     �     �     �     �     �     �                    )     :     K     W     ]     t     �  
   �     �     �     �     �  	   �                    9     @     Q  
   X     c     s  	   �     �     �     �  %   �     �     �               0     I  	   V     `          �     �     �     �     �     �     �     �                     ;     J  "   Z     }     �     �     �     U   Y         %   \       E       ]   !   K   :   A   1                  C   9   ?   .   3           ;   P   `      ^   B   J   
      I       0                        M   R   4   &              *                            6   _              (       G      N   W   L          H   S   @   =       [       Z   $      '      5           #              X      a                V   	   +   D       <              F   O   Q                7   ,   T       "      -   )   2           /      >   8     None %d of %d ... 3px tile About NebiDecker Add color stop Align Apply Aspect Ratio:  Background color Background pattern Batthern Beige paper Black linen Bottom Bright squares Bring forward Button %d By: %s Cancel Center Circle Click to add text... Clone Color Palette Controller Configuration Controller found Could not rename: File already exists… Create a new deck Custom Color Dark geometric Decks Delete Diamond Upholstery Edit image with… End Export to PDF Fabric dark Fabric light Flowers Font Font Style Font color Font size Geometry Gplay Gradient Direction Gradient Editor Gradient Palette Hexellence Insert Image Insert Shape Insert Text Box Jump to slide: Justify Left Light wool Make a Simple Deck Middle My Deck Name:  NebiDecker Neutral squares New Deck Next Slide: Open Open File Open Image Open a saved deck Open file Open file location… Open: %s Pick Color Presenter Notes: Previous Slide: Redo Remove color stop Replace Image… Return to Welcome Screen Right Roundness Save Send backward Set Jump: Set up… Shape color Silver scales Square Start Deck Subtle grid Subtle squares Toggle Deck: Top Undo Untitled Deck XV Project-Id-Version: 1.0.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2018-10-17 21:53+0200
Last-Translator: dabou
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
  Aucun %d de %d ... tuile de 3px À propos de NebiDecker Ajouter un arrêt de couleur Aligner Appliquer Ratio :  Couleur de fond Modèle de fond Batthern Papier beige Linge noir Bas Carrés brillants Avancer Bouton %d De : %s Annuler Centre Cercle Cliquer pour ajouter du texte... Dupliquer Palette de Couleurs Configuration du Contrôleur Contrôleur trouvé Impossible de renommer : Le fichier existe déjà… Créer une nouvelle présentation Modifier la Couleur Géométrie sombre Présentations Supprimer Diamant tissé Modifier l'image avec… Fin Exporter en PDF Tissu sombre Tissu clair Fleurs Police Couleur du fond Couleur du texte Taille de police Géométrie Gplay Direction du dégradé Éditeur de dégradé Dégradé de Couleur Hexellence Insérer une image Inserer une forme Insérer du texte Allez à la diapositive : Justifier Gauche Laine claire Créer une Présentation Simple Milieu Ma Présentation Nom :  NebiDecker Carrés neutres Nouvelle Présentation Suivant : Ouvrir Ouvrir un fichier Ouvrir une Image Ouvrir une présentation enregistrée Ouvrir un fichier Ouvrir le dossier… Ouvrir : %s Sélectionner la couleur Notes du présentateur : Précedent : Rétablir Supprimer un arrêt de couleur Remplacer l'image… Retourner à l'accueil Droite Arrondi Enregistrer Reculer Créer un saut : Configuration… Couleur de la forme Écailles argentées Carré Commencer la Présentation Grille subtile Carrés subtils Démarrer/Quitter la présentation Haut Annuler Présentation sans titre XV 