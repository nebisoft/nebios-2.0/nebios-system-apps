import os
import subprocess
import sys

import gi
gi.require_version("Gtk", "3.0")
gi.require_version('GtkSource', '4')
from gi.repository import GtkSource, Gtk, GObject
from gi.overrides import Gtk, GObject

class MyApp(object):

    def __init__(self):
        self.restartWMcmd = "echo 'awesome.restart()' | awesome-client"
        self.builder = Gtk.Builder()
        self.glade_file = "ui/main.glade"
        GObject.type_register(GtkSource.View)
        self.builder.add_from_file(self.glade_file)

        self.stack = self.builder.get_object("stack1")
        self.builder.get_object("Back").connect("clicked", self.goBack)
        self.builder.get_object("Next").connect("clicked", self.goNext)
        self.builder.get_object("nebicloud_login").connect("clicked", lambda _: self.window.set_keep_below(True))
        self.builder.get_object("nebicloud_login").connect("clicked", self.login)
        self.builder.get_object("howdy_setup").connect("clicked", self.howdy)
        self.builder.get_object("kdeconnect_begin").connect("clicked", self.pair)
        self.builder.get_object("accessibility").connect("clicked", self.accessibility)
        self.builder.get_object("lightGTK").connect("clicked", lambda _: self.changeTheme(""))
        self.builder.get_object("darkGTK").connect("clicked", lambda _: self.changeTheme("-Dark"))
        self.builder.get_object("keyboard").connect("clicked", self.keyboard)
        try:
                self.builder.get_object("keylang").set_text("Current Keyboard Language: " + str(subprocess.check_output(["dconf", "read", "/org/mate/desktop/peripherals/keyboard/kbd/layouts"])).split("[")[1].split("]")[0].upper().replace("'", ""))
        except:
                self.builder.get_object("keylang").set_text("Current Keyboard Language: None")

        self.window = self.builder.get_object("mainWin")
        self.window.set_wmclass("nebide-welcome", "nebide-welcome")
        self.window.resize(640,480)
        self.window.show_all()

    def login(self, btn):
        os.system("com.nebisoftware.nebios.nebicloud_integration &")
        
    def howdy(self, btn):
        os.system("dex /usr/share/applications/biolock.desktop")

    def pair(self, btn):
        self.window.hide()
        self.window.show_all()
        os.system("kdeconnect-app")

    def accessibility(self, btn):
        self.window.hide()
        self.window.show_all()
        os.system("mate-at-properties")
        
    def changeTheme(self, variant):
    	os.system("dconf write /org/mate/desktop/interface/gtk-theme \"'NebiDE" + variant + "'\" &")

    def keyboard(self, btn):
        self.window.hide()
        self.window.show_all()
        os.system("mate-keyboard-properties")
        try:
                self.builder.get_object("keylang").set_text("Current Keyboard Language: " + str(subprocess.check_output(["dconf", "read", "/org/mate/desktop/peripherals/keyboard/kbd/layouts"])).split("[")[1].split("]")[0].upper().replace("'", ""))
        except:
                self.builder.get_object("keylang").set_text("Current Keyboard Language: None")



    def goBack(self, btn):
        pages = self.stack.get_children()
        cur_page = self.stack.get_visible_child()
        i = pages.index(cur_page)
        if i == 0: return
        self.stack.set_visible_child(pages[i - 1])

    def goNext(self, btn):
        pages = self.stack.get_children()
        cur_page = self.stack.get_visible_child()
        i = pages.index(cur_page)
        if i == len(pages) - 1: self.endWelcome()
        self.stack.set_visible_child(pages[i + 1])

    def endWelcome(self):
        os.system(self.restartWMcmd)
        exit(0)

if __name__ == '__main__':
    try:
        gui = MyApp()
        Gtk.main()
    except KeyboardInterrupt:
        pass
