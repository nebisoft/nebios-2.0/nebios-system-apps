��    *      l  ;   �      �     �     �     �     �     �     �     �     �                    /     4     E     U     b  	   t     ~     �     �     �  	   �  !   �     �     �  	   �               !     3     K     Q     V  $   v     �     �     �     �     �  
   �     �  .  �     *     2     A     I     i  
   p     {     �  	   �     �     �  
   �     �     �          !  	   6     @     P     f     x       -   �     �     �  
   �     �     
	     	     3	     K	     T	  "   `	  -   �	  ,   �	  
   �	     �	     �	     	
     
     
         	   %                          
                        (                  $           #             "         *                             &   '      !                                        )       Add Bullet List Cancel Choose a file to open Close Columns: Create a new document. Crop Delete Delete Image Document folder location: Find Float above text In line of text Insert Table Lock aspect ratio NebiTyper New File No Documents Open Numbered List Open Open File Open a document to begin editing. Open a file Open a saved document. Paragraph Preferences Print this file Restore this file Rich Text Format (.rtf) Rows: Save Save file with a different name Save this file with a different name Select Document Folder… Subtitle Table Table Properties Title Two-Column Untitled Document %i Project-Id-Version: com.nebisoftware.nebityper
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-21 17:11+0200
Last-Translator: NathanBnm
Language-Team: none
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter Liste à puces Annuler Choisissez un fichier à ouvrir Fermer Colonnes : Créer un nouveau document. Rogner Supprimer Supprimer l'image Emplacement des documents : Rechercher Flotter au-dessus du texte Aligner au texte Insérer un tableau Verrouiller le ratio NebiTyper Nouveau fichier Aucun document ouvert Liste numérotée Ouvrir Ouvrir un fichier Ouvrir un document pour commencer l'édition. Ouvrir un fichier Ouvrir un document enregistré. Paragraphe Préférences Imprimer ce fichier Restaurer ce fichier Format Rich Text (.rtf) Lignes : Enregistrer Enregistrer avec un nom différent Enregistrer ce fichier avec un nom différent Sélectionnez l'emplacement des documents… Sous-titre Tableau Propriétés du tableau Titre Deux colonnes Document sans titre %i 