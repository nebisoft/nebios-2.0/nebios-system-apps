��    J      l  e   �      P     Q     _     c     s     �     �     �     �     �  4   �               "     /     <     H     ^      p     �     �     �     �     �     �     �     �     �          &     +     <     L     \     i     v     �     �     �     �     �  	   �     �     �     �     �  	    	  !   
	     ,	     8	  	   O	     Y	     e	      u	     �	     �	     �	     �	     �	  $   �	     
     *
      ?
     `
     o
     x
     ~
     �
     �
     �
     �
     �
  
   �
     �
  c  �
     Y  	   m     w     �     �     �     �     �       Z     	   v  	   �     �     �     �     �     �      �       	        #  
   <     G     ^     j  %   �      �     �     �     �     �               (     <     J     Y     h     x     �     �     �     �     �     �     �  )        ,     =  	   [  
   e     p  *   �     �     �     �     �  %   �  (        9  (   Q  3   z     �  
   �     �     �     �     �                "     7     E         ?   %       0   E   )      2   /                 "   A   3       J                4         I           $                  .                   9   G             =   ,   1   #   >   	          D              :          @   6       (       5   !              <   8      -   ;             7   
                           &   C   H         B       F       '   +       *    %s — Writer Add Add a new table Align image left Align image right Align table left Align table right Align text left Align text right Allow to resize images without changing aspect ratio Bullet List Cancel Center image Center table Center text Choose a file to open Choose font color Choose font family and font size Close Columns: Create a new document. Crop Decrease indent Delete Delete Image Delete selected image Delete selected table Document folder location: Find Float above text In line of text Increase indent Insert Table Insert image Insert link Insert table Insert text Justify table Justify text Lock aspect ratio NebiTyper New File No Documents Open Numbered List Open Open File Open a document to begin editing. Open a file Open a saved document. Paragraph Preferences Print this file Remove unnecessary part of image Restore this file Rich Text Format (.rtf) Rows: Save Save file with a different name Save this file with a different name Select Document Folder… Set how to wrap text Set properties of selected table Set text style Subtitle Table Table Properties Title Toggle bold Toggle italic Toggle strikethrough Toggle underline Two-Column Untitled Document %i Project-Id-Version: com.nebisoftware.nebityper
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-08-22 13:07+0200
Last-Translator: Heimen Stoffels <vistausss@outlook.com>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.1
 %s - Tekstverwerker Toevoegen Voeg een tabel toe Afbeelding links uitlijnen Afbeelding rechts uitlijnen Tabel links uitlijnen Tabel rechts uitlijnen Tekst links uitlijnen Tekst rechts uitlijnen Sta toe dat afbeeldingen andere afmetingen krijgen zonder de beeldverhouding aan te passen Opsomming Annuleren Afbeelding centreren Tabel centreren Tekst centreren Kies een te openen bestand Letterkleur instellen Lettertype en -grootte instellen Sluiten Kolommen: Maak een nieuw document. Bijsnijden Inspringing verkleinen Verwijderen Afbeelding verwijderen Verwijder de geselecteerde afbeelding Verwijder de geselecteerde tabel Locatie van documentenmap: Zoeken Boven tekst Tussen tekst Inspringing vergroten Tabel invoegen Afbeelding invoegen Link invoegen Tabel invoegen Tekst invoegen Tabel uitvullen Tekst uitvullen Beeldverhouding vergrendelen Tekstverwerker Nieuw bestand Geen geopende documenten Genummerde lijst Openen Bestand openen Open een document om aan de slag te gaan. Open een bestand Open een opgeslagen document. Paragraaf Voorkeuren Druk dit bestand af Verwijder onnodige delen van de afbeelding Herstel dit bestand Rich Text Format (.rtf) Rijen: Opslaan Bestand opslaan onder een andere naam Sla dit bestand op onder een andere naam Documentenmap kiezen… Stel in hoe tekst moet worden omgeslagen Stel de eigenschappen van de geselecteerde tabel in Tekststijl instellen Ondertitel Tabel Tabeleigenschappen Titel Vetgedrukt aan/uit Cursief aan/uit Doorhalen aan/uit Onderstrepen aan/uit Twee kolommen Naamloos document %i 