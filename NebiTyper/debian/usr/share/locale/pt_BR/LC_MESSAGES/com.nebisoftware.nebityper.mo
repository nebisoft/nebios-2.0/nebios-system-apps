��    R      �  m   <      �  2   �  8   $     ]     k     o          �     �     �     �     �  4   �  *        F     R     Y     f     s  #        �     �      �     �     �     �     	     	     '	     .	     ;	     Q	     g	     �	     �	     �	  #   �	     �	     �	     �	     �	     
     
     
     (
     5
  	   G
     Q
     Z
     l
     z
  	   
  !   �
     �
     �
  	   �
     �
     �
     �
      �
          ,     3     K     Q     V  $   v     �     �      �     �     �          	                ,     :     O  
   `     k     p  #  �  4   �  4   �       	   !     +     E     `     z     �     �     �  5   �  3        L     [     d     w     �  1   �     �     �  "        &     -     6     O     V     e     m     |     �  %   �  	   �     �     �  +        9     H     W     f     s     �     �     �     �  	   �     �     �     �            )        C     Q  
   k     v     �     �  &   �     �     �     �          	       "   .  !   Q     s  *   �     �  
   �     �     �     �          	       
        %     2     ;         N          M   E                       %   "           4   8   1   Q             
                 '         :   ;   0   +   F      -   9   @      &   /           2                   B      J       ,         3   )       $          5   <           R         #       =   7          G   K   *   H       O       6             ?   I      	            >   P   C          L      .   !       A   D       (    %d character with spaces %d characters with spaces %d character without spaces %d characters without spaces %s — Writer Add Add a new table Align image left Align image right Align table left Align table right Align text left Align text right Allow to resize images without changing aspect ratio Are you sure you want to revret this file? Bullet List Cancel Center image Center table Center text Changes you made will be discarded. Choose a file to open Choose font color Choose font family and font size Close Columns: Create a new document. Crop Decrease indent Delete Delete Image Delete selected image Delete selected table Document folder location: Find Float above text In line of text Include spaces to count characters: Increase indent Insert Table Insert image Insert link Insert table Insert text Justify table Justify text Lock aspect ratio NebiTyper New File No Documents Open Numbered List Open Open File Open a document to begin editing. Open a file Open a saved document. Paragraph Preferences Print this file Redo Remove unnecessary part of image Restore this file Revert Rich Text Format (.rtf) Rows: Save Save file with a different name Save this file with a different name Select Document Folder… Set how to wrap text Set properties of selected table Set text style Subtitle Table Table Properties Title Toggle bold Toggle italic Toggle strikethrough Toggle underline Two-Column Undo Untitled Document %i Project-Id-Version: com.nebisoftware.nebityper
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-08-18 22:30+0900
Last-Translator: Matheus T. Ricci
Language:pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 %d caractere com espaços %d caracteres com espaços %d caractere sem espaços %d caracteres sem espaços %s — Writer Adicionar Adicionar uma nova tabela Alinhar imagem à esquerda Alinhar imagem à direita Alinhar tabela à esquerda Alinhar tabela à direita Alinhar texto à esquerda Alinhar texto à direita Permitir redimensionar imagens sem mudar proporções Você tem certeza que deseja reverter esse arquivo? Lista pontuada Cancelar Centralizar imagem Centralizar tabela Centralizar texto As alterações que você fez serão descartadas. Escolher um arquivo para abrir Escolher cor da fonte Escolher estilo e tamanho da fonte Fechar Colunas: Criar um novo documento. Cortar Diminuir recuo Deletar Deletar imagem Deletar imagem selecionada Deletar tabela selecionada Localização da pasta de Documentos: Encontrar Flutuar sobre o texto Alinhado com o texto Incluir espaços na contagem de caracteres: Aumentar recuo Inserir tabela Inserir imagem Inserir link Inserir tabela Inserir texto Justificar tabela Justificar texto Travar proporções NebiTyper Novo arquivo Nenhum documento aberto Lista numerada Abrir Abrir arquivo Abra um documento para começar a editar. Abrir arquivo Abrir um documento salvo. Parágrafo Preferências Imprimir esse arquivo Refazer Remover parte desnecessária da imagem Restaurar esse arquivo Reverter Rich Text Format (.rtf) Linhas: Salvar Salvar arquivo com outro nome Salvar esse arquivo com outro nome Selecionar pasta de Documentos… Definir como cortar texto Definir propriedades da tabela selecionada Definir estilo do texto Subtítulo Tabela Propriedades da Tabela Título Negrito Itálico Tachado Sublinhado Duas colunas Desfazer Documento Sem Título %i 