import os
import gi

gi.require_version('Gtk', '3.0')

from gi.overrides import Gtk
from gi.repository import Gtk

class RecoveryModeClient(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)

        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/main.glade")

        self.window = self.builder.get_object("main-window")
        self.window.connect("destroy", lambda _: os.system("reboot & killall awesome"))
        quitRec = self.builder.get_object("quitBtn")
        quitRec.connect("clicked", lambda _: os.system("reboot & killall awesome"))

        installNebi = self.builder.get_object("installNebi")
        installNebi.connect("clicked", self.installNebiOS)

        openTerm = self.builder.get_object("openTerm")
        openTerm.connect("clicked", self.openTerminal)

        diskUtil = self.builder.get_object("diskUtil")
        diskUtil.connect("clicked", self.diskUtility)

        self.window.show()

    def installNebiOS(self, arg1):
        self.window.hide()
        self.window.show()
        install_app = os.system("ubiquity")
        if install_app != 0:
            os.system("zenity --info --text=\"You're not booted NebiOS from USB stick. Redirecting you to NebiOS download page.\"")
            nm_service = os.system("sudo systemctl start NetworkManager")
            firefox_app = os.system("firefox https://nebisoftware.com/nebios/download.html")
            udisks2_service = os.system("sudo systemctl start udisks2")
            disks_app = os.system("gnome-disks")
            if nm_service != 0 or firefox_app != 0 or udisks2_service != 0 or disks_app != 0:
                os.system('zenity --error --text="Failed to Install NebiOS. No luck..."')

    def openTerminal(self, arg1):
        self.window.hide()
        self.window.show()
        os.system("x-terminal-emulator")

    def diskUtility(self, arg1):
        self.window.hide()
        self.window.show()
        udisks2_service = os.system("sudo systemctl start udisks2")
        disks_app = os.system("gnome-disks")
        if udisks2_service != 0 or disks_app != 0:
            os.system('zenity --error --text="Failed to launch Disk Utility."')

mw = RecoveryModeClient()
Gtk.main()
