#!/bin/env python3
import gi, os, cairo, requests, signal
from sympy import sympify
gi.require_version('Gdk', '3.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gio', '2.0')
from gi.repository import Gtk, Gio, GdkPixbuf, Pango, Gdk, GLib

class LauncherWindow(Gtk.Window):
    def show_launcher(self, signum, frame):
        self.appinfo_list = Gio.AppInfo.get_all()
        self.InitBox()
        self.show_all()
        self.fullscreen()
        
    def __init__(self):
        Gtk.Window.__init__(self, title="Launcher")
        self.set_wmclass("NebiDE_launcher", "NebiDE_launcher")
        signal.signal(signal.SIGUSR1, self.show_launcher)

        self.columns = 5

        screen: Gdk.Screen = Gdk.Screen.get_default()
        self.dpi = screen.get_resolution() / 96
        self.screen_width = screen.get_width()

        visual = screen.get_rgba_visual()
        if visual is None or not screen.is_composited():
            visual = screen.get_system_visual()
        self.set_visual(visual)

        self.show_all()
        self.fullscreen()

        self.appinfo_list = Gio.AppInfo.get_all()

        winbox = Gtk.VBox()

        search = Gtk.SearchEntry()
        search.set_placeholder_text("Global Search...")
        search.set_margin_top(20 * self.dpi)
        search.set_margin_start(20 * self.dpi)
        search.set_margin_end(20 * self.dpi)
        search.connect("search-changed", self.InitFindBox)
        winbox.pack_start(search, False, True, 0)

        self.search = search

        scroll = Gtk.ScrolledWindow()
        scroll.set_margin_start(2 * self.dpi)
        scroll.set_margin_end(2 * self.dpi)
        viewport = Gtk.Viewport()
        scroll.add(viewport)

        self.get_style_context().add_class("transparent-bg")
        winbox.get_style_context().add_class("transparent-bg")
        scroll.get_style_context().add_class("transparent-bg")
        viewport.get_style_context().add_class("transparent-bg")

        self.blacklist = (
            "mate-about",
            "apturl %u",
            "/usr/share/apport/apport-gtk -c %f",
            "/usr/libexec/gnome-initial-setup",
            "feh %F",
            "/usr/bin/display-im6.q16hdri -nostdin %F",
            "/usr/bin/display-im6.q16 -nostdin %F",
            "mate-window-properties"
        )

        self.xdg_categories = {
            "Windows Container": ("Wine",),
            "Internet": ("Network",),
            "Games": ("Game",),
            "Multimedia": ("AudioVideo", "Audio", "Video"),
            "Office": (),
            "Graphics": (),
            "Development": (),
            "Education": (),
            "Science": (),
            "Settings": (),
            "System": (),
            "Utilities": ("Utility",),
            "Unknown": ()
        }

        self.xdg_category_icons = {
            "Windows Container": "application-x-ms-dos-executable",
            "Internet": "applications-internet",
            "Games": "applications-games",
            "Multimedia": "applications-multimedia",
            "Office": "applications-office",
            "Graphics": "applications-graphics",
            "Development": "applications-development",
            "Education": "applications-education",
            "Science": "applications-science",
            "Settings": "preferences-system",
            "System": "applications-system",
            "Utilities": "applications-utilities",
            "Unknown": "applications-all"
        }

        self.box = Gtk.VBox()
        self.box.set_margin_bottom(2 * self.dpi)
        self.box.set_homogeneous(False)
        self.box.get_style_context().add_class("transparent-bg")
        self.box.set_border_width(16 * self.dpi)
        self.box.set_spacing(12 * self.dpi)
        self.cbox = Gtk.VBox()
        self.cbox.pack_start(self.box, False, True, 0)
        viewport.add(self.cbox)
        winbox.pack_start(scroll, True, True, 0)
        self.add(winbox)

        self.show_all()
        self.set_border_width(32 * self.dpi)
        self.set_position(Gtk.WindowPosition.CENTER)

        GLib.timeout_add(1000, self.InitBox)

        self.connect("destroy", Gtk.main_quit)
        self.connect('draw', self.draw)

        # draw fonksiyonuna aşağıdaki satırı ekleyin
        self.connect("key-press-event", self.on_key_press_event)

    def on_key_press_event(self, widget, event):
        key = event.keyval
        if key == Gdk.KEY_Escape:
            self.PrepareToQuit()

    def InitBox(self):
        self.clear_box()
        spinner = Gtk.Spinner()
        spinner.start()

        self.box.pack_start(spinner, True, True, 0)
        self.box.show_all()

        def update_gui():
            thegrid = self.populate_cgrid()
            if thegrid is not None:
                spinner.stop()
                self.clear_box()
                self.box.pack_start(thegrid, True, True, 0)
                self.box.show_all()

            self.show_and_leave_thread()

        GLib.idle_add(lambda: update_gui())

    def InitAppBox(self, button, category):
        self.clear_box()

        spinner = Gtk.Spinner()
        spinner.start()

        self.box.pack_start(spinner, True, True, 0)
        self.box.show_all()

        def update_gui():
            thegrid = self.populate_grid(category, self.xdg_categories[category], "", True)
            if thegrid is not None:
                spinner.stop()
                self.box.pack_start(thegrid, True, True, 0)

            self.show_and_leave_thread()

        GLib.idle_add(update_gui)

    def generate_math_result_button(self, problem, result):
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=16)

        # Calculator icon
        calculator_icon = Gtk.Image.new_from_icon_name("accessories-calculator", Gtk.IconSize.DIALOG)
        box.pack_start(calculator_icon, False, False, 0)

        # Math problem and result
        label = Gtk.Label(f"{problem} = {result}")
        box.pack_start(label, False, True, 0)

        # Flexible space
        spacer = Gtk.Label()
        spacer.set_hexpand(True)
        box.pack_start(spacer, True, True, 0)

        # Copy icon
        copy_icon = Gtk.Image.new_from_icon_name("edit-copy", Gtk.IconSize.BUTTON)
        box.pack_start(copy_icon, False, False, 0)

        button = Gtk.Button()
        button.get_style_context().add_class("app-btn")
        button.set_always_show_image(True)
        button.add(box)

        # Connect the button to copy the result to the clipboard
        button.connect("clicked", self.copy_math_result, result)

        return button

    def copy_math_result(self, button, result):
        clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        clipboard.set_text(str(result), -1)
        clipboard.store()


    def InitSearch(self, search=Gtk.SearchEntry(), engine_index=0):

        query = search.get_text()

        spinner = Gtk.Spinner()
        spinner.start()

        is_math_expression = any(char in query for char in "+-*/")

        if is_math_expression:
            try:
                result = sympify(query)
                is_math_problem = True
            except:
                is_math_problem = False
        else:
            is_math_problem = False

        if is_math_problem:
            math_result_button = self.generate_math_result_button(query, result)
            self.box.pack_start(math_result_button, False, True, 0)

        for i in self.xdg_categories:
            thegrid = self.populate_grid(i, self.xdg_categories[i], query, False, False)
            if thegrid is not None and search.get_text() != "":
                self.box.pack_start(thegrid, True, True, 0)

        self.box.pack_start(spinner, True, True, 0)
        self.box.show_all()

        # Add a title for search suggestions
        search_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        catlabel = Gtk.Label()
        catlabel.set_markup("<b><span size='xx-large'>Search suggestions from</span></b>")
        catlabel.set_alignment(0.0, 0.5)
        catlabel.set_margin_start(16 * self.dpi)
        search_box.pack_start(catlabel, False, False, 0)

        thedirgrid = self.populate_grid("My Files", ("MyFiles",), query, False, False)
        if thedirgrid is not None and search.get_text() != "":
            self.box.pack_start(thedirgrid, True, True, 0)

        self.search_engines = Gtk.ComboBoxText()
        self.search_engines.append_text("DuckDuckGo")
        self.search_engines.append_text("Google")
        self.search_engines.set_active(engine_index)
        self.search_engines.connect("changed", self.on_search_engine_changed)
        search_box.pack_start(self.search_engines, False, False, 0)
        
        self.SuggestFinish = False
        self.get_search_suggestions(query) 

        while self.SuggestFinish == False:
            Gtk.main_iteration()
            
        if self.SuggestFinish == True:
            self.box.pack_start(search_box, False, True, 0)
            suggestions = self.suggestions if not self.SearchOK else []
            for suggestion in suggestions:
                button = self.GenerateSearchButton(suggestion)
                self.box.pack_start(button, True, True, 1)
            else:
                print("Error fetching search suggestions.")


        spinner.stop()
        self.SearchOK = True
        self.box.show_all()

    def search_folders(self, query):
        folders = []

        home_directory = os.path.expanduser("~")
        for root, dirs, files in os.walk(home_directory):
            for folder in dirs:
                if query.lower() in folder.lower():
                    folder_path = os.path.join(root, folder)
                    if folder_path.__contains__("/.") == False:
                        folders.append((folder, folder_path))

        return folders

    def GenerateFolderButton(self, folder_name, folder_path):
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        # Folder icon
        folder_icon = Gtk.Image.new_from_icon_name("folder", Gtk.IconSize.DIALOG)
        folder_icon.set_pixel_size((self.get_allocation().width * 0.4 / self.columns) * self.dpi)
        box.pack_start(folder_icon, True, True, 0)

        # Folder name
        name_label = Gtk.Label.new(folder_name)
        name_label.set_ellipsize(Pango.EllipsizeMode.END)
        box.pack_start(name_label, False, False, 0)

        # Folder path
        path_label = Gtk.Label.new(folder_path)
        path_label.set_ellipsize(Pango.EllipsizeMode.END)
        box.pack_start(path_label, False, False, 0)

        button = Gtk.Button()
        button.get_style_context().add_class("app-btn")
        button.set_always_show_image(True)
        box.set_halign(Gtk.Align.CENTER)
        box.set_valign(Gtk.Align.CENTER)
        box.set_size_request(-1, (self.get_allocation().width * 0.6 / self.columns) * self.dpi)
        button.add(box)

        # Connect the button to open the folder
        button.connect("clicked", self.open_folder, folder_path)
        Gtk.main_iteration()
        return button

    def open_folder(self, button, folder_path):
        # Use the appropriate file manager command based on the user's system
        file_manager_command = "xdg-open"
        os.system(f"{file_manager_command} {folder_path}")
        self.PrepareToQuit()

    def GenerateSearchButton(self, suggestion):
      box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=16)

      # Arama simgesi
      search_icon = Gtk.Image.new_from_icon_name("edit-find", Gtk.IconSize.BUTTON)
      box.pack_start(search_icon, False, False, 0)

      # Arama metni
      label = Gtk.Label(suggestion)
      box.pack_start(label, False, True, 0)

      # Boşluk
      spacer = Gtk.Label()
      spacer.set_hexpand(True)
      box.pack_start(spacer, True, True, 0)

      # Ok simgesi
      arrow_icon = Gtk.Image.new_from_icon_name("go-next", Gtk.IconSize.BUTTON)
      box.pack_start(arrow_icon, False, False, 0)

      button = Gtk.Button()
      button.get_style_context().add_class("app-btn")
      button.set_always_show_image(True)
      button.add(box)

      # connect the button to the callback function
      button.connect("clicked", self.on_search_suggestion_clicked, suggestion)
      Gtk.main_iteration()
      return button

    def InitFindBox(self, search=Gtk.SearchEntry(), engine_index=0):
        self.clear_box()
        if search.get_text() == "":
            self.InitBox()
        else:
            self.SearchOK = False
            GLib.idle_add(self.InitSearch, search, engine_index)

    def on_search_engine_changed(self, combo):
        engine_index = combo.get_active()
        engine_text = combo.get_model()[engine_index][0]
        self.InitFindBox(self.search, engine_index)

    def get_search_suggestions(self, query):
        # Modify this method to use the selected search engine
        selected_engine = self.search_engines.get_model()[self.search_engines.get_active()][0]
        base_url = ""

        if selected_engine == "DuckDuckGo":
            base_url = "https://duckduckgo.com/ac/"
        elif selected_engine == "Google":
            base_url = "http://suggestqueries.google.com/complete/search"

        params = {
            "q": query,
            "output": "firefox"
        }

        def download_data():
            try:
                response = requests.get(base_url, params=params, stream=True)
                response.raise_for_status()
                data = response.json()
                if selected_engine == "DuckDuckGo":
                    suggestions = [result["phrase"] for result in data]
                elif selected_engine == "Google":
                    suggestions = data[1]
                self.SuggestFinish = True
                self.suggestions = suggestions

            except requests.exceptions.RequestException as e:
                print(f"Error fetching search suggestions: {e}")
                # Call the process_data function with None to indicate an error
                self.suggestions = None

        # Execute download_data asynchronously
        suggestions = GLib.idle_add(download_data)

    def on_search_suggestion_clicked(self, button, suggestion):
        print(f"Search suggestion clicked: {suggestion}")
        selected_search_engine = self.get_selected_search_engine()
        search_url = self.construct_search_url(selected_search_engine, suggestion)
        self.open_uri(search_url)

    def get_selected_search_engine(self):
        # Assuming you have a ComboBox named 'search_engine_combo'
        combo = self.search_engines
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            return model[tree_iter][0]  # Assuming the first column of the model is the search engine name
        return None

    def construct_search_url(self, search_engine, query):
        # Add logic to construct the search URL based on the selected search engine and the query
        if search_engine == "DuckDuckGo":
            return f"https://duckduckgo.com/?q={query}"
        elif search_engine == "Google":
            return f"https://www.google.com/search?q={query}"
        else:
            # Add support for other search engines as needed
            return None

    def open_uri(self, uri):
        app_info = Gio.AppInfo.create_from_commandline(
            f"xdg-open {uri}",
            None,
            Gio.AppInfoCreateFlags.NONE
        )
        if app_info:
            app_info.launch(None, None)
            self.PrepareToQuit()
        else:
            print(f"Failed to create AppInfo for {uri}")

    def clear_box(self):
        for child in self.box.get_children():
            self.box.remove(child)

    def show_and_leave_thread(self):
      Gdk.threads_leave()  # İş parçacığı güvenliğini kaldır
      Gtk.main_iteration()
      self.box.show_all()

    def populate_grid(self, category, aliases=(), query="", show_back=False, upd_ui=True):
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        aliases += (category,)
        apps = ()

        row = 0
        col = 0

        if category == "My Files":
            folder_results = self.search_folders(query)[0:(2 * self.columns)] if self.SearchOK == False else []
            for f_name, f_path in folder_results:
                apps += (f_path,)
                button = self.GenerateFolderButton(f_name, f_path)
                grid.attach(button, col, row, 1, 1)
                col += 1
                if col == self.columns:
                    col = 0
                    row += 1
        else:
            for appinfo in self.appinfo_list:
                if "Wine" in aliases:
                    if str(appinfo.get_startup_wm_class()).endswith(".exe"):
                        name = appinfo.get_display_name()
                        if query != "":
                            if str(name.lower()).__contains__(query.lower()):
                                apps += (appinfo,)
                                button = self.GenerateIcon(appinfo, upd_ui)
                                grid.attach(button, col, row, 1, 1)
                                col += 1
                                if col == self.columns:
                                    col = 0
                                    row += 1
                        else:
                            apps += (appinfo,)
                            button = self.GenerateIcon(appinfo, upd_ui)
                            grid.attach(button, col, row, 1, 1)
                            col += 1
                            if col == self.columns:
                                col = 0
                                row += 1
                else:
                    if ((appinfo.get_categories() is not None) ^ (category == "Unknown")) and not appinfo.get_commandline() in self.blacklist:
                        categories = str(appinfo.get_categories()).split(";")
                        if categories == ["None"]:
                            categories = ["Unknown",]
                        keywords = appinfo.get_keywords()
                        name = str(appinfo.get_display_name()).replace("GNOME ", "").replace("MATE ", "")
                        print(name, categories, appinfo.get_nodisplay())
                        for cat in categories:
                            if cat in aliases and not appinfo in apps and (appinfo.get_nodisplay() == False):
                                if query != "":
                                    if (str(name.lower()).__contains__(query.lower()) or query.lower() in keywords):
                                        apps += (appinfo,)
                                        button = self.GenerateIcon(appinfo, upd_ui)
                                        grid.attach(button, col, row, 1, 1)
                                        col += 1
                                        if col == self.columns:
                                            col = 0
                                            row += 1
                                else:
                                    apps += (appinfo,)
                                    button = self.GenerateIcon(appinfo, upd_ui)
                                    grid.attach(button, col, row, 1, 1)
                                    col += 1
                                    if col == self.columns:
                                        col = 0
                                        row += 1

        if col > 0:
            for i in range(self.columns):
                if grid.get_child_at(i, row) is None:
                    grid.attach(Gtk.Label(), i, row, 1, 1)

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
        catlabel = Gtk.Label()
        catlabel.set_markup("<b><span size='xx-large'>" + category + "</span></b>")
        catlabel.set_alignment(0.0, 0.5)
        catlabel.set_margin_start(16 * self.dpi)

        # Geri tuşunu bir simge içeren bir düğme olarak oluştur
        btn = Gtk.Button()
        btn.get_style_context().add_class("back-btn")
        btn.add(Gtk.Image.new_from_icon_name("go-previous", Gtk.IconSize.BUTTON))
        btn.connect("clicked", lambda _: self.InitBox())

        if show_back == True:
            box.pack_start(btn, False, False, 0)

        box.pack_start(catlabel, True, True, 0)
        if len(apps) > 0:
            self.box.pack_start(box, True, True, 0)

        if len(apps) > 0:
            return grid
        else:
            if show_back == True:
                return box
            else:
                return None

    def populate_cgrid(self):
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        apps = ()

        row = 0
        col = 0
        for appinfo in self.xdg_categories.keys():
            apps += (appinfo,)
            button = self.GenerateCatIcon(appinfo, self.xdg_category_icons[appinfo])
            grid.attach(button, col, row, 1, 1)
            col += 1
            if col == self.columns:
                col = 0
                row += 1

        if col > 0:
            for i in range(self.columns):
                if grid.get_child_at(i, row) is None:
                    grid.attach(Gtk.Label(), i, row, 1, 1)

        if len(apps) > 0:
            return grid
        else:
            return None

    def GenerateIcon(self, appinfo, upd_ui=True):
        name = str(appinfo.get_display_name()).replace("GNOME ", "").replace("MATE ", "")
        icon = appinfo.get_icon()

        # create a box to hold the icon and name
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        if icon is not None:
            if isinstance(icon, Gio.FileIcon):
                iconfile = icon.get_file().get_path()
                if os.path.exists(iconfile):
                    pixicon_pre = GdkPixbuf.Pixbuf.new_from_file(iconfile).scale_simple(
                        (self.get_allocation().width * 0.4 / self.columns) * self.dpi,
                        (self.get_allocation().width * 0.4 / self.columns) * self.dpi
                        , GdkPixbuf.InterpType.BILINEAR)
                    pixicon = Gtk.Image.new_from_pixbuf(pixicon_pre)
                else:
                    pixicon = Gtk.Image.new_from_icon_name("executable", Gtk.IconSize.DIALOG)
            else:
                pixicon = pixicon = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.DIALOG)
        else:
            pixicon = Gtk.Image.new_from_icon_name("executable", Gtk.IconSize.DIALOG)

        pixicon.set_pixel_size((self.get_allocation().width * 0.4 / self.columns) * self.dpi)

        box.pack_start(pixicon, True, True, 0)

        label = Gtk.Label.new(name)
        label.set_ellipsize(Pango.EllipsizeMode.END)
        box.pack_start(label, False, False, 0)

        button = Gtk.Button()
        button.get_style_context().add_class("app-btn")
        button.set_always_show_image(True)
        box.set_halign(Gtk.Align.CENTER)
        box.set_valign(Gtk.Align.CENTER)
        box.set_size_request(-1, (self.get_allocation().width * 0.6 / self.columns) * self.dpi)
        button.add(box)

        # connect the button to launch the app
        button.connect("clicked", self.launch_app, appinfo)

        # Connect secondary click event
        button.connect("button-press-event", self.on_button_press_event, appinfo)

        if upd_ui:
            Gtk.main_iteration()

        return button

    def on_button_press_event(self, widget, event, appinfo):
        if event.button == Gdk.BUTTON_SECONDARY:
            menu = Gtk.Menu()
            pin_item = Gtk.MenuItem.new_with_label("Pin to Utility Panel")
            pin_item.connect("activate", self.pin_to_utility_panel, appinfo)
            menu.append(pin_item)

            # Add other items if needed

            menu.show_all()
            menu.popup(None, None, None, None, event.button, event.time)
            return True

    def pin_to_utility_panel(self, widget, appinfo):
        desktop_file = appinfo.get_filename()  # Get the .desktop file's path
        if desktop_file:
            # Append the path to the apps.txt file
            username = os.getenv("USER")  # Get the username from the environment variables
            apps_txt_path = f"/Users/{username}/.config/NebiSoft/NebiDE.shell.data/widget/pinned-apps/apps.txt"
            with open(apps_txt_path, "a") as file:
                file.write(desktop_file + "\n")

            # Update the UI by emitting the signal
            cmd = 'awesome-client \'awesome.emit_signal("pinned_apps::update")\''
            os.system(cmd)

    def GenerateCatIcon(self, name, icon):

        # create a box to hold the icon and name
        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)

        if icon is not None:
            pixicon = pixicon = Gtk.Image.new_from_icon_name(icon, Gtk.IconSize.DIALOG)
        else:
            pixicon = Gtk.Image.new_from_icon_name("executable", Gtk.IconSize.DIALOG)

        pixicon.set_pixel_size((self.get_allocation().width * 0.4 / self.columns) * self.dpi)

        box.pack_start(pixicon, True, True, 0)

        label = Gtk.Label.new(name)
        label.set_ellipsize(Pango.EllipsizeMode.END)
        box.pack_start(label, False, False, 0)

        button = Gtk.Button()
        button.get_style_context().add_class("app-btn")
        button.set_always_show_image(True)
        box.set_halign(Gtk.Align.CENTER)
        box.set_valign(Gtk.Align.CENTER)
        box.set_size_request(-1, (self.get_allocation().width * 0.6 / self.columns) * self.dpi)
        button.add(box)

        # connect the button to launch the app
        button.connect("clicked", lambda _: GLib.idle_add(self.InitAppBox, None, name))
        Gtk.main_iteration()
        return button

    def launch_app(self, button, appinfo):
        print(appinfo.get_commandline())
        appinfo.launch([], None)
        self.PrepareToQuit()

    def PrepareToQuit(self):
        self.hide()

    def draw(self, widget, context):
        style = self.get_style_context()
        bgcolor = style.get_background_color(Gtk.StateType.NORMAL)
        context.set_source_rgba(0, 0, 0, 0.5)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

        self.cbox.set_margin_start(int(self.get_allocation().width * 0.1))
        self.cbox.set_margin_end(int(self.get_allocation().width * 0.1))
        self.search.set_margin_start(int(self.get_allocation().width * 0.35))
        self.search.set_margin_end(int(self.get_allocation().width * 0.35))

win = LauncherWindow()

CSS_DATA = """
.transparent-bg {
    background-color: transparent;
    color: #fff;
}

button {
    background-color: transparent;
    border: none;
    padding-bottom: 0px;
    color: #fff;
}

.app-btn {
    padding: 16px 12px;
    border: none;
}

button:hover {
    background-color: rgba(255, 255, 255, 0.2);
}

button:active {
    background-color: #ff5f5f;
}
"""

css_provider = Gtk.CssProvider()
css_provider.load_from_data(CSS_DATA.encode())
screen = Gdk.Screen.get_default()
context = win.get_style_context()
context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
GLib.threads_init()
Gtk.main()
