import locale
lang = locale.getlocale()[0]

# Default lang (en_US)
windowTitle = "Change wallpaper"
info = "Double click to change wallpaper."
uploadBtn = "Set a custom wallpaper (Only image)"
img = "Image"
liveWp = "Live Wallpaper"
if lang == "tr_TR":
    windowTitle = "Duvar kağıdı değiştir"
    info = "Duvar kağıdı değiştirmek için iki kere tıklayın."
    uploadBtn = "Özel duvar kağıdı kullan (Sadece imaj)"
    img = "İmaj"
    liveWp = "Canlı Duvar Kağıdı"