import os
import sys
import translations
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

default_wall = "Ant by NebiSoft ✕ Pok Rie.jpg"

default_wall_path = "/System/Wallpapers/Image/"
default_live_wall_path = "/System/Wallpapers/Live/"

bg_modes = ["--bg-scale", "--bg-center", "--bg-fill", "--bg-max", "--bg-tile"]


class LiveWallpaperSet(QWidget):
    def __init__(self):
        super().__init__()
        lay = QVBoxLayout()
        self.listView = QListWidget()
        self.listView.setFrameStyle(0)
        self.listView.setStyleSheet(
            "background-color: rgba(35, 35, 35, 0); color: #535353 !important; font-family: Karla;")
        self.listView.setViewMode(QListView.ListMode)

        files = []
        for filename in os.listdir(default_live_wall_path):
            if filename.endswith(".mp4") or filename.endswith(".mov") or filename.endswith(".mpeg"):
                files.append(filename)

        for file in files:
            print(default_wall_path + file)
            item = QListWidgetItem()
            icon = QIcon()
            icon.addPixmap(QPixmap(default_wall_path + file))
            item.setIcon(icon)
            item.setText(file)
            self.listView.addItem(item)

        self.listView.doubleClicked.connect(self.setWall)
        label = QLabel(translations.info)
        label.setStyleSheet("color: #a5a5a5 !important; font-family: Karla;")
        lay.addWidget(label)
        lay.addWidget(self.listView)
        self.listView.setIconSize(QSize(144, 144))
        self.setLayout(lay)

    def setWall(self):
        item = self.listView.currentItem()
        file = item.toolTip()
        comp_file = default_live_wall_path + file
        print(comp_file)
        os.system("rm ~/.vidbg")
        os.system("link '" + comp_file + "' ~/.vidbg")
        os.system("nebide-live-wallpaper ~/.vidbg")


class WallpaperSelector(QMainWindow):
    def customWallpaper(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "PNG Files (*.png);;JPG Files (*.jpg);;JPEG Files (*.jpeg)",
                                                  options=options)
        if fileName:
            settings = QSettings("NebiSoft", "NebiDE")
            wall_method = settings.value("wallpaper_method", "--bg-scale")
            settings.setValue("wallpaper", fileName)
            os.system("feh " + wall_method + " '" + fileName + "'")

    def bgMode(self):
        settings = QSettings("NebiSoft", "NebiDE")
        wall = settings.value("wallpaper", default_wall)
        settings.setValue("wallpaper_method", "--bg-" + self.bgModSwitcher.currentText().lower())
        wall_method = settings.value("wallpaper_method", "--bg-scale")
        os.system("feh " + wall_method + " '" + wall + "'")

    def __init__(self):
        super(WallpaperSelector, self).__init__()
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setWindowTitle(translations.windowTitle)
        self.tabs = QTabWidget()

        widget = QWidget()
        widget2 = QWidget()
        self.tabs.addTab(widget, translations.img)
        lws = LiveWallpaperSet()
        self.tabs.addTab(lws, translations.liveWp)
        widget2.setObjectName("widget")
        widget2.setStyleSheet(
            "#widget{background-color: rgba(255, 255, 255, 0);color: white !important; font-family: Karla;}");

        lay = QVBoxLayout()
        widget.setLayout(lay)
        lay2 = QVBoxLayout()
        widget2.setLayout(lay2)
        lay2.setContentsMargins(0, 0, 0, 0)
        lay2.addWidget(self.tabs)
        self.tabs.setStyleSheet("""
        QTabWidget::pane {
        font-family: Karla;
    background: rgba(255,255,255,0.94);
    border:0;
}

QTabBar::tab {
    width: 320px !important;
        font-family: Karla;
    background: rgba(255,255,255,0.75);
    color: #535353;
}
QTabBar::tab:hover {
    width: 320px !important;
        font-family: Karla;
    background: rgba(255,255,255,0.85);
    color: #535353;
}
QTabBar::tab:selected {
        font-family: Karla;
    background: rgba(255,255,255,0.94);
    color: #353535;
}

QTabWidget::tab-bar {
            alignment: center;
        }
        
QTabBar::scroller {width: 0px;}
        """)

        tb = QToolBar()
        tb.setObjectName("tb")
        tb.setStyleSheet("* {color: #353535 !important; font-family: Karla;font-weight: bold; background-color: rgba("
                         "255, 255, 255, 1); border: none;} QPushButton, QComboBox { border: 1px solid #c5c5c5; "
                         "padding: 4px 6px; border-radius: 6px;} #tb { padding: 8px; } QPushButton { padding: 6px; } "
                         "QPushButton::hover, QComboBox::hover { background: #c5c5c5; }")
        tb.setContextMenuPolicy(Qt.ContextMenuPolicy.PreventContextMenu)
        self.addToolBar(tb)
        tb.setMovable(False)

        self.uploadBtn = QPushButton(translations.uploadBtn)
        self.uploadBtn.clicked.connect(self.customWallpaper)
        tb.addWidget(self.uploadBtn)

        spaceItem = QWidget()
        spaceItem.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        tb.addWidget(spaceItem)

        self.bgModSwitcher = QComboBox()
        self.bgModSwitcher.setStyleSheet("""
        QComboBox::drop-down {
            border: none;
        }
        """)

        for i in bg_modes:
            mode_name = i.replace("--bg-", "").title()
            self.bgModSwitcher.addItem(mode_name)

        self.bgModSwitcher.currentTextChanged.connect(self.bgMode)

        tb.addWidget(self.bgModSwitcher)

        self.listView = QListWidget()
        self.listView.setFrameStyle(QFrame.NoFrame)
        self.listView.setFrameStyle(0)
        self.listView.setStyleSheet("QListWidget { background-color: rgba(35, 35, 35, 0); color: rgba(0,0,0,"
                                    "0) !important; font-family: Karla; }")
        self.listView.setViewMode(QListView.ViewMode.IconMode)
        self.listView.setResizeMode(QListView.Adjust)
        self.listView.setSpacing(8)
        self.listView.setIconSize(QSize(1, 1))

        files = []
        for filename in os.listdir(default_wall_path):
            if filename.endswith(".png") or filename.endswith(".jpg") or filename.endswith(".jpeg"):
                files.append(filename)

        for file in files:
            print(default_wall_path + file)
            item = QListWidgetItem()
            icon = QIcon()
            pixmap = QPixmap(default_wall_path + file).scaledToWidth(1920)
            cropped = QRect(0, int((pixmap.height() - 1080) / 2), 1920, 1080)
            radius = 48

            # create empty pixmap of same size as original
            rounded = QPixmap(pixmap.size())
            rounded.fill(QColor("transparent"))

            # draw rounded rect on new pixmap using original pixmap as brush
            painter = QPainter(rounded)
            painter.setRenderHint(QPainter.Antialiasing)
            painter.setBrush(QBrush(pixmap))
            painter.setPen(Qt.NoPen)
            painter.drawRoundedRect(cropped, radius, radius)
            painter.end()
            icon.addPixmap(rounded.copy(cropped))
            item.setIcon(icon)
            item.setToolTip(file)
            self.listView.addItem(item)

        self.listView.doubleClicked.connect(self.setWall)
        label = QLabel(translations.info)
        label.setStyleSheet("color: #a5a5a5 !important; font-family: Karla;")
        lay.addWidget(label)
        lay.addWidget(self.listView)
        self.setFixedSize(QSize(640, 480))
        self.listView.setIconSize(QSize(180, 180))
        self.listView.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOn)
        lay.setContentsMargins(16,16,16,0)
        self.setCentralWidget(widget2)

    def setWall(self):
        settings = QSettings("NebiSoft", "NebiDE")
        item = self.listView.currentItem()
        file = item.toolTip()
        comp_file = default_wall_path + file
        print(comp_file)
        wall_method = settings.value("wallpaper_method", "--bg-scale")
        settings.setValue("wallpaper", comp_file)
        os.system("feh " + wall_method + " '" + comp_file + "'")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setApplicationName("NebiDE")
    app.setOrganizationName("NebiSoft")
    try:
        if sys.argv[1] == "--gui-selector":
            window = WallpaperSelector()
            app.setStyle("Fusion")
            window.show()
            app.exec_()
    except:
        default_wall = default_wall_path + default_wall
        settings = QSettings("NebiSoft", "NebiDE")
        wall_path = settings.value("wallpaper", default_wall)
        wall_method = settings.value("wallpaper_method", "--bg-scale")
        os.system("feh " + wall_method + " '" + wall_path + "'")
