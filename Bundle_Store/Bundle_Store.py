#!/usr/bin/python3
import os
import pwd
import re
import subprocess
from subprocess import check_output, CalledProcessError
import lsb_release
import cairo
import gi
import shutil

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('WebKit2', '4.0')

from gi.repository import WebKit2
from gi.overrides import Gtk
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from gi.repository import Pango

APPS_DIR = "/Applications/"
SUDO_EXEC = "pkexec"
if os.environ.get("BS_INSTALL_SYSTEM_WIDE") == None:
    APPS_DIR = os.path.join(os.path.expanduser("~"), "Applications") + "/"
    SUDO_EXEC = ""

def apt_cache(prop, *args):
    try:
        out = str(check_output(args))
        m = re.search(prop + ":.*", out)
        mgroup = str(m.group()).replace(prop + ": ", "")
        return mgroup if m else ""
    except CalledProcessError as e:
        return e.output

class AppView(Gtk.Box):
    def __init__(self):
        super().__init__()
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/main.glade")
        box = self.builder.get_object("AppView")
        self.listbox:Gtk.Box = self.builder.get_object("AppView_ListBox")
        self.pack_start(box, 0, 1, 1)

        self.detail_currentapp = "nil"

        self.detail_appicon:Gtk.Image = self.builder.get_object("app_icon_details")
        self.detail_appname:Gtk.Label = self.builder.get_object("app_name_details")
        self.detail_apppub:Gtk.Label = self.builder.get_object("app_pub_details")

        self.detail_del_cache_btn:Gtk.Button = self.builder.get_object("detailview_clear_cache")
        self.detail_del_data_btn:Gtk.Button = self.builder.get_object("detailview_wipe_data")
        self.detail_uninstall_btn:Gtk.Button = self.builder.get_object("detailview_uninstall_app")

        self.detail_del_cache_btn.connect("clicked", lambda _: self.cleanAppCache(self.detail_currentapp))
        self.detail_del_data_btn.connect("clicked", lambda _: self.wipeAppData(self.detail_currentapp))
        self.detail_uninstall_btn.connect("clicked", lambda _: self.uninstallApp(self.detail_currentapp))

    def cleanAppCache(self, app_path):
        app = app_path.split("/")[-1]
        appname_with_dashes = app.replace(".napp", "")
        appname = appname_with_dashes.replace("_", " ")
        os.system(f"rm -rf /Applications/.{appname_with_dashes}")
        os.system(f"rm -rf ~/.cache/.Application_Data/{appname_with_dashes}")


    def wipeAppData(self, app_path):
        app = app_path.split("/")[-1]
        appname_with_dashes = app.replace(".napp", "")
        appname = appname_with_dashes.replace("_", " ")
        os.system(f"rm -rf ~/.cache/.Application_Data/{appname_with_dashes}")
        os.system(f"rm -rf ~/.config/.Application_Data/{appname_with_dashes}")
        os.system(f"rm -rf ~/.local/.Application_Data/{appname_with_dashes}")
        os.system(f"rm -rf ~/.Application_Data/{appname_with_dashes}")

    def uninstallApp(self, app_path):
        app = app_path.split("/")[-1]
        appname_with_dashes = app.replace(".napp", "")
        appname = appname_with_dashes.replace("_", " ")

        dialog = Gtk.Dialog("Uninstall Application", None, 0,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_DELETE, Gtk.ResponseType.DELETE_EVENT))

        label = Gtk.Label(f"Do you want to keep data for {appname}?")
        dialog.vbox.pack_start(label, True, True, 0)
        dialog.vbox.show_all()

        dialog.set_default_size(150, 100)

        response = dialog.run()

        if response == Gtk.ResponseType.DELETE_EVENT:
            # User chose to delete data
            if app_path.startswith("/Applications/"):
                if os.environ.get("BS_INSTALL_SYSTEM_WIDE") == None:
                    error_dialog = Gtk.MessageDialog(
                        None,
                        Gtk.DialogFlags.MODAL,
                        Gtk.MessageType.ERROR,
                        Gtk.ButtonsType.CLOSE,
                        "Error: It's a system app and can't be uninstalled."
                    )
                    error_dialog.run()
                    error_dialog.destroy()
                    self.listApps()
                    dialog.destroy()
                    return
                else:
                    error_dialog = Gtk.MessageDialog(
                        None,
                        Gtk.DialogFlags.MODAL,
                        Gtk.MessageType.INFO,
                        Gtk.ButtonsType.CLOSE,
                        "INFO: It's a system app and can't be uninstalled normal conditions but you've activated the "
                        "System-Wide modification so this means you are accepted the risk. Continuing..."
                    )
                    error_dialog.run()
                    error_dialog.destroy()
            os.system(f"rm -rf {app_path}")
            self.cleanAppCache(app_path)
            self.wipeAppData(app_path)
            self.listApps()
        elif response == Gtk.ResponseType.CANCEL:
            # User chose to keep data
            pass

        dialog.destroy()

    def listApps(self):
        for app in self.listbox.get_children():
            self.listbox.remove(app)

        MyApps = Gtk.Label()
        MyApps.set_xalign(0.0)
        MyApps.set_markup("<b>My Applications</b>")
        self.listbox.add(MyApps)

        for my_app in os.listdir(os.environ["HOME"] + "/Applications"):
            if my_app.endswith(".napp"):
                self.listbox.add(self.makeAppBtn(my_app, "user"))

        SysApps = Gtk.Label()
        SysApps.set_xalign(0.0)
        SysApps.set_markup("<b>System Applications</b>")
        self.listbox.add(SysApps)

        for my_app in os.listdir("/Applications"):
            if my_app.endswith(".napp"):
                self.listbox.add(self.makeAppBtn(my_app, "system"))

        self.show_all()

    def makeAppBtn(self, app, install_type):
        if install_type == "user":
            InstallPrefix = os.environ["HOME"] + "/Applications"
        else:
            InstallPrefix = "/Applications"
        appname = app.replace(".napp", "").replace("_", " ")
        appbox:Gtk.Box = Gtk.HBox()
        appbox.set_spacing(3)
        appbox.set_size_request(0, 32)
        app_icon = GdkPixbuf.Pixbuf.new_from_file_at_scale(f"{InstallPrefix}/.{app}.png", 24, 24, True)
        app_label = Gtk.Label()
        app_label.set_xalign(0.0)
        app_label.set_ellipsize(Pango.EllipsizeMode.END)
        app_label.set_markup(f"<b>{appname}</b>")
        appbox.pack_start(Gtk.Image.new_from_pixbuf(app_icon), 0, 0, 0)
        appbox.add(app_label)
        btn = Gtk.Button()
        btn.connect("clicked", lambda _: self.showData(app, install_type))
        btn.add(appbox)
        return btn

    def showData(self, app, install_type):
        if install_type == "user":
            InstallPrefix = os.environ["HOME"] + "/Applications"
        else:
            InstallPrefix = "/Applications"
        app_icon = GdkPixbuf.Pixbuf.new_from_file_at_scale(f"{InstallPrefix}/.{app}.png", 48, 48, True)
        appname = app.replace(".napp", "").replace("_", " ")
        apppub = "Unknown"
        lsm_b:bytes = subprocess.check_output([InstallPrefix + "/" + app, "--lsm"])
        lsm = lsm_b.decode()
        for line in lsm.splitlines():
            if line.startswith("Author="):
                apppub = line.replace("Author=", "")[1:-1]
        self.detail_appicon.set_from_pixbuf(app_icon)
        self.detail_appname.set_text(appname)
        self.detail_apppub.set_text(f"by {apppub}")
        self.detail_currentapp = f"{InstallPrefix}/{app}"

class InstallLegacyApp(Gtk.Window):
    def __init__(self, appPath):
        Gtk.Window.__init__(self)

        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/main.glade")
        window = self.builder.get_object("legacyInstall")
        window.set_title("Install Legacy Application")

        pkgName = apt_cache("Package", "dpkg", "--info", appPath).split("\\n")[0] + " - " + \
                  str(apt_cache("Description", "dpkg", "--info", appPath)).split("\\n")[0]
        pkgPublisher = apt_cache("Maintainer", "dpkg", "--info", appPath).split("\\n")[0]
        pkgDescList = apt_cache("Description", "dpkg", "--info", appPath).replace("\\n", "\n")[:-1].replace("  .",
                                                                                                            "").replace(
            "  ", "").split("\n")[1:]
        for i in pkgDescList:
            try:
                pkgDesc = pkgDesc + "\n" + i
            except:
                pkgDesc = i

        print(pkgName)
        print(pkgPublisher)
        print(pkgDesc)

        self.builder.get_object("legacyAppName").set_text(pkgName)
        self.builder.get_object("legacyAppPublisher").set_text(pkgPublisher)
        self.builder.get_object("legacyAppDesc").set_text(pkgDesc)

        self.pkgPath = str(appPath)
        self.dlg = window
        self.builder.get_object("installLegacy").connect("clicked",
                                                         lambda _: self.builder.get_object("installLegacy").hide())
        self.builder.get_object("installLegacy").connect("clicked", self.installpkg)

        window.show_all()

    def installpkg(self, arg1):
        subprocess.call(['pkexec', "apt", "install", "-y", self.pkgPath])
        self.dlg.destroy()


class BundleWeb(WebKit2.WebView):
    def downloadRequested(self, download):
        download.connect("received-data", self.whenDownloading)
        download.connect("created-destination", self.setPath)
        download.connect("finished", self.moveToPath)
        download.connect("failed", self.error)

    def moveToPath(self, download):
        mw.header.set_subtitle("Installed " + self.appname + " successfully!")
        dwnld_bar: Gtk.ProgressBar = mw.builder.get_object("DownloadBar")
        dwnld_bar.set_visible(False)
        
        # Extracting file extension
        file_extension = download.get_request().get_uri().split(".")[-1]
    
        # Check file extension
        if file_extension == "napp":
            shutil.move(self.oldDest, self.dest)
            os.system(
                "wget https://bundlestoreclient.nebisoftware.com/app-database." + ARCH + "/" + self.appname.replace(
                    " ", "_") + f".png -O {APPS_DIR}." + self.appname.replace(
                    " ", "_") + ".napp.png")
            os.system("chmod +x " + self.dest)
            os.system(f"{SUDO_EXEC} update-napp-launchers --skip-check")
        elif file_extension == "nscr":
            # Create the script directory in HOME/Downloads if not exists
            script_directory = os.path.join(os.path.expanduser("~"), "Downloads", self.appname.replace(" ", "_"))
            os.makedirs(script_directory, exist_ok=True)
    
            # Move the script to the appropriate directory
            script_destination = os.path.join(script_directory, self.appname.replace(" ", "_") + ".nscr")
            shutil.move(self.oldDest, script_destination)
    
            # Change script permissions
            os.system("chmod +x " + script_destination)
    
            # Execute the script
            os.system("/Applications/NSCR_Intepreter.napp -- \"" + script_destination + "\"")
    
        # Handle other file types as needed
        else:
            print("Unsupported file type: {}".format(file_extension))

    def error(self, download, code):
        # TODO: Move To path
        try:
            mw.header.set_subtitle("Error while installing " + self.appname + ": " + code)
        except:
            pass

    def setPath(self, download, path):
        filename = str(path).split("/")[-1]
        self.oldDest = str(path).replace("file://", "")
        print(self.oldDest)
        self.dest = APPS_DIR + filename
        print(self.dest)

    def whenDownloading(self, download:WebKit2.Download, length):
        percentProg = download.props.estimated_progress * 100
        percentProgInt = int(percentProg)
        dwUri = download.get_request().get_uri()
        appnameList = str(dwUri).split("/")[-1].split(".")[:-1]
        appname = ''.join(str("." + e) for e in appnameList)
        self.appname = appname[1:]
        self.appname = self.appname.replace("_", " ")
        status = "Downloading " + self.appname
        if download.props.estimated_progress == 0.0:
            status = status + " (Can't show progress)"
        else:
            status = status + " %" + str(percentProgInt)
        dwnld_bar:Gtk.ProgressBar = mw.builder.get_object("DownloadBar")
        dwnld_bar.set_visible(True)
        dwnld_bar.set_fraction(download.props.estimated_progress)
        mw.header.set_subtitle(status)


class BundleStore(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)

        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/main.glade")

        window = self.builder.get_object("main-window")
        window.set_title("Bundle Store")

        self.header: Gtk.HeaderBar = self.builder.get_object("header")
        dwnld_bar:Gtk.ProgressBar = self.builder.get_object("DownloadBar")
        dwnld_bar.set_visible(False)

        window.connect('destroy', self.quit)
        window.connect('draw', self.draw)

        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            window.set_visual(visual)

        window.set_app_paintable(True)

        username = self.builder.get_object("sidebar-username")
        upwd = pwd.getpwuid(os.getuid())[4]
        if upwd.endswith(",,,"):
            username.set_text(upwd.replace(",,,", "'s Apps"))
        else:
            username.set_text(upwd)

        self.view = BundleWeb()
        print(lsb_release.get_os_release()["RELEASE"])
        uasettings = self.view.get_settings()
        cpu_arch = ARCH
        user_agent = f'Bundle Store - NebiOS {lsb_release.get_os_release()["RELEASE"]} ({cpu_arch})'
        uasettings.set_user_agent(user_agent)
        self.view.set_settings(uasettings)
        self.view.connect("load_changed", self.load)
        self.view.connect("decide_policy", self.policy_decision_requested)

        self.scrollView = self.builder.get_object("webkit-scroller")
        self.appView = self.builder.get_object("app-scroller")
        self.scrollView.add(self.view)
        self.view.show()

        self.AppView = AppView()
        self.appView.add(self.AppView)

        # Download Test
        # self.view.load_uri("https://cdn.discordapp.com/attachments/939627120135643227/947119847941238824/Discord.napp")

        # The Store
        self.view.load_uri("https://bundlestoreclient.nebisoftware.com")

        window.show()
        self.builder.get_object("installLegacyBtn").connect("clicked", self.InstallLegacy)
        self.builder.get_object("refreshNappLaunchers").connect("clicked",
                                                                lambda _: os.system(f"{SUDO_EXEC} update-napp-launchers"))
        self.builder.get_object("about-btn").connect("clicked", self.show_about)

        self.builder.get_object("sidebar-home").connect("clicked", lambda _: self.view.load_uri(
            "https://bundlestoreclient.nebisoftware.com"))
        self.builder.get_object("sidebar-updates").connect("clicked", self.updates)

        self.builder.get_object("storeSearchBox").connect("search-changed", self.search)

        css_provider_search = Gtk.CssProvider()
        css_provider_search.load_from_data("""
        .s_headerbar {
            background: transparent;
        }
        """.encode())
        search_context = self.builder.get_object("search_header").get_style_context()
        search_context.add_provider(css_provider_search,
                                    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        css_provider_box = Gtk.CssProvider()
        css_provider_box.load_from_data("""
        .DownloadBar {
            background: white;
        }
        """.encode())
        box_context = self.builder.get_object("DownloadBar").get_style_context()
        box_context.add_provider(css_provider_box,
                                    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        side_my_apps = self.builder.get_object("appinfo-btn")
        side_my_apps.connect("clicked", lambda _: (self.scrollView.hide(), self.appView.show(), self.AppView.listApps()))

    def updates(self, btnarg):
        appslist = []
        for i in os.listdir(APPS_DIR):
            if i.endswith(".napp"):
                appslist.append(i)

        appVerList = ""
        for app in appslist:
            result = subprocess.run([APPS_DIR + app, '--lsm'], stdout=subprocess.PIPE)
            for line in result.stdout.decode('utf-8').split("\n"):
                if line.startswith(" Name="):
                    appId = line.replace(" Name=", "").replace(" ", "_")
                elif line.startswith("Version="):
                    appVer = line.replace("Version=", "").replace('"', "")

            appVerList += appId + "|" + appVer + ";"

        jslist = appVerList
        for liste in jslist.split(";"):
            print(liste)

        self.view.load_uri("https://bundlestoreclient.nebisoftware.com/updates.html?list=" + jslist)

    def search(self, arg1):
        self.view.load_uri("https://bundlestoreclient.nebisoftware.com/search.html?search=" + arg1.get_text())

    def load(self, view, event):
        if event == 3:
            print("Finished")
            self.scrollView.show()
            self.appView.hide()
            print(self.view.get_settings().get_user_agent())
            self.builder.get_object("loadingSpin").stop()
            self.builder.get_object("loadingSpin").hide()
            if str(view.get_uri()).__contains__(".napp"):
                view.go_back()
        elif event == 2:
            if str(view.get_uri()).__contains__(".nscr"):
                view.go_back()
        elif event == 0:
            print("Started")
            self.builder.get_object("loadingSpin").start()
            self.builder.get_object("loadingSpin").show()

    def policy_decision_requested(self, view, policy_decision, mimetype):
        try:
            uri = str(policy_decision.get_navigation_action().get_request().get_uri()).split("?")[0]
            print(uri.endswith(".html"))
            if uri.__contains__(".napp") or uri.__contains__(".nscr"):
                self.view.downloadRequested(
                    self.view.download_uri(str(policy_decision.get_navigation_action().get_request().get_uri())))
        except:
            pass

    def quit(self, id):
        os.system("rm $HOME/Downloads/*.{napp,nscr}.wkdownload")
        Gtk.main_quit()

    def show_about(self, arg1):
        self.builder.add_from_file("ui/main.glade")
        about = self.builder.get_object("aboutWin")
        about.show_all()

    def onResize(self, win, event):
        self.builder.get_object("headerbox").set_size_request(win.get_size().width - 257, -1)

    def InstallLegacy(self, arg1):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a *.deb file", parent=self, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print("Open clicked")
            filepath = dialog.get_filename()
            print("File selected: " + filepath)
            InstallLegacyApp(filepath)
        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()
            print("Cancel clicked")

        dialog.destroy()

    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Debian Package")
        filter_text.add_pattern("*.deb")
        dialog.add_filter(filter_text)

    def draw(self, widget, context):
        style = self.get_style_context()
        bgcolor = style.get_background_color(Gtk.StateType.NORMAL)
        context.set_source_rgba(bgcolor.red, bgcolor.green, bgcolor.blue, 0.9)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)


if os.environ.__contains__("BS_ARCH") == True:
    ARCH = os.environ["BS_ARCH"]
else:
    ARCH = subprocess.check_output(["uname", "-m"]).decode().strip()

mw = BundleStore()
Gtk.main()
