from cx_Freeze import setup, Executable

setup(name="GUI Test",
      description="GUITest",
      version="0.1",
      options={"build_exe": {"build_exe": "Bin/pygobject",
                             "packages": ["gi"],
                             }},
      executables=[Executable(script="Bundle_Store.py",
                              targetName="app.exec",
                              )]
      )
