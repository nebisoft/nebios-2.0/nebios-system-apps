import gi
import random
import string

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib
import feedparser
from bs4 import BeautifulSoup

def generate_random_suffix():
    return ''.join(random.choices(string.ascii_lowercase, k=6))

class RSSWidget(Gtk.Bin):
    def __init__(self, url, count, upd_int):
        Gtk.Bin.__init__(self)
        self.index = 0
        self.url = url
        self.count = count
        self.update_interval = upd_int

        self.headline_images = []
        self.headline_text = []
        self.headline_date = []
        self.headline_url = []

        self.stack = Gtk.Stack()
        self.stack.set_transition_type(Gtk.StackTransitionType.UNDER_LEFT)
        self.add(self.stack)

        self.start_gen()

    def start_gen(self):
        self.initial_content()
        self.timer_id = GLib.timeout_add_seconds(2, lambda: (self.get_content(), self.add_contents()))

    def remove_contents(self):
        for page in self.stack.get_children():
            self.stack.remove(page)

    def update_page(self):
        self.index = (self.index + 1) % len(self.stack.get_children())
        if self.index == 0:
            self.remove_contents()
            self.start_gen()
        else:
            self.stack.set_visible_child(self.stack.get_children()[self.index])
            self.timer_id = GLib.timeout_add_seconds(self.update_interval, lambda: self.update_page())

    def add_contents(self):
        print("Adding Data")
        widget_css_provider_data = ""
        random_suffix = generate_random_suffix()
        for i in range(len(self.headline_text)):
            image = self.headline_images[i]
            class_suffix = random_suffix + str(i)
            widget_css_provider_data += f"""
                                    .bg-{class_suffix} {{
                                        background-image: url('{image}');
                                        background-size: cover;
                                        background-position: center;
                                        border-radius: 16px;
                                    }}
                                    
                                    .bg-mbox-{class_suffix} {{
                                       border-radius: 16px;
                                       background-image: -gtk-gradient (linear,
                                       left top,
                                       left bottom,
                                       from (rgba(0, 0, 0, 1)),
                                       to (rgba(0, 0, 0, 0)));
                                       padding: 10px;
                                    }}
                                    
                                    GtkLabel-{class_suffix} {{
                                        color: white;
                                        text-shadow: none;
                                    }}
                                    
                                    .date-{class_suffix} {{
                                        color: rgba(255, 255, 255, 0.5);
                                        font-weight: 500;
                                        font-size: 16px;
                                    }}
                                    
                                    .label-{class_suffix} {{
                                        font-weight: 800;
                                        font-size: 40px;
                                        text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
                                    }}
                                    
                                    .label-{class_suffix}:hover {{
                                        text-decoration: underline;
                                    }}
                                """

        widget_css_provider = Gtk.CssProvider()
        widget_css_provider.load_from_data(widget_css_provider_data.encode())
        widget_context = self.stack.get_style_context()
        widget_context.add_provider_for_screen(Gdk.Screen.get_default(), widget_css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        for i in range(len(self.headline_text)):
            eventbox = Gtk.EventBox()
            class_suffix = random_suffix + str(i)
            eventbox.get_style_context().add_class(f"bg-{class_suffix}")
            vbox = Gtk.VBox()
            date_lbl_txt = self.headline_url[i].split("/")[2]
            if self.headline_date[i] != " ":
                date_lbl_txt += "\n" + self.headline_date[i]
            date = Gtk.Label(date_lbl_txt)
            label = Gtk.Label()
            headline_text = str(self.headline_text[i])
            if len(headline_text) > 40:
                headline_text = headline_text[0:40] + "..."
            if len(headline_text) == 0:
                headline_text = "<i>Untitled</i>"
            label.set_markup(f"<span foreground='white' underline='none'><a href=\"" + self.headline_url[i] + "\">" + headline_text + "</a></span>")
            label.set_line_wrap(Gtk.WrapMode.WORD)
            label.set_alignment(0, 0)
            label.set_justify(Gtk.Justification.LEFT)
            date.set_line_wrap(Gtk.WrapMode.WORD)
            date.set_alignment(0, 0)
            date.set_justify(Gtk.Justification.LEFT)
            vbox.pack_end(label, False, False, 1)
            vbox.pack_end(date, False, False, 0)
            mbox = Gtk.VBox()
            mbox.pack_start(vbox, False, False, 0)
            vbox.set_margin_left(10)
            vbox.set_margin_top(8)
            eventbox.add(mbox)

            label.get_style_context().add_class(f"label-{class_suffix}")
            date.get_style_context().add_class(f"date-{class_suffix}")
            mbox.get_style_context().add_class(f"bg-mbox-{class_suffix}")

            self.stack.add_named(eventbox, str(i))

        self.stack.show_all()
        self.timer_id = GLib.timeout_add_seconds(1, lambda: self.update_page())

    def initial_content(self):
        title_text = Gtk.Label("RSS Feed")
        loading_text = Gtk.Label("Fetching data...")
        title_text.set_line_wrap(Gtk.WrapMode.WORD)
        title_text.set_alignment(0, 0)
        title_text.set_justify(Gtk.Justification.LEFT)
        title_text.get_style_context().add_class("title")
        loading_text.set_line_wrap(Gtk.WrapMode.WORD)
        loading_text.set_alignment(0, 0)
        loading_text.set_justify(Gtk.Justification.LEFT)
        loading_text.get_style_context().add_class("loadtxt")

        vbox = Gtk.VBox()
        vbox.pack_start(title_text, True, True, 0)
        vbox.pack_start(loading_text, True, True, 0)

        widget_css_provider = Gtk.CssProvider()
        widget_css_provider.load_from_data(f"""
                                            .title {{
                                                font-weight: 500;
                                                font-size: 16px;
                                            }}

                                            .loadtxt {{
                                                font-weight: 800;
                                                font-size: 40px;
                                            }}
                """.encode())
        widget_context = vbox.get_style_context()
        widget_context.add_provider_for_screen(Gdk.Screen.get_default(), widget_css_provider,
                                               Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        self.stack.add_named(vbox, str("init"))

    def get_content(self):
        print("Fetching Data")
        feed = feedparser.parse(self.url)

        for entry in feed.entries:
            if 'description' in entry:
                content = entry.description
                soup = BeautifulSoup(content, 'html.parser')
                img_child = soup.findChild('img')

                if img_child is not None:
                    img_tag = img_child.get_attribute_list("src")[0]
                    if img_tag.endswith(".png") \
                    or img_tag.endswith(".jpg") \
                    or img_tag.endswith(".jpeg"):
                        if len(self.headline_images) < self.count:
                            if 'title' in entry:
                                self.headline_text.append(entry.title)
                            if 'link' in entry:
                                self.headline_url.append(entry.link)
                            if 'published' in entry:
                                self.headline_date.append(":".join(entry.published.rsplit(":", 1)[:1]))
                            else:
                                self.headline_date.append(" ")
                            self.headline_images.append(img_tag)

class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self, url, count=5, update_interval=60):
        Gtk.VBox.__init__(self)
        self.min_w = 360
        self.min_h = 144
        self.add(RSSWidget(url, count, update_interval))

    def resizeWidget(self, w, h):
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)
