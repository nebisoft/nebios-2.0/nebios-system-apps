import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Pango

class Notepad(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL, spacing=6)

        self.textview = Gtk.TextView()
        self.textview.set_wrap_mode(Gtk.WrapMode.WORD)
        self.set_border_width(16)

        self.pack_start(self.textview, True, True, 0)


class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self):
        Gtk.VBox.__init__(self)
        self.min_w = 256
        self.min_h = 256
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 1))
        self.add(Notepad())

    def resizeWidget(self, w, h):
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)
