import os
import configparser
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf, Gio, Pango, GLib

class DraggableIcon(Gtk.EventBox):
	def __init__(self, label, pixbuf, icon_name):
		super().__init__()
		self.icon_name = icon_name

		# Create an image and label
		icon_image = Gtk.Image.new_from_pixbuf(pixbuf)
		icon_label = Gtk.Label(label)
		icon_label.set_ellipsize(Pango.EllipsizeMode.END)

		# Create a box to hold the image and label
		box = Gtk.VBox()
		box.pack_start(icon_image, False, False, 0)
		box.pack_start(icon_label, False, False, 0)
		self.add(box)

		# Add a CSS class to style the icon
		self.get_style_context().add_class("draggable-icon")
		box.get_style_context().add_class("draggable-icon-box")

		# Enable drag-and-drop
		self.connect("button-press-event", self.on_button_press_event)
		self.connect("button-release-event", self.on_button_release_event)
		self.connect("motion-notify-event", self.on_motion_notify_event)
		self.connect("enter-notify-event", self.on_mouse_enter)
		self.connect("leave-notify-event", self.on_mouse_leave)
		self.dragging = False
		self.drag_start_x = 0
		self.drag_start_y = 0
		self.drag_threshold = 12  # Drag sensitivity threshold (in pixels)

		self.clipboard = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

	def on_mouse_enter(self, widget, event):
		# Add a hover style class
		self.get_style_context().add_class("hover")

	def on_mouse_leave(self, widget, event):
		# Remove the hover style class
		self.get_style_context().remove_class("hover")

	def on_button_press_event(self, widget, event):
		if event.type == Gdk.EventType._2BUTTON_PRESS and event.button == 1:
			# Double-click event, open the file
			self.open_file()
		elif event.button == 1:
			# Handle dragging
			self.dragging = True
			self.shoulddragging = False
			self.drag_start_x = event.x
			self.drag_start_y = event.y
			self.original_x, self.original_y = widget.translate_coordinates(self.get_parent(), 0, 0)

			# Add a press style class
			self.get_style_context().add_class("pressed")
		elif event.button == 3:
			# Right-click event, show context menu
			self.show_context_menu(event)

	def on_button_release_event(self, widget, event):
		if event.button == 1:
			if self.dragging and self.shoulddragging:
				self.dragging = False
				self.shoulddragging = False
				parent = self.get_parent()
				if parent:
					x, y = widget.translate_coordinates(parent, event.x, event.y)

					# Calculate the grid spacing
					grid_spacing = parent.icon_size + 40  # Adjust based on the icon size and desired spacing

					# Snap to the nearest grid point
					snapped_x = round(x / grid_spacing) * grid_spacing
					snapped_y = round(y / grid_spacing) * grid_spacing

					# Move the icon to the snapped position
					parent.move(widget, int(snapped_x), int(snapped_y))
					parent.save_icon_positions()

				# Remove the press style class
				self.get_style_context().remove_class("pressed")

	def on_motion_notify_event(self, widget, event):
		if self.dragging:
			# Check if dragging threshold has been exceeded
			drag_distance_x = abs(event.x - self.drag_start_x)
			drag_distance_y = abs(event.y - self.drag_start_y)

			if drag_distance_x > self.drag_threshold or drag_distance_y > self.drag_threshold:
				if not self.shoulddragging:
					self.shoulddragging = True

			if self.shoulddragging:
				parent = self.get_parent()
				if parent:
					# Translate widget coordinates to parent coordinates
					x, y = widget.translate_coordinates(parent, event.x, event.y)

					# Apply dragging
					parent.move(widget, int(x - self.drag_start_x), int(y - self.drag_start_y))



	def open_file(self):
		file_path = os.path.join(self.get_parent().desktop_path, self.icon_name)
		
		if file_path.endswith(".napp"):
			command = f"napp-run \"{file_path}\" &"
		elif file_path.endswith(".desktop"):
			# .desktop dosyasını parse et ve Exec komutunu çalıştır
			config = configparser.ConfigParser(interpolation=None)
			config.read(file_path)
			
			exec_command = config.get('Desktop Entry', 'Exec', fallback=None)
			if exec_command:
				# Exec komutunu çalıştır, %f gibi parametreleri dosya yoluyla değiştir
				command = exec_command.replace("%f", f"\"{file_path}\"").replace("%u", f"\"{file_path}\"")
				command = f"{command} &"
			else:
				print(f"Exec komutu bulunamadı: {file_path}")
				return
		else:
			command = f"xdg-open \"{file_path}\" &"
		
		# Komutu çalıştır
		os.system(command)

	def show_context_menu(self, event):
		menu = Gtk.Menu()

		# New Folder
		new_folder_item = Gtk.MenuItem(label="New Folder")
		new_folder_item.connect("activate", self.on_new_folder)
		menu.append(new_folder_item)

		# New File from Template with Submenu
		new_file_item = Gtk.MenuItem(label="New File from Template")
		submenu = Gtk.Menu()
		new_file_item.set_submenu(submenu)

		parent = self.get_parent()
		template_path = os.path.expanduser("~/Templates")
		if os.path.exists(template_path):
			for file_name in os.listdir(template_path):
				if not file_name.startswith('.'):
					template_item = Gtk.MenuItem(label=file_name)
					template_item.connect("activate", self.on_new_file_from_template, file_name)
					submenu.append(template_item)

		menu.append(new_file_item)

		# Rename
		rename_item = Gtk.MenuItem(label="Rename")
		rename_item.connect("activate", self.on_rename)
		menu.append(rename_item)

		# Delete
		delete_item = Gtk.MenuItem(label="Delete")
		delete_item.connect("activate", self.on_delete)
		menu.append(delete_item)

		menu.show_all()
		menu.popup(None, None, None, None, event.button, event.time)

	def on_new_folder(self, widget):
		parent = self.get_parent()
		dialog = Gtk.MessageDialog(
			transient_for=parent.get_toplevel(),
			flags=0,
			message_type=Gtk.MessageType.QUESTION,
			buttons=Gtk.ButtonsType.OK_CANCEL,
			text="Enter the name of the new folder:",
		)
		dialog.set_default_response(Gtk.ResponseType.OK)

		entry = Gtk.Entry()
		entry.set_text("New Folder")
		entry.show()
		dialog.vbox.pack_end(entry, True, True, 0)

		response = dialog.run()
		folder_name = entry.get_text()
		dialog.destroy()

		if response == Gtk.ResponseType.OK and folder_name:
			new_folder_path = os.path.join(parent.desktop_path, folder_name)
			os.makedirs(new_folder_path, exist_ok=True)

	def on_new_file_from_template(self, widget, file_name):
		parent = self.get_parent()
		template_path = os.path.expanduser("~/Templates")
		source = os.path.join(template_path, file_name)
		destination = os.path.join(parent.desktop_path, file_name)
		with open(source, 'r') as fsrc, open(destination, 'w') as fdst:
			fdst.write(fsrc.read())

	def on_rename(self, widget):
		parent = self.get_parent()
		dialog = Gtk.MessageDialog(
			transient_for=parent.get_toplevel(),
			flags=0,
			message_type=Gtk.MessageType.QUESTION,
			buttons=Gtk.ButtonsType.OK_CANCEL,
			text="Enter the new name:",
		)
		dialog.set_default_response(Gtk.ResponseType.OK)

		entry = Gtk.Entry()
		entry.set_text(self.icon_name)
		entry.show()
		dialog.vbox.pack_end(entry, True, True, 0)

		response = dialog.run()
		new_name = entry.get_text()
		dialog.destroy()

		if response == Gtk.ResponseType.OK and new_name:
			old_path = os.path.join(parent.desktop_path, self.icon_name)
			new_path = os.path.join(parent.desktop_path, new_name)
			os.rename(old_path, new_path)

	def on_delete(self, widget):
		parent = self.get_parent()
		dialog = Gtk.MessageDialog(
			transient_for=parent.get_toplevel(),
			flags=0,
			message_type=Gtk.MessageType.WARNING,
			buttons=Gtk.ButtonsType.OK_CANCEL,
			text=f"Are you sure you want to delete '{self.icon_name}'?",
		)
		response = dialog.run()
		dialog.destroy()

		if response == Gtk.ResponseType.OK:
			file_path = os.path.join(parent.desktop_path, self.icon_name)
			if self.icon_name.endswith('.napp'):
				delete_icon = f"rm -r '{parent.desktop_path}/.{self.icon_name}.png'"
				delete_legacy = f"rm -r '~/.local/share/applications/{self.icon_name}.desktop'"
				os.system(delete_icon)
				os.system(delete_legacy)
				
			os.remove(file_path)
			parent.load_icons()


# Add CSS provider
css_provider = Gtk.CssProvider()
css_provider.load_from_data(b"""
	.draggable-icon {
		background-color: transparent;
		transition-duration: 250ms;
	}
	.draggable-icon-box {	
		margin: 8px 12px;
	}  
	.draggable-icon-box label {	
		text-shadow: 0px 1px 4px black;
	}
	.draggable-icon.hover {
		background-color: rgba(255, 255, 255, 0.1);
		border-radius: 12px;
	}
	.draggable-icon.pressed {
		background-color: rgba(255, 255, 255, 0.2);
		border-radius: 12px;
	}
""")

# Add the CSS provider to the screen's default style context
screen = Gdk.Screen.get_default()
style_context = Gtk.StyleContext()
style_context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

class DesktopGrid(Gtk.Fixed):
	def __init__(self, location, icon_size):
		super().__init__()
		self.icon_size = icon_size
		self.desktop_path = os.path.expanduser(location)
		self.icon_positions = {}
		self.icons = {}  # Ensure this is a dictionary of Gtk.Widget instances
		self.file_names = set()
		self.load_icon_positions()
		self.load_icons()

		# Monitor directory changes
		self.monitor = Gio.File.new_for_path(self.desktop_path).monitor_directory(Gio.FileMonitorFlags.NONE, None)
		self.monitor.connect("changed", self.on_directory_changed)

	def on_directory_changed(self, monitor, file, other_file, event_type):
		if event_type in [Gio.FileMonitorEvent.CREATED, Gio.FileMonitorEvent.DELETED]:
			self.update_icons(event_type, file.get_basename())

	def update_icons(self, event_type, file_name):
		# Dosya modifiye edildiyse hiçbir şey yapma
		if event_type == Gio.FileMonitorEvent.CHANGES_DONE_HINT:
			self.load_icons()
			return
		
		# Aynı dosyadan iki tane olamaz
		if file_name in self.icons:
			self.load_icons()
			return

		# Event türüne göre işlem yap
		if event_type == Gio.FileMonitorEvent.CREATED:
			if not file_name.startswith('.'):
				self.load_icons()
		elif event_type == Gio.FileMonitorEvent.DELETED:
			if file_name in self.icons:
				self.remove(self.icons[file_name])
				del self.icons[file_name]
				self.save_icon_positions()
				self.load_icons()


	def load_icons(self):
		# Clear existing icons
		for icon in list(self.icons.values()):
			self.remove(icon)
		self.icons.clear()

		# Load new icons
		files = os.listdir(self.desktop_path)
		self.grid_spacing = self.icon_size + 40  # Adjust grid spacing

		for i, item in enumerate(files):
			if not item.startswith('.'):  # Skip dotfiles
				self.load_icon(item)

	def load_icon(self, item):
		display_name_item = item
		item_path = os.path.join(self.desktop_path, item)
		file = Gio.File.new_for_path(item_path)

		# Özel ikon kontrolü
		file_icon = file.query_info('metadata::custom-icon', 0, None).get_attribute_as_string('metadata::custom-icon')

		if file_icon:
			pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(file_icon.replace("file://", "", 1), self.icon_size, self.icon_size, True)
		else:
			# (current directory)/.(file_name).png kontrolü
			hidden_icon_path = os.path.join(self.desktop_path, f".{item}.png")
			if os.path.exists(hidden_icon_path) and item.endswith(".napp"):
				pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(hidden_icon_path, self.icon_size, self.icon_size, True)
			elif item.endswith(".desktop"):
				# .desktop dosyasını parse et ve ilgili simgeyi ve ismi ayarla
				config = configparser.ConfigParser(interpolation=None)
				config.read(item_path)

				# .desktop dosyasında "Icon" ve "Name" bilgilerini al
				icon_name = config.get('Desktop Entry', 'Icon', fallback='empty-file')
				display_name_item = config.get('Desktop Entry', 'Name', fallback=item)

				try:
					# İkonu yükle
					pixbuf = Gtk.IconTheme.get_default().load_icon(icon_name, self.icon_size, 0)
				except GLib.Error:
					# Eğer ikon bulunamazsa, boş dosya simgesini kullan
					pixbuf = Gtk.IconTheme.get_default().load_icon("empty-file", self.icon_size, 0)
			else:
				# İkon isimlerini döngüle ve bir simge bulunana kadar devam et
				icon_names = file.query_info('standard::icon', 0, None).get_icon().get_names()
				pixbuf = None
				for icon_name in icon_names:
					try:
						pixbuf = Gtk.IconTheme.get_default().load_icon(icon_name, self.icon_size, 0)
						break  # Simge bulundu, döngüden çık
					except GLib.Error:
						continue  # Simge bulunamadı, bir sonraki simgeye geç

				# Eğer hiçbir simge bulunamazsa, boş dosya simgesini yükle
				if pixbuf is None:
					pixbuf = Gtk.IconTheme.get_default().load_icon("empty-file", self.icon_size, 0)

		# Dosya ismini gösterime uygun hale getir
		if item.endswith(".napp"):
			display_name_item = item[:-5].replace("_", " ")

		# Sürüklenebilir simgeyi oluştur
		icon = DraggableIcon(display_name_item, pixbuf, item)
		self.icons[item] = icon  # Simgeyi dictionary'e kaydet

		# Konum hesaplama
		pos_x, pos_y = self.icon_positions.get(item, (len(self.icons) // 10 * self.grid_spacing, len(self.icons) % 10 * self.grid_spacing))
		self.put(icon, pos_x, pos_y)

		# Dosya setini güncelle
		self.file_names.add(item)

		self.show_all()


	def save_icon_positions(self):
		conf_file_path = os.path.join(self.desktop_path, ".nebide_we_iconpos.conf")
		with open(conf_file_path, "w") as f:
			for item, icon in self.icons.items():  # Iterate over dictionary items
				x, y = self.get_widget_position(icon)
				f.write(f"{item}:{x}:{y}\n")

	def get_widget_position(self, widget):
		if isinstance(widget, Gtk.Widget):  # Ensure widget is a Gtk.Widget
			# Get the current position of the widget within its parent
			x, y = widget.translate_coordinates(self, 0, 0)
			return x, y
		return 0, 0
		
	def load_icon_positions(self):
		conf_file_path = os.path.join(self.desktop_path, ".nebide_we_iconpos.conf")
		if os.path.exists(conf_file_path):
			with open(conf_file_path, "r") as f:
				for line in f:
					item_name, x, y = line.strip().split(":")
					self.icon_positions[item_name] = (int(x), int(y))
					

class DesktopViewer(Gtk.Box):
	def __init__(self, location, icon_size):
		super().__init__()

		# Create a scrolled window
		scrolled_window = Gtk.ScrolledWindow()
		scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		# Create the desktop grid layout
		self.desktop_grid = DesktopGrid(location, int(icon_size))

		# Add the desktop grid layout to the scrolled window
		scrolled_window.add(self.desktop_grid)

		# Pack the scrolled window into the box
		self.pack_start(scrolled_window, True, True, 0)


class WidgetEngine_MainCall(Gtk.VBox):
	def __init__(self, location="~/Applications", icon_size=64, background=1):
		super().__init__()

		self.min_w = 250
		self.min_h = 250

		if int(background) == 1:
			self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0.25))
		else:
			self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0.00001))
			GLib.timeout_add(1000, lambda: self.get_style_context().remove_class("WidgetEngine_ChildContainer"))

		self.pack_start(DesktopViewer(location, icon_size), True, True, 0)

	def resizeWidget(self, w, h):
		fw = max(w, self.min_w)
		fh = max(h, self.min_h)
		self.set_size_request(fw, fh)


if __name__ == "__main__":
	widgetwin = Gtk.Window()
	widgetwin.add(WidgetEngine_MainCall())
	widgetwin.show_all()
	Gtk.main()

