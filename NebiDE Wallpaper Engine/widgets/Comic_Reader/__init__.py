import importlib
import os, sys
import threading
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, GLib, Gdk, Pango

PREFIX="widgets/Comic_Reader/"
MODPREFIX="widgets.Comic_Reader."

class ComicReader(Gtk.Bin):
    def __init__(self, api_module_name):
        Gtk.Window.__init__(self)
        self.set_border_width(10)
        
        self.downloaded_image = ""
        self.apply_css(PREFIX+"style.css")

        # Load the API module dynamically
        threading.Thread(target=self.add_comic_api, args=(api_module_name,)).start()

        # Create UI components
        self.comic_image = Gtk.Image()
        self.comic_title = Gtk.Label()
        self.comic_title.set_ellipsize(Pango.EllipsizeMode.END)
        self.navigation_box = Gtk.Box(spacing=10)

        # Scrolled window for the comic image
        self.scrolled_window = Gtk.ScrolledWindow()
        self.scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        self.scrolled_window.add(self.comic_image)

        # Navigation buttons
        self.prev_button = Gtk.Button(label="Previous")
        self.prev_button.connect("clicked", self.on_prev_button_clicked)
        self.next_button = Gtk.Button(label="Next")
        self.next_button.connect("clicked", self.on_next_button_clicked)
        self.random_button = Gtk.Button(label="Random")
        self.random_button.connect("clicked", self.on_random_button_clicked)

        self.navigation_box.pack_start(self.prev_button, True, True, 0)
        self.navigation_box.pack_start(self.next_button, True, True, 0)
        self.navigation_box.pack_start(self.random_button, True, True, 0)

        # Layout
        self.grid = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.grid.get_style_context().add_class("comic_win")
        self.grid.pack_start(self.comic_title, False, False, 0)
        self.grid.pack_start(self.scrolled_window, True, True, 0)
        self.grid.pack_end(self.navigation_box, False, False, 0)

        self.add(self.grid)

        # Connect the size-allocate event to dynamically resize the image
        self.connect("size-allocate", self.on_size_allocate)
        
    def add_comic_api(self, api_module_name):
        self.comic_module = importlib.import_module(MODPREFIX + f"sources.{api_module_name}")
        self.comic_source = self.comic_module.ComicSource()
        self.load_comic(self.comic_source.fetch_latest_comic())
        
    def apply_css(self, css_file):
        css_provider = Gtk.CssProvider()
        css_provider.load_from_path(css_file)
        screen = Gdk.Screen.get_default()
        style_context = Gtk.StyleContext()
        style_context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def load_comic(self, comic_info):
        self.comic_title.set_text(comic_info['name'])
        self.current_comic = comic_info

        # Start a thread to download the image
        threading.Thread(target=self.download_and_display_image, args=(comic_info['image'],)).start()
        self.scale_and_set_image(self.current_comic['image'], False)

    def download_and_display_image(self, image_url):
        self.downloaded_image = self.download_image(image_url)

        # Use GLib.idle_add to update the UI from the GTK main thread
        GLib.idle_add(self.scale_and_set_image, image_url)

    def scale_and_set_image(self, image_url, should_show=True):
        if os.path.exists(self.downloaded_image):
            image_path = self.downloaded_image
            allocation = self.get_allocation()
            scaled_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(image_path, allocation.width - 20 - ((8+2) * 2), allocation.height - 100 - ((8+2) * 2), True)
            self.comic_image.set_from_pixbuf(scaled_pixbuf)

    def on_size_allocate(self, widget, allocation):
        if hasattr(self, 'current_comic'):
            self.scale_and_set_image(self.current_comic['image'])

    def on_prev_button_clicked(self, widget):
        prev_comic = self.comic_source.fetch_comic_by_name(self.current_comic['prev'])
        self.load_comic(prev_comic)

    def on_next_button_clicked(self, widget):
        next_comic = self.comic_source.fetch_comic_by_name(self.current_comic['next'])
        self.load_comic(next_comic)

    def on_random_button_clicked(self, widget):
        random_comic = self.comic_source.get_random_comic(self.current_comic["name"])
        self.load_comic(random_comic)

    def download_image(self, image_url):
        from PIL import Image
        import requests
        from os.path import basename, splitext

        response = requests.get(image_url)
        original_image_path = f"/tmp/{basename(image_url)}"
        png_image_path = splitext(original_image_path)[0] + ".png"

        # Save the original image
        with open(original_image_path, 'wb') as f:
            f.write(response.content)

        # Convert to PNG
        with Image.open(original_image_path) as img:
            img.save(png_image_path, "PNG")

        return png_image_path


class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self, comic="explosm"):
        super().__init__()

        self.min_w = 250
        self.min_h = 250

        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 1))
        self.pack_start(ComicReader(comic), True, True, 0)

    def resizeWidget(self, w, h):
        fw = max(w, self.min_w)
        fh = max(h, self.min_h)
        self.set_size_request(fw, fh)
        
if __name__ == "__main__":
    widgetwin = Gtk.Window()
    PREFIX=""
    MODPREFIX=""
    widgetwin.add(WidgetEngine_MainCall("loadingartist"))
    widgetwin.show_all()
    Gtk.main()

