import requests
from bs4 import BeautifulSoup

class ComicSource:
	def __init__(self):
		self.base_url = "https://loadingartist.com"
		
	def fetch_latest_comic(self):
		"""Fetches the latest comic from the homepage"""
		response = requests.get(self.base_url)
		if response.status_code != 200:
			raise Exception("Failed to load homepage")
		
		soup = BeautifulSoup(response.text, 'html.parser')
		latest_comic_element = soup.find("a", class_="comic-thumb wide highlight ribbon left only-img-hover section-comic")
		
		if latest_comic_element and 'href' in latest_comic_element.attrs:
			latest_comic_url = self.base_url + latest_comic_element['href']
			return self.fetch_comic_by_url(latest_comic_url)
		
		return None

	def fetch_comic_by_name(self, comic_name):
		"""Fetches a comic or art by its name (slug)"""
		comic_url = f"{self.base_url}/comic/{comic_name}/"
		art_url = f"{self.base_url}/art/{comic_name}/"

		# Try fetching as a comic
		try:
			return self.fetch_comic_by_url(comic_url)
		except Exception as e:
			print(f"Failed to load as comic, trying as art: {e}")
		
		# If it fails, try fetching as art
		try:
			return self.fetch_comic_by_url(art_url)
		except Exception as e:
			raise Exception(f"Failed to load comic or art page: {comic_name}. Error: {e}")

	def fetch_comic_by_url(self, comic_url):
		"""Fetches a comic by its URL"""
		response = requests.get(comic_url)
		if response.status_code != 200:
			raise Exception(f"Failed to load comic page: {comic_url}")
		
		soup = BeautifulSoup(response.text, 'html.parser')
		nav = self.get_navigation_links(soup)
		prev = nav.get("previous")
		next = nav.get("next")
		if next == None:
			next = None
		else:
			next = next.rsplit("/", 2)[1]
		if prev == None:
			prev = None
		else:
			prev = prev.rsplit("/", 2)[1]
		return {
			"name": self.get_comic_title(soup),
			"image": self.get_comic_image(soup),
			"prev": prev,
			"next": next
		}

	def get_comic_title(self, soup):
		title_element = soup.find("h1")
		return title_element.text.strip() if title_element else "Unknown Title"

	def get_comic_image(self, soup):
		main_image_container = soup.find("div", class_="main-image-container")
		if main_image_container:
			picture_element = main_image_container.find("picture")
			if picture_element:
				source_element = picture_element.find("source", {"type": "image/webp"})
				img_element = picture_element.find("img")
				
				if source_element and 'srcset' in source_element.attrs:
					# Extract the highest resolution WebP image from srcset
					image_url = source_element['srcset'].split(",")[-1].strip().split(" ")[0]
				elif img_element and 'srcset' in img_element.attrs:
					# Extract the highest resolution image from srcset as a fallback
					image_url = img_element['srcset'].split(",")[-1].strip().split(" ")[0]
				elif img_element and 'src' in img_element.attrs:
					# Fallback to the standard src attribute
					image_url = img_element['src']
				
				# Ensure the image URL is complete
				if not image_url.startswith('http'):
					image_url = self.base_url + image_url
				
				return image_url
		return "Image not found"

	def get_navigation_links(self, soup):
		nav_links = {}
		prev_element = soup.find("a", class_="btn-prev")
		if prev_element:
			nav_links["previous"] = self.base_url + prev_element['href']
		
		next_element = soup.find("a", class_="btn-next")
		if next_element:
			nav_links["next"] = self.base_url + next_element['href']
		
		random_element = soup.find("a", class_="btn-random")
		if random_element:
			nav_links["random"] = self.base_url + random_element['href']
		
		return nav_links
		
	def get_random_comic(self, name=""):
		return self.fetch_comic_by_url("https://loadingartist.com/random")

