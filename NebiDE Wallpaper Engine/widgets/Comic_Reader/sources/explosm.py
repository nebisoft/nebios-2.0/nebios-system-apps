import requests
from bs4 import BeautifulSoup
import json

class ComicSource:
	def __init__(self):
		self.base_url = "https://explosm.net"
		self.current_slug = None

	def fetch_latest_comic(self):
		response = requests.get(self.base_url)
		if response.status_code != 200:
			raise Exception("Failed to fetch the website.")
		
		soup = BeautifulSoup(response.text, 'html.parser')

		# JSON verisini al
		navigation_data = self.extract_navigation_data(soup)
		self.current_slug = navigation_data.get("name", "")

		# Görsel URL'sini al
		comic_image_url = self.extract_comic_image(soup)
		return {
			"name": navigation_data["slug"],
			"image": comic_image_url,
			"prev": navigation_data["previousSlug"],
			"next": navigation_data["nextSlug"],
			"random": navigation_data["randomSlug"]
		}

	def fetch_comic_by_name(self, slug):
		comic_url = f"{self.base_url}/comics/{slug}#comic"
		response = requests.get(comic_url)
		if response.status_code != 200:
			raise Exception(f"Failed to fetch the comic with slug: {slug}")
		
		soup = BeautifulSoup(response.text, 'html.parser')

		# JSON verisini al
		navigation_data = self.extract_navigation_data(soup)
		self.current_slug = navigation_data.get("name", slug)

		# Görsel URL'sini al
		comic_image_url = self.extract_comic_image(soup)
		return {
			"name": navigation_data["slug"],
			"image": comic_image_url,
			"prev": navigation_data["previousSlug"],
			"next": navigation_data["nextSlug"],
			"random": navigation_data["randomSlug"]
		}

	def extract_comic_image(self, soup):
		# Görsel URL'sini belirleyen HTML elementini bul
		comic_image = soup.find('div', {'class': 'MainComic__ComicImage-sc-ndbx87-2'})
		img_tag = comic_image.find('img')
		return img_tag['src'] if img_tag else None

	def extract_navigation_data(self, soup):
		# __NEXT_DATA__ script tag'ini bul ve içindeki JSON verisini al
		script_tag = soup.find('script', {'id': '__NEXT_DATA__'})
		if not script_tag:
			return {}

		json_data = json.loads(script_tag.string)

		# navigation verilerine doğru şekilde erişim
		comic_data = json_data.get("props", {}).get("pageProps", {}).get("urqlState", {})
		
		# Navigation verilerini bul
		for key, value in comic_data.items():
			data = json.loads(value.get("data", "{}"))
			if "comic" in data:
				navigation = data["comic"]["navigation"][0]
				return {
					"slug": data["comic"].get("slug"),
					"previousSlug": navigation.get("previousSlug"),
					"nextSlug": navigation.get("nextSlug"),
					"randomSlug": navigation.get("randomSlug")
				}
		return {}

	def get_previous_comic(self):
		if self.current_slug:
			navigation_data = self.fetch_comic_by_name(self.current_slug)["navigation"]
			return self.fetch_comic_by_name(navigation_data.get("previousSlug"))

	def get_next_comic(self):
		if self.current_slug:
			navigation_data = self.fetch_comic_by_name(self.current_slug)["navigation"]
			return self.fetch_comic_by_name(navigation_data.get("nextSlug"))

	def get_random_comic(self, name):
		navigation_data = self.fetch_comic_by_name(name)
		return self.fetch_comic_by_name(navigation_data.get("random"))

