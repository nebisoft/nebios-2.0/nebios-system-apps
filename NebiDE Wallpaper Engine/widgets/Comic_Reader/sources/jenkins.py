import requests
import json

class ComicSource:
    def __init__(self):
        self.base_url = "https://thejenkinscomic.net"
        self.comic_names, self.total_comics = self.fetch_comic_data()

    def fetch_comic_data(self):
        # comicsite.js dosyasını çek
        js_url = f"{self.base_url}/comicsite.js"
        response = requests.get(js_url)
        if response.status_code != 200:
            raise Exception("Failed to fetch the JS file.")
        
        js_content = response.text
        
        # COMIC_NAMES'i çıkar
        start_index = js_content.find('const COMIC_NAMES = [') + len('const COMIC_NAMES = [')
        end_index = js_content.find('];', start_index)
        comic_names_str = '{ "comics": ' + js_content[start_index:end_index].split("const COMIC_NAMES = \n", 1)[1].replace("'comic_name',", "", 1).replace("    '", " \"").replace(" '", " \"").replace("',", "\",").rsplit("\",", 1)[0] + "\"]}"

        try:
            comic_names = json.loads(comic_names_str)["comics"]
        except json.JSONDecodeError as e:
            raise Exception(f"Failed to parse COMIC_NAMES JSON: {e}")

        total_comics = len(comic_names)
        return comic_names, total_comics

    def get_comic_url(self, comic_number):
        return f"{self.base_url}/images/{comic_number}.png"

    def fetch_comic(self, comic_name):
        if comic_name not in self.comic_names:
            raise ValueError("Comic name not found")
        
        comic_number = self.comic_names.index(comic_name) + 1
        if comic_number < 1 or comic_number > self.total_comics:
            raise ValueError("Comic number out of range")
        
        comic_url = self.get_comic_url(comic_number)
        
        previous_comic = self.comic_names[comic_number - 2] if comic_number > 1 else None
        next_comic = self.comic_names[comic_number] if comic_number < self.total_comics else None
        
        return {
            "name": comic_name,
            "image": comic_url,
            "prev": previous_comic,
            "next": next_comic
        }

    def fetch_latest_comic(self):
        return self.fetch_comic(self.comic_names[-1])

    def fetch_comic_by_name(self, name):
        return self.fetch_comic(name)

    def get_previous_comic(self, current_name):
        current_number = self.comic_names.index(current_name) + 1
        if current_number < 1:
            raise ValueError("No previous comic available")
        return self.fetch_comic(self.comic_names[current_number - 2])

    def get_next_comic(self, current_name):
        current_number = self.comic_names.index(current_name) + 1
        if current_number >= self.total_comics:
            raise ValueError("No next comic available")
        return self.fetch_comic(self.comic_names[current_number])

    def get_random_comic(self, name):
        import random
        random_name = random.choice(self.comic_names)
        return self.fetch_comic(random_name)
