import gi
import cairo
import math
import psutil

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, Pango, PangoCairo

class SystemResourceWidget(Gtk.EventBox):
    def __init__(self):
        Gtk.EventBox.__init__(self)
        self.set_app_paintable(True)

        # Minimum width & height for the widget
        self.min_width = 200
        self.min_height = 200

        self.update_progress()
        # Update drawing area periodically
        GObject.timeout_add(1000, self.update_progress)

    def on_size_allocate(self, widget, allocation):
        # Resize Rules
        width = allocation.width
        height = allocation.height
        size = min(width, height)
        if size < self.min_width:
            size = self.min_width
        self.set_size_request(size, size)

    def draw_speedometer(self, context, x, y, radius, angle_start, angle_end, value, max_value, gradient_color, solid_line_width, shadow_line_width):
        td_bg = cairo.RadialGradient(x, y, radius - 15, x, y, radius)
        td_bg.add_color_stop_rgba(0, 1, 1, 1, 0)  # Transparent at the center
        td_bg.add_color_stop_rgba(1, 1, 1, 1, 0.5)  # Semi-transparent color at the outer edge

        td_fg = cairo.RadialGradient(x, y, radius - 15, x, y, radius)
        td_fg.add_color_stop_rgba(0.5, 1, 1, 1, 1)  # Transparent at the center
        td_fg.add_color_stop_rgba(1, gradient_color[0], gradient_color[1], gradient_color[2], 1)  # Gradient color at the outer edge

        context.set_line_width(solid_line_width)
        context.set_line_cap(cairo.LINE_CAP_ROUND)  # Make the line ends round
        context.set_source_rgba(0, 0, 0, 0)  # Set the source color to fully transparent black
        context.stroke_preserve()
        context.set_source_rgba(0, 0, 0, 0)  # Set the source color to semi-transparent black
        context.fill()

        # Draw colored portion based on value
        colored_end = angle_start + (angle_end - angle_start) * (value / max_value)
        context.set_source(td_bg)
        context.arc(x, y, radius, angle_start, angle_end)
        context.stroke()
        context.set_source(td_fg)
        context.arc(x, y, radius, angle_start, colored_end)
        context.stroke()

        # Draw shadow with gradient
        shadow_gradient = cairo.RadialGradient(x, y, radius + solid_line_width, x, y, radius)
        shadow_gradient.add_color_stop_rgba(0.5, 0, 0, 0, 0.3)  # Outer color (fully transparent black)
        shadow_gradient.add_color_stop_rgba(1, 0, 0, 0, 0)  # Outer color (fully transparent black)

        context.set_source(shadow_gradient)
        context.set_line_width(shadow_line_width)
        context.arc(x, y, radius, angle_start, angle_end)
        context.stroke()

    def draw_text(self, context, x, y, text, size, gradient_color):
        # Create a linear gradient for the text
        text_gradient = cairo.LinearGradient(x, y, x, y + size)
        text_gradient.add_color_stop_rgba(0, gradient_color[0], gradient_color[1], gradient_color[2], 0.5)  # Semi-transparent white at the starting point
        text_gradient.add_color_stop_rgba(1, gradient_color[0], gradient_color[1], gradient_color[2], 1)  # Fully opaque white at the ending point

        # Draw text with gradient
        font_description = Pango.font_description_from_string(f"Manrope Medium {size}")
        pango_ctx = PangoCairo.create_context(context)
        layout = Pango.Layout(pango_ctx)
        layout.set_font_description(font_description)
        layout.set_text(text)

        text_width, text_height = layout.get_pixel_size()
        x -= text_width / 2
        y -= text_height / 2

        # Draw transparent outline
        context.set_line_width(2)
        context.set_source_rgba(0, 0, 0, 0.1)  # Fully transparent black
        context.move_to(x - 1, y - 2)
        PangoCairo.show_layout(context, layout)
        context.move_to(x + 1, y + 2)
        PangoCairo.show_layout(context, layout)
        context.move_to(x - 1, y + 2)
        PangoCairo.show_layout(context, layout)
        context.move_to(x + 1, y - 2)
        PangoCairo.show_layout(context, layout)

        # Draw transparent outline
        context.set_line_width(1)
        context.set_source_rgba(0, 0, 0, 0.1)  # Fully transparent black
        context.move_to(x - 1, y - 1)
        PangoCairo.show_layout(context, layout)
        context.move_to(x + 1, y + 1)
        PangoCairo.show_layout(context, layout)
        context.move_to(x - 1, y + 1)
        PangoCairo.show_layout(context, layout)
        context.move_to(x + 1, y - 1)
        PangoCairo.show_layout(context, layout)

        # Draw text with gradient
        context.set_source(text_gradient)
        context.move_to(x, y)
        PangoCairo.show_layout(context, layout)

    def update_progress(self):
        # Fetch actual system resource data
        self.cpu_usage = psutil.cpu_percent()
        self.ram_usage = psutil.virtual_memory().percent
        self.disk_usage = psutil.disk_usage('/').percent

        # Trigger a redraw of the drawing area
        self.queue_draw()

        # Continue updating periodically
        return True

    def do_draw(self, context):
        # Calculate the scaling factor based on the current window width
        scale_factor = self.get_allocated_width() / 600  # 800 is the initial width, adjust as needed
        solid_line_width = 12 * scale_factor
        shadow_line_width = 16 * scale_factor

        # Draw "CPU" text above the CPU progress bar
        cpu_text = "CPU"
        cpu_text_size = 12 * scale_factor
        cpu_value = f"{int(self.cpu_usage)}%"
        cpu_value_size = 24 * scale_factor
        context.rotate(math.pi / 2)
        self.draw_speedometer(context, 100 * scale_factor, -100 * scale_factor, 80 * scale_factor, math.pi / 4,
                              7 * math.pi / 4, self.cpu_usage, 100, [0.419, 0.804, 0.804], solid_line_width, shadow_line_width)
        context.rotate(-(math.pi / 2))
        self.draw_text(context, 100 * scale_factor, 150 * scale_factor, cpu_text, cpu_text_size, [0.960, 0.960, 0.960])
        self.draw_text(context, 100 * scale_factor, 100 * scale_factor, cpu_value, cpu_value_size, [0.960, 0.960, 0.960])

        # Draw "RAM" text above the RAM progress bar
        ram_text = "RAM"
        ram_text_size = 12 * scale_factor
        ram_value = f"{int(self.ram_usage)}%"
        ram_value_size = 24 * scale_factor
        context.rotate(math.pi / 2)
        self.draw_speedometer(context, 100 * scale_factor, -300 * scale_factor, 80 * scale_factor, math.pi / 4,
                              7 * math.pi / 4, self.ram_usage, 100, [0.973, 0.769, 0.282], solid_line_width, shadow_line_width)
        context.rotate(-(math.pi / 2))
        self.draw_text(context, 300 * scale_factor, 150 * scale_factor, ram_text, ram_text_size, [0.960, 0.960, 0.960])
        self.draw_text(context, 300 * scale_factor, 100 * scale_factor, ram_value, ram_value_size, [0.960, 0.960, 0.960])

        # Draw "Disk" text above the Disk progress bar
        disk_text = "Disk"
        disk_text_size = 12 * scale_factor
        disk_value = f"{int(self.disk_usage)}%"
        disk_value_size = 24 * scale_factor
        context.rotate(math.pi / 2)
        self.draw_speedometer(context, 100 * scale_factor, -500 * scale_factor, 80 * scale_factor, math.pi / 4,
                              7 * math.pi / 4, self.disk_usage, 100, [0.419, 1.0, 0.419], solid_line_width, shadow_line_width)
        context.rotate(-(math.pi / 2))
        self.draw_text(context, 500 * scale_factor, 150 * scale_factor, disk_text, disk_text_size, [0.960, 0.960, 0.960])
        self.draw_text(context, 500 * scale_factor, 100 * scale_factor, disk_value, disk_value_size, [0.960, 0.960, 0.960])

        # Restore the previous transformation matrix
        context.save()
        context.restore()

        self.set_border_width(16 * scale_factor)


class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self):
        # NebiDE Wallpaper Engine (WE) will use this class to call the widget.
        Gtk.VBox.__init__(self)

        # Minimum width & height (in non-DPI pixel) for widget reported to NebiDE WE.
        self.min_w = 500
        self.min_h = 240
        self.override_background_color(Gtk.StateFlags.NORMAL,
                                       Gdk.RGBA(0, 0, 0, 0.5))  # Set the background color to transparent

        # Replace MyWidget() with SystemResourceWidget()
        self.pack_start(SystemResourceWidget(), True, True, 0)

    def resizeWidget(self, w, h):
        # Resize Rules. DO NOT DELETE OR EDIT!
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)

if __name__ == "__main__":
    # Executed when no connected NebiDE Wallpaper Engine runtime found.
    widgetwin = Gtk.Window()
    widgetwin.add(WidgetEngine_MainCall())
    widgetwin.show_all()
    Gtk.main()
