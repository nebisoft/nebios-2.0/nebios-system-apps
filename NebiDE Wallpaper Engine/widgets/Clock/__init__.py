#!/usr/bin/env python3
import gi
import cairo
import math


gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

class AnalogClock(Gtk.DrawingArea):
    def __init__(self):
        Gtk.DrawingArea.__init__(self)
        self.radius = 0.0
        self.center_x = 0.0
        self.center_y = 0.0
        self.connect("draw", self.on_draw)
        self.connect("size-allocate", self.on_size_allocate)
        GLib.timeout_add(1000 / 10, self.always_draw)

    def always_draw(self):
        self.queue_draw()
        GLib.timeout_add(1000 / 10, self.always_draw)

    def on_draw(self, widget, cr):
        cr.set_line_cap(cairo.LINE_CAP_ROUND)
        cr.set_line_width(min(self.get_allocation().width, self.get_allocation().height) / 32)
        self.draw_hour_hand(cr)
        self.draw_minute_hand(cr)
        self.draw_second_hand(cr)

    def draw_hour_hand(self, cr):
        current_time = GLib.DateTime.new_now_local()
        hours = current_time.get_hour()
        minutes = current_time.get_minute()
        angle = math.radians(((hours % 12) + minutes / 60) * 30 - 90)
        hand_length = self.radius * 0.5
        x = self.center_x + hand_length * math.cos(angle)
        y = self.center_y + hand_length * math.sin(angle)
        cr.move_to(self.center_x, self.center_y)
        cr.line_to(x, y)
        cr.set_source_rgb(1.0, 0.37254901960784315, 0.37254901960784315)
        cr.stroke()

    def draw_minute_hand(self, cr):
        current_time = GLib.DateTime.new_now_local()
        minutes = current_time.get_minute()
        seconds = current_time.get_second()
        angle = math.radians((minutes + seconds / 60) * 6 - 90)
        hand_length = self.radius * 0.7
        x = self.center_x + hand_length * math.cos(angle)
        y = self.center_y + hand_length * math.sin(angle)
        cr.move_to(self.center_x, self.center_y)
        cr.line_to(x, y)
        cr.set_source_rgb(0.9529411764705882, 0.6137254901960784, 0.2568627450980392)
        cr.stroke()

    def draw_second_hand(self, cr):
        current_time = GLib.DateTime.new_now_local()
        seconds = current_time.get_second()
        microseconds = current_time.get_microsecond()
        angle = math.radians(
            seconds * 6 + microseconds / 1000000 * 6 - 90)
        hand_length = self.radius * 0.9
        x = self.center_x + hand_length * math.cos(angle)
        y = self.center_y + hand_length * math.sin(angle)
        cr.move_to(self.center_x, self.center_y)
        cr.line_to(x, y)
        cr.set_source_rgb(0.9725490196078431, 0.7686274509803922, 0.2823529411764706)
        cr.stroke()

    def on_size_allocate(self, widget, allocation):
        self.radius = min(allocation.width, allocation.height) / 2 - 10
        self.center_x = allocation.width / 2
        self.center_y = allocation.height / 2

class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self):
        Gtk.VBox.__init__(self)
        self.min_w = 128
        self.min_h = 128
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0.85))
        self.add(AnalogClock())

    def resizeWidget(self, w, h):
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)
