#!/usr/bin/env python3
import os
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Pango

PARENT = None

class WidgetAdder(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_border_width(12)

        self.grid = Gtk.Grid()
        self.grid.set_row_homogeneous(True)
        self.grid.set_column_homogeneous(True)
        self.grid.set_row_spacing(12)
        self.grid.set_column_spacing(12)

        scr_win = Gtk.ScrolledWindow()
        scr_win.add(self.grid)
        scr_win.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.pack_start(scr_win, True, True, 0)

        # Find folders in the ./widgets directory
        widget_folder = "./widgets"
        folders = [f for f in os.listdir(widget_folder) if os.path.isdir(os.path.join(widget_folder, f))]
        folders.remove("Add_Widgets")

        # Create buttons for each folder
        for index, folder in enumerate(folders):
            # Replace underscores with spaces
            display_name = folder.replace("_", " ")

            button = Gtk.Button()
            button.set_size_request(-1, 48)
            button_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
            button_box.set_spacing(6)
            button_box.set_margin_top(6)
            button_box.set_margin_bottom(6)
            button_box_img = Gtk.Image.new_from_file("./widgets/" + folder + "/icon.png")
            button_box_label = Gtk.Label(display_name)
            button_box_label.set_ellipsize(Pango.EllipsizeMode.END)
            button_box.pack_start(button_box_img, True, True, 0)
            button_box.pack_start(button_box_label, False, True, 0)
            button.add(button_box)
            button.eval = folder
            button.connect("clicked", self.AddWidget)
            button.set_hexpand(False)
            self.grid.attach(button, index % 3, index // 3, 1, 1)

        self.show_all()

    def AddWidget(self, btn):
        widget_folder = os.path.join("./widgets", btn.eval)
        args_path = os.path.join(widget_folder, "args")

        if os.path.exists(args_path):
            with open(args_path, "r") as args_file:
                args_definition = args_file.read()
            args_dialog = ArgsDialog(args_definition)
            response = args_dialog.run()

            if response == Gtk.ResponseType.OK:
                args = args_dialog.get_args()
                PARENT.WID_ENGINE.add_widget([btn.eval, *args], 0, 0, 1, 1)  # Modified this line
            args_dialog.destroy()
        else:
            PARENT.WID_ENGINE.add_widget([btn.eval], 0, 0, 1, 1)

class ArgsDialog(Gtk.Dialog):
    def __init__(self, args_definition):
        Gtk.Dialog.__init__(self, "Set Arguments", None, Gtk.DialogFlags.MODAL)
        self.set_default_size(300, 150)

        box = self.get_content_area()
        self.args_entry = Gtk.TextView()
        box.add(self.args_entry)
        self.args_entry.get_buffer().set_text(args_definition)

        self.add_button("Cancel", Gtk.ResponseType.CANCEL)
        self.add_button("OK", Gtk.ResponseType.OK)

        self.show_all()

    def get_args(self):
        buffer:Gtk.TextBuffer = self.args_entry.get_buffer()
        startIter, endIter = buffer.get_bounds()
        args_text = buffer.get_text(startIter, endIter, True)
        args_list = args_text.splitlines()
        args = [arg.strip() for arg in args_list if arg.strip()]
        return args

class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self, parent):
        Gtk.VBox.__init__(self)
        global PARENT
        self.min_w = 360
        self.min_h = 360
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0, 0, 0, 0.75))
        PARENT = parent
        self.RemoveOnExitEdit = True
        self.add(WidgetAdder())

    def resizeWidget(self, w, h):
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)