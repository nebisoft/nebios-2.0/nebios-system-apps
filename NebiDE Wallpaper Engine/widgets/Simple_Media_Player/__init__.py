import subprocess
import time

from PIL import Image
import gi
import cairo, math

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf, Gdk, GLib, Pango

class MediaPlayer(Gtk.Box):
    def __init__(self, parent):
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL, spacing=32)
        self.set_border_width(32)
        self.cover_output = ""
        self.PARENT = parent
        self.cf_timeout_id = None
        self.crossfade_duration = 1  # Set the duration of the crossfade in seconds

        self.cover_image = Gtk.Image()
        pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, 144, 144)
        self.cover_image.set_from_pixbuf(pixbuf)
        self.cover_image.set_halign(Gtk.Align.CENTER)
        self.cover_image.set_valign(Gtk.Align.CENTER)
        self.cover_image.get_style_context().add_class("cover-image")
        self.pack_start(self.cover_image, False, False, 0)

        details_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=16)
        # Song details labels (replace with actual details)
        self.artist_album_label = Gtk.Label(label="Artist Name · Album Name")
        self.song_label = Gtk.Label(label="Song Name")

        self.artist_album_label.set_xalign(0.0)
        self.song_label.set_xalign(0.0)

        self.artist_album_label.set_ellipsize(Pango.EllipsizeMode.END)
        self.song_label.set_ellipsize(Pango.EllipsizeMode.END)

        self.song_label.modify_font(Pango.FontDescription("Bold 20"))

        details_box.pack_start(self.artist_album_label, True, True, 0)
        details_box.pack_start(self.song_label, True, True, 0)

        # Progress bar
        self.progress_bar = Gtk.ProgressBar()
        details_box.pack_start(self.progress_bar, True, True, 0)

        # Play, Pause, Next, Prev buttons
        button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        prev_button = Gtk.Button(label="Prev")
        playpause_button = Gtk.Button(label="Play/pause")
        next_button = Gtk.Button(label="Next")
        button_box.pack_start(prev_button, False, False, 0)
        button_box.pack_start(playpause_button, False, False, 0)
        button_box.pack_start(next_button, False, False, 0)

        self.pack_start(details_box, True, True, 0)
        #self.pack_start(button_box, True, True, 0)
        GLib.timeout_add(1000, self.set_song_info)

    def extract_dominant_colors(self, image_path):
        try:
            image = Image.open(image_path)
            # Resize the image for faster processing if needed
            image.thumbnail((100, 100))  # You can adjust the size as needed

            # Get pixel data from the image
            pixels = list(image.getdata())

            # Create a dictionary to count color occurrences
            color_count = {}

            for pixel in pixels:
                # Convert the pixel data to RGB format
                r, g, b = pixel[:3]
                color = (r, g, b)

                if color in color_count:
                    color_count[color] += 1
                else:
                    color_count[color] = 1

            # Sort colors by count in descending order
            sorted_colors = sorted(color_count.items(), key=lambda x: x[1], reverse=True)

            # Extract the top 2 dominant colors
            dominant_colors = [sorted_colors[64], sorted_colors[128]]

            # Convert RGB values to GdkRGBA format
            gdk_colors = [[r, g, b] for (r, g, b), _ in dominant_colors]

            return gdk_colors

        except Exception as e:
            print(f"Error extracting colors: {e}")
            return []

    def set_bg(self):
        # Define CSS as a string
        css = ("""
                /* Define a gradient background with two colors (adjust as needed) */
                .gradient-background {
                    background: linear-gradient(to bottom, rgb(""" +
                        str(self.colors[0][0]) + "," + str(self.colors[0][1]) + "," + str(self.colors[0][2]) +
               """), rgb(""" +
                        str(self.colors[1][0]) + "," + str(self.colors[1][1]) + "," + str(self.colors[1][2]) +
               """));
                }
                
                .song-text, .artist-text {
                    color: white;
                    text-shadow: 0 1px 2px black;
                }
                
                .crossfade {
                    transition: opacity """ + str(self.crossfade_duration) + """s ease-in-out;
                }
                
                .cover-image {
                    border-radius: 6px;  /* Adjust the border-radius as needed for rounded corners */
                    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.5);  /* Adjust the box-shadow as needed for the desired shadow effect */
                }
                """)

        # Load the CSS string
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(css.encode())  # Encode the CSS string to bytes

        # Apply the CSS styles to the entire application
        screen = Gdk.Screen.get_default()
        context = Gtk.StyleContext()
        context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.PARENT.get_style_context().add_class("gradient-background")
        self.artist_album_label.get_style_context().add_class("artist-text")
        self.song_label.get_style_context().add_class("song-text")

    def set_song_info(self):
        try:
            if self.cover_output != subprocess.check_output(["playerctl", "metadata", "mpris:artUrl"]).decode('utf-8'):
                self.cover_output = subprocess.check_output(["playerctl", "metadata", "mpris:artUrl"]).decode('utf-8')
                if self.cover_output.startswith("file://"):
                    if self.cf_timeout_id is not None:
                        GLib.source_remove(self.cf_timeout_id)
                        self.cf_timeout_id = None
                    cover_output = self.cover_output.replace("file://", "", 1).split("\n")[0]
                    print(cover_output)

                    # Update other UI elements
                    self.colors = self.extract_dominant_colors(cover_output)
                    self.set_bg()

                    self.cf_timeout_id = GLib.timeout_add_seconds(0, self.finish_crossfade, cover_output)

            artist_output = subprocess.check_output(["playerctl", "metadata", "xesam:artist"]).decode().strip()
            album_output = subprocess.check_output(["playerctl", "metadata", "xesam:album"]).decode().strip()
            name_output = subprocess.check_output(["playerctl", "metadata", "xesam:title"]).decode().strip()

            if album_output.__contains__(name_output):
                self.artist_album_label.set_label(artist_output)
            else:
                self.artist_album_label.set_label(artist_output + " · " + album_output)
            self.song_label.set_label(name_output)

            # Get the song length in seconds
            length_output = subprocess.check_output(["playerctl", "metadata", "mpris:length"])
            song_length_seconds = int(length_output.decode().strip()) // 1_000_000

            # Get the current position in seconds
            position_output = subprocess.check_output(["playerctl", "position"])
            current_position_seconds = int(float(position_output.decode().strip()))

            self.progress_bar.set_fraction(float(current_position_seconds) / song_length_seconds)

        except subprocess.CalledProcessError as e:
            # Handle errors, e.g., if no media player is running
            print(f"Error: {e}")

        return True

    def finish_crossfade(self, cover_output):
        # Load the new cover image after the crossfade
        new_pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(cover_output, 144, 144, True)

        # Create a composite image with transparency
        composite_pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, 144, 144)
        composite_pixbuf.fill(0x00000000)  # Fill with transparent black

        # Blend the old and new images progressively
        for alpha in range(0, 256):
            # Linear interpolation for alpha values
            alpha_old = int(255 - alpha)
            alpha_new = int(alpha)

            # Copy the old image with transparency
            self.cover_image.get_pixbuf().composite(
                composite_pixbuf,
                0, 0, 144, 144,  # Destination rectangle
                0, 0, 1, 1,  # Source rectangle (entire source)
                GdkPixbuf.InterpType.BILINEAR,
                alpha_old
            )

            # Copy the new image with reduced transparency
            new_pixbuf.composite(
                composite_pixbuf,
                0, 0, 144, 144,  # Destination rectangle
                0, 0, 1, 1,  # Source rectangle (entire source)
                GdkPixbuf.InterpType.BILINEAR,
                alpha_new
            )

            # Set the composite image
            self.cover_image.set_from_pixbuf(self.create_rounded_image(composite_pixbuf))

            # Wait a short time before updating the transparency
            Gtk.main_iteration_do(True)

            time.sleep(0.01)  # Adjust the sleep time as needed for a smooth transition

        GLib.source_remove(self.cf_timeout_id)
        self.cf_timeout_id = None
        return False

    def create_rounded_image(self, pixbuf):
        # Create a surface for drawing
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, pixbuf.get_width(), pixbuf.get_height())
        ctx = cairo.Context(surface)

        # Draw the rounded image
        radius = 6  # Set the radius for rounded corners
        ctx.arc(radius, radius, radius, math.pi, 1.5 * math.pi)
        ctx.arc(pixbuf.get_width() - radius, radius, radius, 1.5 * math.pi, 2 * math.pi)
        ctx.arc(pixbuf.get_width() - radius, pixbuf.get_height() - radius, radius, 0, 0.5 * math.pi)
        ctx.arc(radius, pixbuf.get_height() - radius, radius, 0.5 * math.pi, math.pi)

        ctx.close_path()
        ctx.clip()

        Gdk.cairo_set_source_pixbuf(ctx, pixbuf, 0, 0)
        ctx.paint()

        # Convert Cairo surface to GdkPixbuf
        gdk_pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, surface.get_width(), surface.get_height())

        return gdk_pixbuf

# Copied from NebiOS SDK examples.
class WidgetEngine_MainCall(Gtk.VBox):
    def __init__(self):
        # NebiDE Wallpaper Engine (WE) will use this class to call the widget.
        Gtk.VBox.__init__(self)

        # Minimum width & height (in non-DPI pixel) for widget reported to NebiDE WE.
        self.min_w = 160
        self.min_h = 250

        # You can set custom background color if needed. Else, comment/delete the line below.
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 0.25))

        # Replace MyWidget() to YourClassName(args if needed). Note that your class should be
        # subclass of GTK Box. QT not supported since NebiDE WE built with GTK.
        self.pack_start(MediaPlayer(self), True, True, 0)

    def resizeWidget(self, w, h):
        # Resize Rules. DO NOT DELETE!
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)

if __name__ == "__main__":
    # Executed when no connected NebiDE Wallpaper Engine runtime found.
    widgetwin = Gtk.Window()
    widgetwin.add(WidgetEngine_MainCall())
    widgetwin.show_all()
    Gtk.main()

