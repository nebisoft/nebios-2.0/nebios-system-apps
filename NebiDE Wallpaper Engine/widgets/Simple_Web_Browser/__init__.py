#!/usr/bin/env python3
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, WebKit2, Gdk, GLib, Gio

class WidgetEngine_MainCall(Gtk.VBox):
    def on_load_changed(self, web_view, event):
        if event == WebKit2.LoadEvent.COMMITTED:
            self.url_entry.set_text(web_view.get_uri())

    def on_entry_activate(self, entry):
        url:str = entry.get_text()
        if not url.startswith("http"):
            url = "https://" + url
        self.browser.load_uri(url)

    def __init__(self, url):
        Gtk.VBox.__init__(self)
        self.min_w = 480
        self.min_h = 360
        self.vbox = Gtk.VBox()

        headerbar = Gtk.HeaderBar()
        headerbar.set_show_close_button(False)

        # Back
        back_button = Gtk.Button()
        back_icon = Gio.ThemedIcon(name="go-previous-symbolic")
        back_image = Gtk.Image.new_from_gicon(back_icon, Gtk.IconSize.BUTTON)
        back_button.add(back_image)
        back_button.connect('clicked', lambda _: self.browser.go_back())
        headerbar.pack_start(back_button)

        # Reload
        refresh_button = Gtk.Button()
        refresh_icon = Gio.ThemedIcon(name="view-refresh-symbolic")
        refresh_image = Gtk.Image.new_from_gicon(refresh_icon, Gtk.IconSize.BUTTON)
        refresh_button.add(refresh_image)
        refresh_button.connect('clicked', lambda _: self.browser.reload())
        headerbar.pack_start(refresh_button)

        # URL
        self.url_entry = Gtk.Entry()
        self.url_entry.set_size_request(1, 48)
        self.url_entry.set_margin_start(2)
        self.url_entry.set_margin_end(2)
        self.url_entry.set_hexpand(True)
        self.url_entry.connect('activate', self.on_entry_activate)
        self.url_entry.set_placeholder_text("http://nebisoftware.com")
        headerbar.set_custom_title(self.url_entry)

        # Next
        forward_button = Gtk.Button()
        forward_icon = Gio.ThemedIcon(name="go-next-symbolic")
        forward_image = Gtk.Image.new_from_gicon(forward_icon, Gtk.IconSize.BUTTON)
        forward_button.add(forward_image)
        forward_button.connect('clicked', lambda _: self.browser.go_forward())
        headerbar.pack_end(forward_button)

        # Home
        home_button = Gtk.Button()
        home_icon = Gio.ThemedIcon(name="user-home-symbolic")
        home_image = Gtk.Image.new_from_gicon(home_icon, Gtk.IconSize.BUTTON)
        home_button.add(home_image)
        home_button.connect('clicked', lambda _: self.browser.load_uri(url))
        headerbar.pack_end(home_button)

        self.vbox.pack_start(headerbar, False, 0, 0)

        self.browser = WebKit2.WebView()
        self.browser.connect('load-changed', self.on_load_changed)
        self.browser.load_uri(url)
        self.vbox.set_margin_top(4)
        self.vbox.set_margin_end(4)
        self.vbox.set_margin_start(4)
        self.vbox.set_margin_bottom(4)
        self.browser.get_style_context().add_class("WidgetAPI_CSS_Rounded_Corner")
        self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1, 1, 1, 1))

        self.vbox.pack_start(self.browser, True, 1, 1)

        self.add(self.vbox)

    def resizeWidget(self, w, h):
        fw = w
        fh = h
        if w < self.min_w:
            fw = self.min_w
        if h < self.min_h:
            fh = self.min_h
        self.set_size_request(fw, fh)