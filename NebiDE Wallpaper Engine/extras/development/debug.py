import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DebugMenu(Gtk.VBox):
    def __init__(self, parent):
        Gtk.Box.__init__(self)

        self.set_margin_top(100)

        self.WallpaperArray1 = [
                "/System/Wallpapers/Live/Khalifa by Norbert Yanzon.mp4",
                "/System/Wallpapers/Live/Jammy Jellyfish by Magda Ehlers.mp4",
                "/System/Wallpapers/Image/Capadocia by Francesco Ungaro.jpg",
                "/System/Wallpapers/Image/Galata (Night) by Zafer Erdoğan.jpg",
                "/System/Wallpapers/Live/Ant by Michal Marek.mp4",
            ]

        self.WallpaperArray2 = [
                "/System/Wallpapers/Image/Galata (Night) by Zafer Erdoğan.jpg",
                "/System/Wallpapers/Live/Ant by Michal Marek.mp4",
                "/System/Wallpapers/Image/Capadocia by Francesco Ungaro.jpg",
                "/System/Wallpapers/Live/Earth.mp4",
            ]


        self.WallpaperArray3 = [
                "/System/Wallpapers/Image/Capadocia by Francesco Ungaro.jpg",
                "/System/Wallpapers/Image/Galata (Night) by Zafer Erdoğan.jpg",
                "/System/Wallpapers/Live/Khalifa by Norbert Yanzon.mp4",
                "/System/Wallpapers/Live/Jammy Jellyfish by Magda Ehlers.mp4",
                "/System/Wallpapers/Live/Earth.mp4",
            ]

        self.config = {
                "transition_duration_msec": 500,
                "slideshow_duration_sec": 3,
                "videorender_speed": 0.5,
                "slideshow_shuffle": True,
                "slideshow_rendermode": "Fill",
            }

        w1btn = Gtk.Button("W1")
        w1btn.connect("clicked", lambda _: parent.RENDERER.UpdateConfig(self.WallpaperArray1, self.config))
        self.pack_start(w1btn, True, True, 0)

        w2btn = Gtk.Button("W2")
        w2btn.connect("clicked", lambda _: parent.RENDERER.UpdateConfig(self.WallpaperArray2, self.config))
        self.pack_start(w2btn, True, True, 0)

        w3btn = Gtk.Button("W3")
        w3btn.connect("clicked", lambda _: parent.RENDERER.UpdateConfig(self.WallpaperArray3, self.config))
        self.pack_start(w3btn, True, True, 0)
