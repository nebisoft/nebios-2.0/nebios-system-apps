#!/usr/bin/env python3
# This file will compile with Nuitka Python Compiler at build sequence.
import configparser
import gc
import cv2
import gettext
import gi
import os
import random
import importlib
import cairo
import json
import utils.Wallpaper_Properties.main as WallpaperManager

gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GdkPixbuf, Gdk, GLib, WebKit2, Gio

NEBIDE_WE_VER = 2.0
WE_CONF_PATH = os.path.expanduser('~/.config/NebiSoft/NebiDE.conf')
WE_WID_CONF_PATH = os.path.expanduser('~/.config/NebiSoft/NebiDE.widgetengine.json')

PRIMARY_EDIT_MODE = False
IS_A_WID_FOCUSED = False

translations = gettext.translation('we_TEMPLATE', localedir='locale', fallback=True, languages=['en_US', 'tr_TR'])
translations.install()

class ItemContainer(Gtk.EventBox):
    def __init__(self, widget, fixed_container, id, border_size=6):
        Gtk.EventBox.__init__(self)
        self.wid_id = id
        self.context_menu = self.create_wid_context_menu()
        self.SENSITIVITY = 10
        self.fixed:WidgetEngine = fixed_container
        self.the_child = widget
        self.is_on_widget = False
        self.border_size = border_size
        self.resize_box = Gtk.EventBox()
        self.resize_box.set_border_width(self.border_size - 1)
        self.resize_box.connect("button_press_event", self.rb_button_press_event)
        self.resize_box.connect('enter-notify-event', self.rb_on_enter_event)
        self.resize_box.connect('leave-notify-event', self.rb_on_leave_event)
        self.child_box = Gtk.EventBox()
        self.child_box.connect('enter-notify-event', self.cb_on_enter_event)
        self.child_box.connect("button_press_event", self.cb_button_press_event)
        self.resize_box.connect('leave-notify-event', self.cb_on_leave_event)
        if self.the_child.get_style_context().get_background_color(Gtk.StateFlags.NORMAL).alpha == 0:
            self.the_child.override_background_color(Gtk.StateFlags.NORMAL, Gtk.Window().get_style_context().
                                                    get_background_color(Gtk.StateFlags.NORMAL))
        self.child_box.set_border_width(3)
        self.child_box.add(self.the_child)
        self.the_child.get_style_context().add_class("WidgetEngine_ChildObject")
        self.the_child.get_style_context().add_class("WidgetEngine_ChildContainer")
        self.resize_box.add(self.child_box)
        self.add(self.resize_box)
        self.offsetx = 0
        self.offsety = 0
        self.px = 0
        self.py = 0
        self.maxx = 0
        self.maxy = 0
        self.connect("button_press_event", self.button_press_event)
        self.connect("motion_notify_event", self.motion_notify_event)
        self.connect('enter-notify-event', self.on_enter_event)
        self.connect('leave-notify-event', self.on_leave_event)
        self.set_events(Gdk.EventMask.BUTTON_PRESS_MASK | Gdk.EventMask.BUTTON1_MOTION_MASK)

    def rb_on_enter_event(self, widget, event):
        global PRIMARY_EDIT_MODE
        if PRIMARY_EDIT_MODE == True:
            acc = widget.get_allocation()
            resize_range = 10  # Range of pixels around the edges for resizing

            top = (event.y <= resize_range)
            bottom = (event.y >= acc.height - resize_range)
            left = (event.x <= resize_range)
            right = (event.x >= acc.width - resize_range)

            cursor_type = Gdk.CursorType.FLEUR  # Default cursor type

            if top and left:
                cursor_type = Gdk.CursorType.TOP_LEFT_CORNER
            elif bottom and right:
                cursor_type = Gdk.CursorType.BOTTOM_RIGHT_CORNER
            elif top:
                cursor_type = Gdk.CursorType.TOP_SIDE
            elif bottom:
                cursor_type = Gdk.CursorType.BOTTOM_SIDE
            elif left:
                cursor_type = Gdk.CursorType.LEFT_SIDE
            elif right:
                cursor_type = Gdk.CursorType.RIGHT_SIDE

            cursor = Gdk.Cursor(cursor_type)
            widget.get_window().set_cursor(cursor)

    def rb_on_leave_event(self, widget, event):
        # Set the cursor to the default cursor when the mouse pointer leaves the EventBox
        cursor = Gdk.Cursor(Gdk.CursorType.ARROW)
        widget.get_window().set_cursor(cursor)

    def on_enter_event(self, widget, event):
        global IS_A_WID_FOCUSED
        IS_A_WID_FOCUSED = True
        global PRIMARY_EDIT_MODE
        if PRIMARY_EDIT_MODE == True:
            cursor = Gdk.Cursor(Gdk.CursorType.FLEUR)
            self.moveable = True
            widget.get_window().set_cursor(cursor)

    def cb_on_enter_event(self, widget, event):
        # Set the cursor to the default cursor when the mouse pointer leaves the EventBox
        cursor = Gdk.Cursor(Gdk.CursorType.ARROW)
        self.resize_power = 0
        self.is_on_widget = True
        self.moveable = False
        widget.get_window().set_cursor(cursor)

    def cb_on_leave_event(self, widget, event):
        self.is_on_widget = False

    def on_leave_event(self, widget, event):
        global IS_A_WID_FOCUSED
        IS_A_WID_FOCUSED = False
        # Set the cursor to the default cursor when the mouse pointer leaves the EventBox
        cursor = Gdk.Cursor(Gdk.CursorType.ARROW)
        self.resize_power = 0
        widget.get_window().set_cursor(cursor)

    def cb_button_press_event(self, widget, event):
        #self.fixed.child_set_property(widget, 'position', 99)
        return

    def rb_button_press_event(self, widget, event):
        global PRIMARY_EDIT_MODE
        if PRIMARY_EDIT_MODE == True:
            if event.button == 1:
                acc = widget.get_allocation()
                resize_range = 3
                self.moveable = True
                self.resize_align = "nil"

                top = (event.y <= resize_range)
                bottom = (event.y >= acc.height - resize_range)
                left = (event.x <= resize_range)
                right = (event.x >= acc.width - resize_range)
                self.moveable = not self.is_on_widget

                if top:
                    self.resize_power = 1
                elif bottom:
                    self.resize_power = -1
                elif left:
                    self.resize_power = 1
                elif right:
                    self.resize_power = -1
            else:
                self.moveable = False

    def create_wid_context_menu(self):
        context_menu = Gtk.Menu()
        remove_item = Gtk.MenuItem(label=_("Remove Widget"))
        context_menu.append(remove_item)
        remove_item.connect("activate", lambda _: self.RemoveWid())
        edit_item = Gtk.MenuItem(label=_("Edit Widget"))
        context_menu.append(edit_item)
        edit_item.connect("activate", lambda _: self.EditWidget(self.fixed.widget_array[self.wid_id]))
        context_menu.show_all()
        return context_menu

    def RemoveWid(self):
        self.fixed.remove(self)
        self.fixed.widget_array.pop(self.wid_id)
        self.destroy()

    def button_press_event(self, w, event):
        global PRIMARY_EDIT_MODE
        if PRIMARY_EDIT_MODE == True:
            if event.button == 3:
                print("button_press_event", str(IS_A_WID_FOCUSED))
                if IS_A_WID_FOCUSED == True:
                    GLib.timeout_add(100, lambda:
                        self.context_menu.popup(None, None, None, None, event.button, event.time))
            elif event.button == 1:
                if self.moveable == True:
                    acc = w.get_allocation()
                    resize_range = self.border_size

                    top = (event.y <= resize_range)
                    bottom = (event.y >= acc.height - resize_range)
                    left = (event.x <= resize_range)
                    right = (event.x >= acc.width - resize_range)
                    self.start_widget_rect = w.get_allocation()

                    if top or bottom or left or right:
                        if event.button == 1 or self.resize_power != 0:
                            p = w.get_parent()
                            # offset == distance of parent widget from edge of screen ...
                            self.offsetx, self.offsety = p.get_window().get_position()
                            # plus distance from pointer to edge of widget
                            self.offsetx += event.x
                            self.offsety += event.y
                            # maxx, maxy both relative to the parent
                            # note that we're rounding down now so that these max values don't get
                            # rounded upward later and push the widget off the edge of its parent.
                            self.maxx = self.RoundDownToMultiple(p.get_allocation().width - w.get_allocation().width,
                                                                 self.SENSITIVITY)
                            self.maxy = self.RoundDownToMultiple(p.get_allocation().height - w.get_allocation().height,
                                                                 self.SENSITIVITY)
                else:
                    self.moveable == False

    def motion_notify_event(self, widget, event):
        if not self.moveable:
            return

        global PRIMARY_EDIT_MODE
        if PRIMARY_EDIT_MODE == False:
            return

        x = event.x_root - self.offsetx + self.fixed.get_parent().get_hadjustment().get_value()
        y = event.y_root - self.offsety

        # Calculate the potential new position of the widget
        x = self.RoundToNearestMultiple(self.Max(x, 0), self.SENSITIVITY)
        y = self.RoundToNearestMultiple(self.Max(self.Min(y, self.maxy - 20 - 20 - 56), 36), self.SENSITIVITY)

        # Check if the potential new position overlaps with any other widget in the dictionary
        widget_rect = Gdk.Rectangle()
        widget_rect.x = x
        widget_rect.y = y
        widget_rect.width = widget.get_allocation().width
        widget_rect.height = widget.get_allocation().height
        for child in self.fixed.get_children():
            if child != widget:
                child_rect = child.get_allocation()
                if x != self.px or y != self.py:
                    new_x = x
                    new_y = y
                    if self.resize_power != 0:
                        if self.resize_power == 1:
                            if new_x <= self.start_widget_rect.x and new_y <= self.start_widget_rect.y:
                                new_x = x - (self.border_size * 3)
                                new_y = y - (self.border_size * 3)
                            else:
                                new_x = self.start_widget_rect.x
                                new_y = self.start_widget_rect.y
                            resize_x = self.start_widget_rect.width + (
                                        self.resize_power * (self.start_widget_rect.x - new_x))
                            resize_y = self.start_widget_rect.height + (
                                        self.resize_power * (self.start_widget_rect.y - new_y))
                        elif self.resize_power == -1:
                            new_x = widget.get_allocation().x
                            new_y = widget.get_allocation().y
                            resize_x = (x - widget.get_allocation().x) + self.start_widget_rect.width + (self.border_size * 3)
                            resize_y = (y - widget.get_allocation().y) + self.start_widget_rect.height + (self.border_size * 3)
                        widget.set_size_request(resize_x, resize_y)
                        self.fixed.widget_array[self.wid_id][3] = resize_x - self.border_size - self.border_size
                        self.fixed.widget_array[self.wid_id][4] = resize_y - self.border_size - self.border_size
                        self.the_child.set_size_request(self.the_child.min_w, self.the_child.min_h)
                    self.px = new_x
                    self.py = new_y
                    self.fixed.move(widget, new_x, new_y)
                    if new_x < self.fixed.get_parent().get_hadjustment().get_value() and self.fixed.get_parent().get_hadjustment().get_value() != 0:
                        self.fixed.get_parent().get_hadjustment().set_value(widget.get_allocation().x)
                    elif new_x > (self.fixed.get_parent().get_hadjustment().get_value() + self.fixed.get_parent().get_allocation().width - widget.get_allocation().width):
                        self.fixed.get_parent().get_hadjustment().set_value(widget.get_allocation().x - self.fixed.get_parent().get_allocation().width + self.fixed.get_parent().get_hadjustment().get_value() + widget.get_allocation().width)
                    if self.px + widget.get_allocation().width >= self.fixed.get_allocation().width - 10:
                        size = self.fixed.get_allocation().width + 10
                        self.fixed.set_size_request(size, self.fixed.get_parent().get_allocation().height)
                    self.fixed.widget_array[self.wid_id][1] = new_x
                    self.fixed.widget_array[self.wid_id][2] = new_y

    def str2bool(self, str):
        fstr = str.lower()
        if fstr == "true":
            return True
        if fstr == "false":
            return False
        return "ERROR: value is " + str

    def Min(self, a, b):
        if b < a:
            return b
        return a

    def Max(self, a, b):
        if b > a:
            return b
        return a

    def RoundDownToMultiple(self, i, m):
        return i / m * m

    def RoundToNearestMultiple(self, i, m):
        if i % m >= m / 2:
            return ((i + m - 1) // m) * m
        return (i // m) * m

    def EditWidget(self, widget):
        widget_folder = os.path.join("./widgets", widget[0][0])
        args_path = os.path.join(widget_folder, "args")

        if os.path.exists(args_path):
            with open(args_path, "r") as args_file:
                args_definition = args_file.read()
            args_dialog = ArgsDialog(args_definition)
            response = args_dialog.run()

            if response == Gtk.ResponseType.OK:
                args = args_dialog.get_args()
                self.RemoveWid()
                self.fixed.add_widget([widget[0][0], *args], widget[1], widget[2], widget[3], widget[4])  # Modified this line
            args_dialog.destroy()
        else:
            self.RemoveWid()
            self.fixed.add_widget([widget[0][0]], widget[1], widget[2], widget[3], widget[4])

class ArgsDialog(Gtk.Dialog):
    def __init__(self, args_definition):
        Gtk.Dialog.__init__(self, "Set Arguments", None, Gtk.DialogFlags.MODAL)
        self.set_default_size(300, 150)

        box = self.get_content_area()
        self.args_entry = Gtk.TextView()
        box.add(self.args_entry)
        self.args_entry.get_buffer().set_text(args_definition)

        self.add_button("Cancel", Gtk.ResponseType.CANCEL)
        self.add_button("OK", Gtk.ResponseType.OK)

        self.show_all()

    def get_args(self):
        buffer:Gtk.TextBuffer = self.args_entry.get_buffer()
        startIter, endIter = buffer.get_bounds()
        args_text = buffer.get_text(startIter, endIter, True)
        args_list = args_text.splitlines()
        args = [arg.strip() for arg in args_list if arg.strip()]
        return args

class WidgetEngine(Gtk.Fixed):
    def __init__(self):
        super().__init__()
        viewport = Gtk.Viewport()
        self.set_has_window(1)
        self.add(viewport)
        self.widget_array = []

    def add_widget(self, eval, x, y, width, height):
        class_name = eval[0]
        class_params = eval[1:]
        widget_class = self.load_widget_class(class_name)
        wid = widget_class(*class_params)
        wid.resizeWidget(width, height)
        wid.show_all()
        widc = ItemContainer(wid, self, len(self.widget_array))
        self.widget_array.append([eval, x, y, width, height])
        self.put(widc, x, y)
        self.show_all()

    def load_widget_class(self, class_name):
        module = importlib.import_module("widgets." + class_name)
        class_obj = getattr(module, "WidgetEngine_MainCall")
        return class_obj


class ImageRenderer(Gtk.EventBox):
    def __init__(self, image, PARENT, pixbuf):
        super().__init__()
        self.set_name(image)
        self.PARENT = PARENT
        self.image = Gtk.Image()
        self.image.set_from_pixbuf(pixbuf)
        self.add(self.image)

    def Exit(self):
        parent = self.PARENT.RENDERER
        if parent.timer_id is not None:
            GLib.source_remove(parent.timer_id)
        parent.timer_id = None
        parent.WallpaperTimer()
        parent.InitTimer()


class WebRenderer(Gtk.Bin):
    def __init__(self, PARENT, weblink):
        super().__init__()

        self.PARENT = PARENT

        self.set_name(weblink)
        self.PARENT = PARENT.RENDERER
        context = WebKit2.WebContext.new_ephemeral()
        allow_http = self.PARENT.current_config["allow_unsecure"]
        print("nebide:", "WebKit https only mode is disabled? " + str(allow_http))
        if allow_http == True:
            context.set_tls_errors_policy(WebKit2.TLSErrorsPolicy.IGNORE)
        self.WebKitView = WebKit2.WebView.new_with_context(context)
        self.WebKitView.set_margin_top(38)
        self.add(self.WebKitView)
        self.WebKitView.load_uri(weblink)

        next_action = Gio.SimpleAction.new("next_wallpaper", None)
        next_action.connect("activate", self.Exit_Action)
        self.next_option = WebKit2.ContextMenuItem.new_from_gaction(next_action, _("Next Wallpaper"))

        prop_action = Gio.SimpleAction.new("props", None)
        prop_action.connect("activate", self.PropAction)
        self.prop_option = WebKit2.ContextMenuItem.new_from_gaction(prop_action, _("Properties"))

        show_action = Gio.SimpleAction.new("show_widgets", None)
        show_action.connect("activate", self.ShowWidgetsAction)
        self.show_option = WebKit2.ContextMenuItem.new_from_gaction(show_action, _("Show/Hide Widgets"))

        self.WebKitView.connect('context-menu', self.on_menu)

    def on_menu(self, webview, context_menu, hit_result_event, event):
        context_menu.append(WebKit2.ContextMenuItem().new_separator())
        context_menu.append(self.next_option)
        context_menu.append(self.prop_option)
        context_menu.append(WebKit2.ContextMenuItem().new_separator())
        context_menu.append(self.show_option)


    def Exit(self):
        parent = self.PARENT
        parent.WallpaperTimer()
        parent.InitTimer()

    def Exit_Action(self, action, parameter):
        self.Exit()

    def ShowWidgetsAction(self, action, parameter):
        self.PARENT.PARENT.SetWidgetsVisible(True)

    def PropAction(self, action, parameter):
        self.PARENT.PARENT.Properties(self.PARENT.PARENT)

class VideoRenderer(Gtk.EventBox):
    def __init__(self, PARENT, vfile):
        super().__init__()
        self.set_name(vfile)
        self.pixbuf = None
        self.PARENT = PARENT.RENDERER
        self.fps_from_conf = self.PARENT.current_config["videorender_speed"] * 25
        self.vfile = vfile
        self.timer_id = None
        self.image = Gtk.Image()
        self.add(self.image)

    def Exit(self):
        self.Stop()
        parent = self.PARENT
        parent.WallpaperTimer()
        parent.InitTimer()

    def start(self):
        cv2.destroyAllWindows()
        self.capture = cv2.VideoCapture(self.vfile)
        self.timer_id = GLib.timeout_add(1000 / self.fps_from_conf, self.update_image)

    def Stop(self):
        if self.timer_id is not None:
            GLib.source_remove(self.timer_id)
            del self.capture
            self.timer_id = None

    def update_image(self):
        ret, frame = self.capture.read()
        if ret:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            parent = self.PARENT
            self.c_width, self.c_height = parent.c_width, parent.c_height
            # frame_height, frame_width, _ = frame.shape
            # ratio = frame_width / self.c_width
            self.set_from_numpy_array(cv2.resize(frame, (self.c_width, self.c_height)))
            return True
        else:
            GLib.source_remove(self.timer_id)
            self.capture.release()
            del self.capture
            parent = self.PARENT
            parent.WallpaperTimer()
            parent.InitTimer()
            return False

    def set_from_numpy_array(self, image):
        height, width, channels = image.shape
        if self.pixbuf is None or self.pixbuf.get_width() != width or self.pixbuf.get_height() != height:
            self.pixbuf = GdkPixbuf.Pixbuf.new_from_data(image.tobytes(), GdkPixbuf.Colorspace.RGB, False, 8, width,
                                                         height, width * channels, None, None)
        else:
            self.pixbuf.fill(0xff0000)
            self.pixbuf = self.pixbuf.new_from_data(image.tobytes(), GdkPixbuf.Colorspace.RGB, False, 8, width,
                                                    height, width * channels, None, None)
        if self.c_width != width and self.c_height != height:
            parent = self.PARENT
            self.pixbuf = parent.GenerateRenderedImage(self.pixbuf, self.c_width, self.c_height,
                                                       parent.current_config["slideshow_rendermode"])
        self.image.set_from_pixbuf(self.pixbuf)

class WallpaperRenderer(Gtk.Bin):
    def __init__(self, PARENT):
        super().__init__()

        self.PARENT = PARENT
        self.rendermode = "Tile"
        self.timer_id = None

        self.c_width = 10
        self.c_height = 10
        self.stack = Gtk.Stack()

        self.stack.set_transition_type(Gtk.StackTransitionType.CROSSFADE)

        self.add(self.stack)
        self.show_all()

    def GenerateRenderedImage(self, pixbuf, width, height, mode):
        if mode == "Scale":
            return pixbuf.scale_simple(width, height, GdkPixbuf.InterpType.BILINEAR)
        if mode == "Center":
            original_width = pixbuf.get_width()
            original_height = pixbuf.get_height()
            calculated_x = (original_width / 2) - (width / 2)
            calculated_y = (original_height / 2) - (height / 2)
            return pixbuf.new_subpixbuf(calculated_x, calculated_y, width, height)
        if mode == "Fill":
            original_width = pixbuf.get_width()
            original_height = pixbuf.get_height()
            aspect_ratio = original_width / original_height
            if width / height > aspect_ratio:
                new_width = width
                new_height = int(width / aspect_ratio)
                y_offset = (new_height - height) / 2
                return pixbuf.scale_simple(new_width, new_height, GdkPixbuf.InterpType.BILINEAR).new_subpixbuf(0,
                                                                                                               y_offset,
                                                                                                               width,
                                                                                                               height)
            else:
                new_width = int(height * aspect_ratio)
                new_height = height
                x_offset = (new_width / 2) - (width / 2)
                return pixbuf.scale_simple(new_width, new_height, GdkPixbuf.InterpType.BILINEAR).new_subpixbuf(x_offset,
                                                                                                               0,
                                                                                                               width,
                                                                                                               height)
        if mode == "Max":
            original_width = pixbuf.get_width()
            original_height = pixbuf.get_height()
            aspect_ratio = original_width / original_height
            if width / height > aspect_ratio:
                new_width = int(height * aspect_ratio)
                new_height = height
                x_offset = (width - new_width) / 2
                updated_pixbuf = pixbuf.scale_simple(new_width, new_height, GdkPixbuf.InterpType.BILINEAR)
                processed_img = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, width, height)
                updated_pixbuf.copy_area(0, 0, updated_pixbuf.get_width(), updated_pixbuf.get_height(),
                                         processed_img, x_offset, 0)

                return processed_img
            else:
                new_width = width
                new_height = int(width / aspect_ratio)
                y_offset = (height - new_height) / 2
                updated_pixbuf = pixbuf.scale_simple(new_width, new_height, GdkPixbuf.InterpType.BILINEAR)
                processed_img = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, width, height)
                updated_pixbuf.copy_area(0, 0, updated_pixbuf.get_width(), updated_pixbuf.get_height(),
                                         processed_img, 0, y_offset)

                return processed_img
        if mode == "Tile":
            original_width = pixbuf.get_width()
            original_height = pixbuf.get_height()
            renderarea_width = 0
            renderarea_height = 0

            while renderarea_width < width:
                renderarea_width += original_width
            while renderarea_height < height:
                renderarea_height += original_height

            canvas_pixbuf = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, renderarea_width, renderarea_height)

            if renderarea_width == 0 and renderarea_height == 0:
                return pixbuf.new_subpixbuf(0, 0, width, height)
            else:
                for y in range(0, 720, original_width):
                    for x in range(0, 1280, original_height):
                        pixbuf.copy_area(
                            0, 0, pixbuf.get_width(), pixbuf.get_height(),
                            canvas_pixbuf, x, y
                        )

                return canvas_pixbuf.new_subpixbuf(0, 0, width, height)

    def UpdateRender(self, parent, allocation):
        childs = self.stack.get_children()
        self.c_width = allocation.width
        self.c_height = allocation.height
        for child in childs:
            if child != None:
                childpage = child.get_name()
                if isinstance(child, ImageRenderer):
                    if str(childpage).startswith("Init") == False:
                        child.image.set_from_pixbuf(
                            self.GenerateRenderedImage(GdkPixbuf.Pixbuf.new_from_file(childpage), self.c_width,
                                                       self.c_height, self.rendermode))
                elif isinstance(child, VideoRenderer) or isinstance(child, WebRenderer):
                    child.set_size_request(self.c_width, self.c_height)

    def ShuffleWallpapers(self):
        random.shuffle(self.current_wallpapers)
        self.UpdateConfig(self.current_wallpapers, self.current_config)
        return False

    def WallpaperTimer(self):
        self.wallpaper_index = (self.wallpaper_index + 1) % len(self.stack.get_children())
        if self.wallpaper_index == 0:
            self.wallpaper_index = 1
        self.stack.set_visible_child(self.stack.get_children()[self.wallpaper_index])
        gc.collect()
        if str(self.stack.get_children()[self.wallpaper_index].get_name()) == "Init2":
            GLib.timeout_add(self.current_config["transition_duration_msec"] * 2, self.ShuffleWallpapers)
            return False
        elif isinstance(self.stack.get_children()[self.wallpaper_index], VideoRenderer) \
                or isinstance(self.stack.get_children()[self.wallpaper_index], WebRenderer):
            if isinstance(self.stack.get_children()[self.wallpaper_index], VideoRenderer):
                self.stack.get_children()[self.wallpaper_index].start()
            self.timer_id = None
            return False
        else:
            user_home = os.path.expanduser('~')
            wall_path = self.stack.get_children()[self.wallpaper_index].get_name()
            with open(f'{user_home}/.current_wallpaper', 'w') as file:
               file.write(wall_path)
               print("nebide: saved current_wallpaper", wall_path)
            return True

    def InitTimer(self):
        if self.timer_id != None:
            GLib.source_remove(self.timer_id)
            self.timer_id = None
        if isinstance(self.stack.get_visible_child(), VideoRenderer) == False \
                and isinstance(self.stack.get_visible_child(), WebRenderer) == False:
            self.timer_id = GLib.timeout_add_seconds(self.current_config["slideshow_duration_sec"], self.WallpaperTimer)

    def RunWallpaper(self):
        self.wallpaper_index = 0
        self.stack.show_all()
        self.WallpaperTimer()
        if len(self.current_wallpapers) != 1:
            if isinstance(self.stack.get_children()[self.wallpaper_index], VideoRenderer) == False \
                    and isinstance(self.stack.get_children()[self.wallpaper_index], WebRenderer) == False:
                self.InitTimer()

    def UpdateConfig(self, wallpapers, config):
        if self.timer_id != None:
            GLib.source_remove(self.timer_id)
            self.timer_id = None

        self.current_wallpapers = wallpapers
        self.current_config = config

        self.rendermode = self.current_config["slideshow_rendermode"]
        self.stack.set_transition_duration(self.current_config["transition_duration_msec"])

        for page in self.stack.get_children():
            if isinstance(page, WebRenderer):
                page.WebKitView.destroy()
            if isinstance(page, VideoRenderer) == True:
                page.Stop()

            self.stack.remove(page)

        TempImg = Gtk.Image()
        TempImg.set_name("Init")
        self.stack.add_named(TempImg, "Init")

        for image in self.current_wallpapers:
            is_image = False
            try:
                GdkPixbuf.Pixbuf.new_from_file(image)
                is_image = True
            except:
                pass

            if is_image:
                self.stack.add_named(ImageRenderer(image, self.PARENT,
                                                   self.GenerateRenderedImage(GdkPixbuf.Pixbuf.new_from_file(image),
                                                                              self.c_width, self.c_height,
                                                                              self.rendermode)), image)
            elif self.IsWebsiteLink(image):
                self.stack.add_named(WebRenderer(self.PARENT, image), image)
            elif self.IsVideoSupported(image):
                self.stack.add_named(VideoRenderer(self.PARENT, image), image)
            else:
                self.current_wallpapers.remove(image)

        if self.current_config["slideshow_shuffle"] == True:
            TempImg2 = Gtk.Image()
            TempImg2.set_name("Init2")
            TempImg2.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0, 0, 0, 1.0))
            self.stack.add_named(TempImg2, "Init2")

        self.RunWallpaper()

    def IsWebsiteLink(self, string):
        if string.startswith("https://"):
            return True
        elif string.startswith("http://"):
            if self.current_config["allow_unsecure"] == True:
                return True
            else:
                return False
        else:
            return False

    def IsVideoSupported(self, file_path):
        capture = cv2.VideoCapture(file_path)
        if capture == None:
            del capture
            return False
        else:
            capture.release()
            del capture
            return True

class WallpaperEngine(Gtk.Window):
    def UpdateRenderers(self, window, allocation):
        if self.c_width != allocation.width and self.c_height != allocation.height:
            monitor = screen.get_monitor_geometry(self.monitor_num)
            print("xorg:", "Screen resolution is updated to " + str(monitor.width) + "x" + str(monitor.height) + ".")
            print("nebide:", "X11 screen resolution seems to be changed. Updating render area from " + str(self.c_width)
                  + "x" + str(self.c_height) + " to " + str(allocation.width) + "x" + str(allocation.height) + ".")
            self.c_width = self.RENDERER.c_width
            self.c_height = self.RENDERER.c_height
            self.resize(monitor.width, monitor.height)
            self.move(monitor.x, monitor.y)
            self.RENDERER.UpdateRender(window, allocation)
            if self.init_conf["enable_widgetengine"] == True:
                self.wideng_scroll.set_size_request(allocation.width, allocation.height)

    def on_delete_event(self, widget, event):
        self.destroy()

    def create_bg_context_menu(self):
        context_menu = Gtk.Menu()

        next_wallpaper_item = Gtk.MenuItem(label=_("Next Wallpaper"))
        properties_item = Gtk.MenuItem(label=_("Properties"))

        context_menu.append(next_wallpaper_item)
        context_menu.append(properties_item)

        next_wallpaper_item.connect("activate", lambda _: self.RENDERER.stack.get_visible_child().Exit())
        properties_item.connect("activate", lambda _: self.Properties(self))

        if self.init_conf["enable_widgetengine"] == True:
           hide_item = Gtk.MenuItem(label=_("Show/Hide Widgets"))
           add_item = Gtk.MenuItem(label=_("Add Widgets"))
           edit_item = Gtk.MenuItem(label=_("On/Off Widget Edit Mode"))
           context_menu.append(hide_item)
           context_menu.append(add_item)
           context_menu.append(edit_item)
           hide_item.connect("activate", lambda _: self.SetWidgetsVisible())
           add_item.connect("activate", lambda _: self.AddWidgetPlacer())
           edit_item.connect("activate", lambda _: self.SetEditMode())

        context_menu.show_all()

        return context_menu

    def on_widget_engine_sb_button_press(self, widget, event):
        global IS_A_WID_FOCUSED
        print("on_widget_engine_sb_button_press", str(IS_A_WID_FOCUSED))
        if IS_A_WID_FOCUSED == False:
            if event.button == 3:
                self.context_menu.popup(None, None, None, None, event.button, event.time)

    def __init__(self, monitor_num):
        Gtk.Window.__init__(self, title="Desktop")
        
        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        self.set_opacity(0.0)
        if visual and screen.is_composited():
            print("nebide:", "is_composited")
            self.set_visual(visual)
            self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0, 0, 0, 0.0))
        
        print("nebide:", "Engine initlaized at monitor ID-" + str(monitor_num))
        self.c_width = 0
        self.c_height = 0
        self.set_wmclass("NebiDE_WE", "NebiDE_WE")

        self.monitor_num = monitor_num
        monitor = screen.get_monitor_geometry(self.monitor_num)
        self.move(monitor.x, monitor.y)

        scroll = Gtk.ScrolledWindow()
        scroll.set_border_width(0)
        scroll.set_shadow_type(Gtk.ShadowType.NONE)
        scroll.set_can_focus(False)
        viewport = Gtk.Viewport()
        viewport.set_border_width(0)
        viewport.set_shadow_type(Gtk.ShadowType.NONE)
        fixed = Gtk.Fixed()
        viewport.add(fixed)
        scroll.add(viewport)
        self.add(scroll)

        self.RENDERER = WallpaperRenderer(self)
        fixed.put(self.RENDERER, 0, 0)

        self.FixConfig()
        self.LoadConfig()

        GLib.timeout_add(int(self.init_conf["transition_duration_msec"] / 3), self.StartRenderer)
        self.context_menu = self.create_bg_context_menu()
        self.RENDERER.connect("button-press-event", self.on_widget_engine_sb_button_press)

        if self.init_conf["enable_widgetengine"] == True:
            self.wideng_scroll = Gtk.ScrolledWindow()
            self.wideng_scroll.set_border_width(0)
            self.wideng_scroll.set_shadow_type(Gtk.ShadowType.NONE)
            wideng_viewport = Gtk.Viewport()
            wideng_viewport.set_border_width(0)
            wideng_viewport.set_shadow_type(Gtk.ShadowType.NONE)
            self.WID_ENGINE = WidgetEngine()
            wideng_viewport.add(self.WID_ENGINE)
            self.wideng_scroll.add(wideng_viewport)
            fixed.put(self.wideng_scroll, 0, 0)
            self.wideng_scroll.connect("button-press-event", self.on_widget_engine_sb_button_press)
            GLib.timeout_add_seconds(5, self.AddWidgets)

        self.connect("configure-event", self.UpdateRenderers)
        self.set_type_hint(Gdk.WindowTypeHint.DESKTOP)
        self.show_all()
        self.set_keep_below(True)

    def FixConfig(self):
        if os.path.isfile(WE_CONF_PATH):
            print("nebide:", "Config exists. Checking config version...")
            conf = configparser.ConfigParser()
            conf.read(WE_CONF_PATH)
            nebios2_var_wallpaper = None
            nebios2_var_wallpaper_method = "--bg-fill"
            try:
                nebios2_var_wallpaper = conf.get('General', 'wallpaper')
                nebios2_var_wallpaper_method = conf.get('General', 'wallpaper_method')
            except:
                pass
            if nebios2_var_wallpaper != None or nebios2_var_wallpaper_method != "--bg-fill":
                print("nebide:", "Config from NebiDE ver. 1.x loaded. Needs to upgrade to this version of NebiDE.")
                wallpaper_mode = nebios2_var_wallpaper_method.replace("--bg-", "").capitalize()
                conf = configparser.ConfigParser()
                conf["General"] = {
                    "config_version": NEBIDE_WE_VER
                }
                conf["WallpaperEngine"] = {
                    "enable_widgetengine": True
                }
                conf["WallpaperEngine.WallpaperRenderer"] = {
                    "wallpapers": [nebios2_var_wallpaper, ],
                    "transition_style": "Crossfade",
                    "transition_duration_msec": 500,
                    "slideshow_duration_sec": 60,
                    "slideshow_shuffle": False,
                    "slideshow_rendermode": wallpaper_mode
                }
                conf["WallpaperEngine.VideoRenderer"] = {
                    "videorender_speed": 1
                }
                conf["WallpaperEngine.WebKit"] = {
                    "allow_unsecure": False
                }
                with open(WE_CONF_PATH, 'w') as configfile:
                    conf.write(configfile)
        else:
            print("nebide:", "Config not exists. Generating...")
            conf = configparser.ConfigParser()
            conf["General"] = {
                "config_version": NEBIDE_WE_VER
            }
            conf["WallpaperEngine"] = {
                "enable_widgetengine": True
            }
            conf["WallpaperEngine.WallpaperRenderer"] = {
                "wallpapers": ["/System/Wallpapers/Image/Galata (Day) by Ahmet Polat.jpg",
                               "/System/Wallpapers/Image/Galata (Night) by Zafer Erdoğan.jpg"],
                "transition_style": "Crossfade",
                "transition_duration_msec": 500,
                "slideshow_duration_sec": 60,
                "slideshow_shuffle": False,
                "slideshow_rendermode": "Fill"
            }
            conf["WallpaperEngine.VideoRenderer"] = {
                "videorender_speed": 1
            }
            conf["WallpaperEngine.WebKit"] = {
                "allow_unsecure": False
            }
            with open(WE_CONF_PATH, 'w') as configfile:
                conf.write(configfile)

    def LoadConfig(self):
        conf = configparser.ConfigParser()
        conf.read(WE_CONF_PATH)
        self.init_wall = conf.get('WallpaperEngine.WallpaperRenderer', 'wallpapers')[1:-1].split(", ")
        for i in range(len(self.init_wall)):
            wpath = str(self.init_wall[i])[1:-1]
            self.init_wall[i] = wpath
        self.init_conf = {
            "enable_widgetengine": conf.getboolean('WallpaperEngine', 'enable_widgetengine'),
            "transition_style": conf.get('WallpaperEngine.WallpaperRenderer', 'transition_style'),
            "transition_duration_msec": conf.getint('WallpaperEngine.WallpaperRenderer', 'transition_duration_msec'),
            "slideshow_duration_sec": conf.getint('WallpaperEngine.WallpaperRenderer', 'slideshow_duration_sec'),
            "slideshow_shuffle": conf.getboolean('WallpaperEngine.WallpaperRenderer', 'slideshow_shuffle'),
            "slideshow_rendermode": conf.get('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode'),
            "videorender_speed": conf.getfloat('WallpaperEngine.VideoRenderer', 'videorender_speed'),
            "allow_unsecure": conf.getboolean('WallpaperEngine.WebKit', 'allow_unsecure'),
        }

    def StartRenderer(self):
        # This is the array that contains photos and videos.
        self.RENDERER.UpdateConfig(
            self.init_wall,
            self.init_conf
        )
        self.set_opacity(1.0)
        GLib.timeout_add(self.init_conf["transition_duration_msec"], lambda: self.override_background_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(0, 0, 0, 1.0)))
        

    def Properties(self, parent):
        if __name__ == '__main__':
            WallpaperManager.init(parent)

    def SetEditMode(self):
        global PRIMARY_EDIT_MODE

        if PRIMARY_EDIT_MODE == True:
            for child in self.WID_ENGINE.get_children():
                if hasattr(child, "the_child"):
                    if hasattr(child.the_child, "RemoveOnExitEdit"):
                        if child.the_child.RemoveOnExitEdit == True:
                            child.RemoveWid()

        PRIMARY_EDIT_MODE = not PRIMARY_EDIT_MODE
        if PRIMARY_EDIT_MODE == False:
            self.SaveWidgets()

    def SaveWidgets(self):
        with open(WE_WID_CONF_PATH, 'w') as json_file:
            json.dump(self.WID_ENGINE.widget_array, json_file, indent=4)

    def AddWidgetPlacer(self):
        if self.init_conf["enable_widgetengine"] == True:
            global PRIMARY_EDIT_MODE
            PRIMARY_EDIT_MODE = False
            self.WID_ENGINE.add_widget(["Add_Widgets", self], 0, 40, 360, 360)
            self.SetEditMode()
            edit_on_notification_title = _("Add Widgets")
            edit_on_notification_message = _("Widget Edit Mode is On. Don't forget to close to save your widgets.")
            edit_on_command = f"notify-send \"{edit_on_notification_title}\" \"{edit_on_notification_message}\""
            os.system(edit_on_command)

    def SetWidgetsVisible(self, bool=None):
        if bool == None:
            bool = not self.wideng_scroll.is_visible()
        if self.init_conf["enable_widgetengine"] == True:
            self.wideng_scroll.set_visible(bool)

    def AddWidgets(self):
        if os.path.exists(WE_WID_CONF_PATH):
            with open(WE_WID_CONF_PATH, 'r') as json_file:
                data = json.load(json_file)
        else:
            data = [
                [['Clock', ], 0, 40, 1, 1]
            ]

        for entry in data:
            self.WID_ENGINE.add_widget(entry[0], entry[1], entry[2], entry[3], entry[4])


if __name__ == '__main__':
    screen = Gdk.Screen.get_default()
    n_monitors = screen.get_n_monitors()

    css_provider = Gtk.CssProvider()
    css_provider.load_from_path('ui/app.css')

    engines = []
    for monitor_num in range(n_monitors):
        instance = WallpaperEngine(monitor_num)
        style_context = instance.get_style_context()
        style_context.add_provider_for_screen(screen, css_provider,
                                              Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        instance.connect("delete-event", instance.on_delete_event)
        engines.append(instance)

    Gtk.main()
