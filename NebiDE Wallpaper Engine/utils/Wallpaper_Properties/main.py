#!/usr/bin/env python3
import gettext
import configparser
import os
import cv2
import shutil
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GdkPixbuf, WebKit2
from PIL import Image

translations = gettext.translation('we_TEMPLATE', localedir='locale', fallback=True, languages=['en_US', 'tr_TR'])
translations.install()

WALL_GEN_CSS = False
WALL_THUMB_CACHE_CSS = ""
VID_THUMB_CACHE = {}

class WallpaperStoreView(WebKit2.WebView):
    def __init__(self):
        super().__init__()
        self.connect("decide-policy", self.on_decide_policy, None)

    def on_decide_policy(self, webview, decision, decision_type, user_data):
        if decision_type == WebKit2.PolicyDecisionType.NAVIGATION_ACTION:
            # Handle navigation requests here (e.g., opening links).
            return False
        elif decision_type == WebKit2.PolicyDecisionType.RESPONSE:
            response = decision.get_response()
            if response.get_mime_type() == "application/octet-stream":
                # Handle download requests here.
                download = decision.get_download()
                self.downloadRequested(download)
                return True

    def downloadRequested(self, download):
        download.connect("received-data", self.whenDownloading)
        download.connect("created-destination", self.setPath)
        download.connect("finished", self.moveToPath)
        download.connect("failed", self.error)

    def moveToPath(self, download):
        shutil.move(self.oldDest, self.dest)
        os.system("chmod +x " + self.dest)

    def setPath(self, download, path):
        filename = str(path).split("/")[-1]
        self.oldDest = str(path).replace("file://", "")
        print(self.oldDest)
        self.dest = "/System/Wallpapers/" + self.getWallpaperType(filename.split(".")[0]) + "/" + filename
        print(self.dest)

    def getWallpaperType(self, ext):
        print(ext)
        if \
        ext.lower() == "mpeg"  or \
        ext.lower() == "mp4" or \
        ext.lower() == "mov":
            return "Live"
        else:
            return "Image"

    def whenDownloading(self, download, length):
        percentProg = download.props.estimated_progress * 100
        percentProgInt = int(percentProg)
        dwUri = download.get_request().get_uri()
        appnameList = str(dwUri).split("/")[-1].split(".")[:-1]
        appname = ''.join(str("." + e) for e in appnameList)
        self.appname = appname[1:]
        self.appname = self.appname.replace("_", " ")
        status = "Downloading " + self.appname + " (" + str(percentProgInt) + "%)"
        print(status)

class WallpaperProperties(Gtk.Application):
    def __init__(self, parent=None):
        super().__init__()

        self.PARENT = parent

        self.builder = Gtk.Builder()
        self.builder.add_from_file(PREFIX + "ui/we_settings.glade")
        self.window: Gtk.Window = self.builder.get_object('prop_win')
        self.window.set_title(_("Wallpaper Properties"))

        self.conf = configparser.ConfigParser()
        self.conf.read(WE_CONF_PATH)

        btn_apply: Gtk.Button = self.builder.get_object('btn_apply')
        btn_apply.connect("clicked", lambda _: self.Save())

        spinbtn_pagesec: Gtk.SpinButton = self.builder.get_object('spinbtn_slideshow_pagesec')
        spinbtn_pagesec.set_value(self.conf.getint('WallpaperEngine.WallpaperRenderer', 'slideshow_duration_sec'))
        spinbtn_pagesec.connect("value-changed", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_duration_sec',
                          str(spinbtn_pagesec.get_value()).split(".")[0]))

        spinbtn_transmsec: Gtk.SpinButton = self.builder.get_object('spinbtn_slideshow_transmsec')
        spinbtn_transmsec.set_value(self.conf.getint('WallpaperEngine.WallpaperRenderer', 'transition_duration_msec'))
        spinbtn_transmsec.connect("value-changed", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'transition_duration_msec',
                          str(spinbtn_transmsec.get_value()).split(".")[0]))

        self.switchbtn_shuffle: Gtk.Switch = self.builder.get_object('switchbtn_slideshow_shuffle')
        self.switchbtn_shuffle.set_state(self.conf.getboolean('WallpaperEngine.WallpaperRenderer', 'slideshow_shuffle'))

        self.switchbtn_widget_enable: Gtk.Switch = self.builder.get_object('switchbtn_widget_enable')
        self.switchbtn_widget_enable.set_state(self.conf.getboolean('WallpaperEngine', 'enable_widgetengine'))

        self.switchbtn_httpsonly_enable: Gtk.Switch = self.builder.get_object('switchbtn_httpsonly_enable')
        self.switchbtn_httpsonly_enable.set_state(not self.conf.getboolean('WallpaperEngine.WebKit', 'allow_unsecure'))

        wallpaper_store_container: Gtk.Box = self.builder.get_object('wallpaper_store_container')
        self.wallpaper_store_webkit = WallpaperStoreView()
        wallpaper_store_container.pack_start(self.wallpaper_store_webkit, True, True, 0)
        self.wallpaper_store_webkit.load_uri("https://wallpaperstore.nebisoftware.com/")
        wallpaper_store_container.show_all()

        rbw = {
            "Scale": self.builder.get_object('radiobtn_wallpaper_scale'),
            "Center": self.builder.get_object('radiobtn_wallpaper_center'),
            "Fill": self.builder.get_object('radiobtn_wallpaper_fill'),
            "Max": self.builder.get_object('radiobtn_wallpaper_max'),
            "Tile": self.builder.get_object('radiobtn_wallpaper_tile')
        }

        rbw[self.conf.get('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode')].set_active(True)

        rbw["Scale"].connect("toggled", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode', "Scale"))
        rbw["Center"].connect("toggled", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode', "Center"))
        rbw["Fill"].connect("toggled", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode', "Fill"))
        rbw["Max"].connect("toggled", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode', "Max"))
        rbw["Tile"].connect("toggled", lambda _: \
            self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_rendermode', "Tile"))

        self.slideshow_items: Gtk.Box = self.builder.get_object('slideshow_items')

        self.walls = self.conf.get('WallpaperEngine.WallpaperRenderer', 'wallpapers')[1:-1].split(", ")

        for i in range(len(self.walls)):
            wpath = str(self.walls[i])[1:-1]
            self.walls[i] = wpath

        self.window.show()

        self.UpdateSlideshowList()

    def UpdateSlideshowList(self):
        self.clear_slideshow_items()
        self.generate_widget_css()
        widget_css_provider = Gtk.CssProvider()
        widget_css_provider.load_from_data(self.widget_css_provider_data.encode())
        widget_context = self.slideshow_items.get_style_context()
        widget_context.add_provider_for_screen(Gdk.Screen.get_default(), widget_css_provider,
                                               Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        for i, wall in enumerate(self.walls):
            self.AddItemsToSlideshowList(i, wall)

        self.AddNewItemButton(len(self.walls) + 1)

        self.slideshow_items.show_all()

    def clear_slideshow_items(self):
        for child in self.slideshow_items.get_children():
            child.destroy()
            self.slideshow_items.remove(child)

    def generate_widget_css(self):
        self.widget_css_provider_data = """
    		    .type {
		        color: rgba(255, 255, 255, 0.5);
		        font-weight: 500;
		        font-size: 16px;
		        text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
		    }

		    .file {
		        font-weight: 800;
		        color: white;
		        font-size: 40px;
		        text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.5);
		    }
		    
		    .wbg, .wtbg {
                background-size: cover;
                background-position: center;
                background-color: #9b59b6;
		    }
    	"""
        for i, wall in enumerate(self.walls):
            img_color = "#6BCDCE" if self.IsWebsiteLink(wall) else "#9b59b6"
            self.widget_css_provider_data += """
		    .wbg-{i} {{
			background-image: url('file://{wall}');
			background-color: {color};
    		}}""".format(i=i, wall=wall, color=img_color)

    def AddNewItemButton(self, id):
        wallbtn = Gtk.Button()
        wallbtn.set_size_request(64, -1)
        wallbtn.set_label("+")
        wallbtn.connect("clicked", lambda _: self.EditWall(id))
        self.slideshow_items.add(wallbtn)

    def AddItemsToSlideshowList(self, item_num, theitem):
        wallbtn = Gtk.Button()
        wallbtn.set_size_request(900 - 40 - 40, -1)
        wallbtn.get_style_context().add_class("wbg")
        wallbtn.get_style_context().add_class("wbg-" + str(item_num))

        wallinfobox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        typelabel = Gtk.Label()
        typelabel.get_style_context().add_class("type")
        typelabel.set_justify(Gtk.Justification.LEFT)
        typelabel.set_line_wrap(Gtk.WrapMode.WORD)
        typelabel.set_alignment(0, 0)

        if self.IsWebsiteLink(theitem):
            typelabel.set_markup(_('Website as wallpaper'))
            namestr = theitem
        else:
            typelabel.set_markup(_('Image/Video as wallpaper'))
            namestr = os.path.basename(theitem).split(".", 1)[0]

        namelabel = Gtk.Label(namestr)
        namelabel.set_justify(Gtk.Justification.LEFT)
        namelabel.get_style_context().add_class("file")
        namelabel.set_line_wrap(Gtk.WrapMode.WORD)
        namelabel.set_alignment(0, 0)

        wallinfobox.pack_start(typelabel, False, False, 0)
        wallinfobox.set_margin_left(20)
        wallinfobox.set_margin_top(16)
        wallinfobox.pack_start(namelabel, False, False, 1)

        wallbtn.connect("clicked", lambda _: self.EditWall(item_num))
        wallbtn.add(wallinfobox)
        self.slideshow_items.add(wallbtn)

    def EditWall_Open_Click(self, widget):
        filechooser = Gtk.FileChooserDialog(
            _("Select a photo or video for wallpaper"),
            self.window,
            Gtk.FileChooserAction.OPEN,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        )

        # Set filters to restrict selection to image and video files
        image_filter = Gtk.FileFilter()
        image_filter.set_name("Image files")
        image_filter.add_mime_type("image/jpeg")
        image_filter.add_mime_type("image/png")
        filechooser.add_filter(image_filter)

        video_filter = Gtk.FileFilter()
        video_filter.set_name("Video files")
        video_filter.add_mime_type("video/mp4")
        video_filter.add_mime_type("video/quicktime")
        video_filter.add_mime_type("video/x-matroska")
        filechooser.add_filter(video_filter)

        response = filechooser.run()
        if response == Gtk.ResponseType.OK:
            selected_file = filechooser.get_filename()
            self.EditWall_Custom_ImageVid(selected_file)

        filechooser.destroy()
        self.edit_window.destroy()

    def EditWall(self, id):
        self.curr_id = id
        builder = Gtk.Builder()
        builder.add_from_file(PREFIX + "ui/we_settings.glade")
        self.edit_window: Gtk.Window = builder.get_object('wallsel_win')
        self.edit_window.set_title(_("Select Wallpaper"))

        self.edit_window.show()

        stack: Gtk.Stack = builder.get_object('wallsel_stack')
        stack.connect("notify::visible-child", self.EditWall_StackOnChild)

        self.EditWall_Open = builder.get_object('wallsel_custom')
        self.EditWall_Ok = builder.get_object('wallsel_ok')
        self.EditWall_rm = builder.get_object('wallsel_rm')
        self.EditWall_Ok.connect("clicked", lambda _: self.WallSel_ApplyURL())
        self.EditWall_Open.connect("clicked", self.EditWall_Open_Click)
        self.EditWall_rm.connect("clicked", lambda _: self.WallSel_PurgeID(id))

        self.wallpapers_box = builder.get_object('wallsel_wallpapers')
        self.videos_box = builder.get_object('wallsel_videos')
        self.EditWall_Url: Gtk.Entry = builder.get_object('wallsel_link')

        self.wgrid = Gtk.Grid()
        self.wgrid.set_column_spacing(10)
        self.wgrid.set_row_spacing(10)
        self.wallpapers_box.add(self.wgrid)

        self.vgrid = Gtk.Grid()
        self.vgrid.set_column_spacing(10)
        self.vgrid.set_row_spacing(10)
        self.videos_box.add(self.vgrid)

        self.edit_window.show_all()

        self.load_images_to_grid(id, "/System/Wallpapers/Image/")
        self.load_videos_to_grid(id, "/System/Wallpapers/Live/")

        self.edit_window.set_title(_("Select Wallpaper"))

    def WallSel_ApplyURL(self):
        buff: Gtk.TextBuffer = self.EditWall_Url.get_buffer()
        self.walls[self.curr_id] = buff.get_text()
        self.UpdateSlideshowList()

    def WallSel_PurgeID(self, id):
        self.walls.pop(id)
        self.edit_window.destroy()
        self.UpdateSlideshowList()

    def generate_image_thumbnail(self, folder_path, i):
        global WALL_THUMB_CACHE_CSS
        if not str(WALL_THUMB_CACHE_CSS).__contains__(".wtbg-" + str(i)):
            WALL_THUMB_CACHE_CSS += """
            .wtbg-{i} {{
                background-image: url('file://{wall}');
            }}
            """.format(i=i, wall=os.path.join(folder_path, self.w_filenames[i]))

    def load_images_to_grid(self, id, folder_path):
        global WALL_GEN_CSS
        global WALL_THUMB_CACHE_CSS

        row, col = 0, 0
        self.w_filenames = os.listdir(folder_path)

        for i in range(len(self.w_filenames)):
            img_path = os.path.join(folder_path, self.w_filenames[i])
            if WALL_GEN_CSS == False:
            	self.generate_image_thumbnail(folder_path, i)
            button = Gtk.Button()
            button.set_size_request(170, 106)
            button.get_style_context().add_class("wtbg")
            button.get_style_context().add_class("wtbg-" + str(i))
            button.set_name(img_path)
            button.connect("clicked", self.EditWall_Image_Func)
            self.wgrid.attach(button, col, row, 1, 1)

            col += 1
            if col > 2:  # You can adjust the number of columns as needed
                col = 0
                row += 1

            self.wgrid.show_all()
        
        if WALL_GEN_CSS == False:
                widget_css_provider = Gtk.CssProvider()
                widget_css_provider.load_from_data(WALL_THUMB_CACHE_CSS.encode())
                widget_context = self.wgrid.get_style_context()
                widget_context.add_provider_for_screen(Gdk.Screen.get_default(), widget_css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
                WALL_GEN_CSS = True

    def load_videos_to_grid(self, id, folder_path):
        row, col = 0, 0
        filenames = os.listdir(folder_path)

        for i in range(len(filenames)):
            img_path = os.path.join(folder_path, filenames[i])
            thumbnail = self.generate_video_thumbnail(img_path)

            image = Gtk.Image.new_from_pixbuf(thumbnail)  # Use new_from_pixbuf() instead of new_from_file()
            button = Gtk.Button()
            button.set_name(img_path)
            button.connect("clicked", self.EditWall_Image_Func)
            button.add(image)
            self.vgrid.attach(button, col, row, 1, 1)

            col += 1
            if col > 2:  # You can adjust the number of columns as needed
                col = 0
                row += 1

            self.vgrid.show_all()

    def EditWall_Image_Func(self, gbtn: Gtk.Button):
        if self.curr_id == len(self.walls) + 1:
            self.walls.append(gbtn.get_name())
        else:
            self.walls[self.curr_id] = gbtn.get_name()
        self.UpdateSlideshowList()

    def EditWall_Custom_ImageVid(self, file: str):
        if self.curr_id == len(self.walls) + 1:
            self.walls.append(file)
        else:
            self.walls[self.curr_id] = file
        self.UpdateSlideshowList()

    def generate_video_thumbnail(self, source_path):
        global VID_THUMB_CACHE

        if source_path in VID_THUMB_CACHE:
            return VID_THUMB_CACHE[source_path]

        cap = cv2.VideoCapture(source_path)
        ret, frame = cap.read()
        if ret:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = cv2.resize(frame, (180, 101))
            thumbnail = GdkPixbuf.Pixbuf.new_from_data(
                frame.tobytes(), GdkPixbuf.Colorspace.RGB, False, 8,
                frame.shape[1], frame.shape[0], frame.shape[2] * frame.shape[1]
            )
            VID_THUMB_CACHE[source_path] = thumbnail
            return thumbnail
        return None

    def EditWall_StackOnChild(self, stack, param):
        visible_child_name = stack.get_visible_child_name()
        self.EditWall_Mode = visible_child_name
        if visible_child_name == "Images":
            self.EditWall_Open.set_visible(True)
            self.EditWall_Ok.set_visible(False)
        elif visible_child_name == "Videos":
            self.EditWall_Open.set_visible(True)
            self.EditWall_Ok.set_visible(False)
        elif visible_child_name == "Website":
            self.EditWall_Open.set_visible(False)
            self.EditWall_Ok.set_visible(True)

    def Save(self):
        self.conf.set('WallpaperEngine', 'enable_widgetengine', str(self.switchbtn_widget_enable.get_state()))
        self.conf.set('WallpaperEngine.WallpaperRenderer', 'slideshow_shuffle', str(self.switchbtn_shuffle.get_state()))
        self.conf.set('WallpaperEngine.WebKit', 'allow_unsecure', str(not self.switchbtn_httpsonly_enable.get_state()))
        self.conf.set('WallpaperEngine.WallpaperRenderer', 'wallpapers', str(self.walls))
        with open(WE_CONF_PATH, 'w') as configfile:
            self.conf.write(configfile)
        self.PARENT.LoadConfig()
        self.PARENT.StartRenderer()

    def IsWebsiteLink(self, string):
        if string.startswith("https://"):
            return True
        elif string.startswith("http://"):
            if self.current_config["allow_unsecure"] == True:
                return True
            else:
                return False
        else:
            return False


def init(parent=None):
    WallpaperProperties(parent)
    Gtk.main()


WE_CONF_PATH = os.path.expanduser('~/.config/NebiSoft/NebiDE.conf')
PREFIX = "./"
if __name__ == "__main__":
    PREFIX = "../../"
    c = init()
    exit()
