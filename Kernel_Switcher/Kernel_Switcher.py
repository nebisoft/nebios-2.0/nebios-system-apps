import gi
import os
import requests
import subprocess

gi.require_version('Gtk', '3.0')

from gi.overrides import Gtk
from gi.repository import Gtk, GObject


class KS:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("ui/main.glade")

        window: Gtk.Window = self.builder.get_object("main-window")
        window.set_title("Kernel Switcher")
        window.connect("destroy", lambda _: Gtk.main_quit())

        self.standard_kernel_box: Gtk.Box = self.builder.get_object("std_k")
        self.modded_kernel_box: Gtk.Box = self.builder.get_object("mod_k")

        for k in os.listdir("./kernels/"):
            self.add_section(k)

        window.show()

    def add_section(self, file):
        builder = Gtk.Builder()
        builder.add_from_file("ui/main.glade")

        template_box: Gtk.Frame = builder.get_object("template")

        kernel_name_label: Gtk.Label = builder.get_object("kernel_name_label")
        kernel_url_markup_label: Gtk.Label = builder.get_object("kernel_url_markup_label")
        kernel_desc_label: Gtk.Label = builder.get_object("kernel_desc_label")

        kernel_variants_box: Gtk.Box = builder.get_object("kernel_variants_box")
        kernel_install_button: Gtk.Button = builder.get_object("kernel_install_button")

        kernel_info = {}

        with open('./kernels/' + file, 'r') as file:
            for line in file:
                # Split each line into key and value pairs
                key, value = map(str.strip, line.split('='))

                # Handle the VariantsName and VariantsURL as lists
                if key == "VariantsName" or key == "VariantsURL":
                    # Split the comma-separated values and strip extra spaces
                    value = [variant.strip() for variant in value.strip("[]").split(",")]

                # Store the key-value pair in the dictionary
                kernel_info[key] = value

        kernel_name_label.set_label(kernel_info["Name"])
        kernel_url_markup_label.set_markup("<a href=\"" + kernel_info["Link"] + "\">" + kernel_info["Link"] + "</a>")
        kernel_desc_label.set_label(kernel_info["Desc"])

        combo = Gtk.ComboBoxText()

        # Populate the ComboBox with VariantsName as display names
        for variant_name in kernel_info.get("VariantsName", []):
            combo.append_text(variant_name)

        kernel_variants_box.add(combo)

        if kernel_info["Type"] == "Standard":
            self.standard_kernel_box.add(template_box)
        elif kernel_info["Type"] == "Modded":
            self.modded_kernel_box.add(template_box)

        kernel_install_button.connect("clicked", lambda _: self.install_kernel(combo, kernel_info))

        template_box.show_all()

    def download_progress(self, block_count, block_size, total_size):
        # Calculate the progress as a percentage
        percent = int((block_count * block_size / total_size) * 100)
        self.progress_bar.set_fraction(percent / 100)

    def install_kernel(self, combo, kernel_info):
        active_text = combo.get_active_text()
        if active_text:
            index = kernel_info.get("VariantsName", []).index(active_text)
            url = kernel_info.get("VariantsURL", [])[index]

            # Create a dialog for showing the download progress
            dialog = Gtk.Dialog("Downloading Kernel", None, Gtk.DialogFlags.MODAL)
            dialog.set_default_size(300, 100)
            dialog.set_resizable(False)

            # Add a progress bar to the dialog
            self.progress_bar = Gtk.ProgressBar()
            dialog.vbox.pack_start(self.progress_bar, True, True, 0)

            # Add a cancel button to the dialog
            cancel_button = Gtk.Button("Cancel")
            dialog.action_area.pack_end(cancel_button, False, False, 0)

            # Connect the cancel button to close the dialog
            cancel_button.connect("clicked", lambda _: dialog.destroy())

            # Start the download in the background
            GObject.idle_add(self.download_kernel, url, dialog)

            dialog.show_all()
            response = dialog.run()
            dialog.destroy()

            if response == Gtk.ResponseType.CANCEL:
                # User canceled the download
                print("Download canceled.")
            elif response == Gtk.ResponseType.DELETE_EVENT:
                # Dialog was closed
                print("Download dialog closed.")
                return

            # Run the script with the downloaded file
            script_path = "/Applications/NSCR_Intepreter.napp"
            if os.path.exists(script_path):
                subprocess.Popen([script_path, "--", "/tmp/downloaded_kernel.nscr"])
            else:
                print("NSCR_Intepreter.napp not found. Are you on old NebiOS version or another Linux distro?")

    def download_kernel(self, url, dialog):
        print("Downloading", url)
        try:
            response = requests.get(url, stream=True)
            total_size = int(response.headers.get('content-length', 0))
            block_size = 1024  # You can adjust this for optimization

            with open("/tmp/downloaded_kernel.nscr", 'wb') as file:
                for data in response.iter_content(block_size):
                    file.write(data)
                    self.download_progress(file.tell(), block_size, total_size)

            dialog.response(Gtk.ResponseType.OK)
            print("Download completed.")
            os.system("chmod +x /tmp/downloaded_kernel.nscr")
        except Exception as e:
            print(f"Download failed: {e}")


mw = KS()
Gtk.main()