const webview = document.getElementById('webview');
const targetUrl = "https://cloud.nebisoftware.com/index.php/apps/repod/";
var reset = false;

window.electronAPI.getCss().then(cssContent => {
	webview.addEventListener('did-finish-load', () => {
	    if (reset == true) {
		reset = false;
		window.location.reload();
	    }

	    // CSS'i enjekte et
	    webview.insertCSS(cssContent);
	});
});

webview.addEventListener('console-message', (e) => {
    console.log('Webview log:', e.message);  // Log to terminal
});

webview.addEventListener('did-navigate', (event) => {
    console.log('Current URL: ' + event.url);  // Log current URL

    // Reload webview if the target URL is reached
    if (event.url === targetUrl) {
        reset = true;
    }
});

document.getElementById('minimize-btn').addEventListener('click', () => {
    window.electronAPI.minimizeWindow();
});

document.getElementById('close-btn').addEventListener('click', () => {
    window.electronAPI.closeWindow();
});

