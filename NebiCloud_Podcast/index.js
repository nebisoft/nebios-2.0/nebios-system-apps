const { app, BrowserWindow, ipcMain, nativeTheme } = require('electron');
const { execSync } = require('child_process');
const path = require('path');
const fs = require('fs');

const xdgSessionDesktop = process.env.XDG_SESSION_DESKTOP ? process.env.XDG_SESSION_DESKTOP.toLowerCase() : '';

function getGtkTheme() {
    try {
        // Use dconf to get the GTK theme
        const theme = execSync('dconf read /org/mate/desktop/interface/gtk-theme').toString().trim().toLowerCase();
        return theme;
    } catch (error) {
        console.error('Error retrieving GTK theme:', error);
        return null;
    }
}

function createWindow() {
    const win = new BrowserWindow({
        width: 1024,
        height: 600,
        transparent: true,
        frame: false,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            nodeIntegration: true, // Enable Node.js integration
            contextIsolation: true, // Disable context isolation
            webviewTag: true
        }
    });
    
    // Minimize window listener
    ipcMain.on('minimize-window', () => {
        win.minimize();
    });

    // Close window listener
    ipcMain.on('close-window', () => {
        win.close();
    });
    
    ipcMain.handle('get-css', () => {
        const cssFilePath = path.join(__dirname, 'css', 'app.css');
        return fs.readFileSync(cssFilePath, 'utf8');
    });

    // Load index.html
    win.loadFile('index.html');
}

function updateTheme() {
    const gtkTheme = getGtkTheme();
    if (gtkTheme) {
        if (gtkTheme.includes('dark')) {
            nativeTheme.themeSource = 'dark';
        } else {
            nativeTheme.themeSource = 'light';
        }
    } else {
        // Default to light theme if GTK theme could not be determined
        nativeTheme.themeSource = 'light';
    }
}

app.whenReady().then(() => {
    createWindow();
    
    if (xdgSessionDesktop === 'nebide' || xdgSessionDesktop === 'nebide-debug') {
        console.log('Environment variable indicates NebiDE desktop environment.');
        setInterval(updateTheme, 1000);
    }

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});
